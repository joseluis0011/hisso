<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        date_default_timezone_set('America/Lima');
        Carbon::setUtf8(true);
        Carbon::setLocale(config('app.locale'));
        setlocale(LC_ALL, 'es_PE', 'es', 'ES', 'es_PE.utf8');
    }
}
