<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = "rol";
    protected $primaryKey = "id";
    protected $fillable = [
        'nombre','descripcion','estado'
    ];
    public $timestamps=false;
    
    protected $casts =[
        'estado'=>'boolean'
    ];
    
    public function Usuario(){
        return $this->hasMany(User::class,'id');
    }
}
