<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agente extends Model
{
    protected $table = "agente";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto',
        'idubigeo',
        'idtservicio',
        'idtmonitoreo',
        'idtagente',
        'iddistrito',
        'codigo',
        'nombre_instalacion',
        'direccion',
        'cant_puntos',
        'usercreated',
        'userupdated'
    ];
}
