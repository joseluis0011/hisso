<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instalacion extends Model
{
    protected $table = "instalacion";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto',
        'area_monitoreada',
        'instalacion',
        'descripcion_instalacion',
        'calle_numero',
        'distrito',
        'area_geografica',
        'region_geografica',
        'estado',
        'usercreated'
    ];
    public $timestamps=false;
}
