<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HProyectoEmpresa extends Model
{
    protected $table = "hproyectoempresa";
    protected $primaryKey = "id";
    protected $fillable = [
        'mes','year','idempresa','cant_trab_emp_proy','domicilio_fiscal','usercreated','userupdated'
    ];
}
