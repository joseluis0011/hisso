<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    protected $table = "equipos";
    protected $primaryKey = "id";
    protected $fillable = [
        'nombre','marca','modelo','serie','fecha_calibracion','fecha_termino','motivo','estado','mes','year'
    ];
    public $timestamps = false;
}
