<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = "permisos";
    protected $primaryKey = "id";
    protected $fillable = [
        'idrol','nombre','descripcion'
    ];
}
