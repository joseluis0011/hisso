<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = "persona";
    protected $primaryKey = "id";
    protected $fillable = [
        'idubigeo','idempresa','idusuario','nombre','apellido','dni',
        'fecha_nacimiento','celular','usercreated','estado','idagencia','idcargo','correo',
        'codigo_interno','sexo','observacion','direccion','fecha_inicio','fecha_fin'
    ];
}
