<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EppTrabajador extends Model
{
    protected $table = "epp_trabajador";
    protected $primaryKey = "id";
    protected $fillable = [
        'idepp',
        'idagente',
        'nombres',
        'dni',
        'area',
        'fecha_entrega',
        'puesto_trabajo'
    ];
    public $timestamps=false;
}
