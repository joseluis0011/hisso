<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table = "provincia";
    protected $primaryKey = "id";
    protected $fillable = [
        'provincia','idDepa'
    ];
    public $timestamps = false;
}
