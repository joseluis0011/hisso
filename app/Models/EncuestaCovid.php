<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EncuestaCovid extends Model
{
    protected $table = "covid_encuesta";
    protected $primaryKey = "id";
    protected $fillable = [
        'idcovid',
        'ultima_semana',
        'presenta_fiebre',
        'tos_dolor_garganta',
        'congestion_nasal',
        'dificultad_respirar',
        'perdida_olfato',
        'malestar_general',
        'expectoracion_flema',
        'dolor_abdominal',
        'otros_diagnostico',
        'tomando_medicacion',
        'validacion',
        'fecha',
        'estado'
    ];
    public $timestamps=false;
}
