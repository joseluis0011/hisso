<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProyectoDetalle extends Model
{
    protected $table = "proyecto_detalle";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto','idpersona','motivo'
    ];
}
