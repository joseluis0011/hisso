<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHasRol extends Model
{
    protected $table = "usuario_has_rol";
    protected $primaryKey = "usuario_id";
    protected $fillable = [
        'usuario_id','rol_id'
    ];
    public $timestamps=false;
}
