<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetErgonomia extends Model
{
    protected $table = "det_ergonomia";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto','idtrabajador','punto_monitoreo','fecha_monitoreo','hora_monitoreo','trabajadores_expuestos','codigo','edad','puesto','hoario_trabajo',
        'turno_rotativo','peso_talla','porcentaje_hora_campo','trabajo_campo_1','trabajo_campo_2','trabajo_campo_3','trabajo_campo_4','trabajo_campo_5',
        'porcentaje_hora_oficina','trabajo_oficina_1','trabajo_oficina_2','trabajo_oficina_3','trabajo_oficina_4','trabajo_oficina_5','dolencia_fisica',
        'discapacidad','gestacion','altura_asiento','altura_espaldar','alcolchonado_recubierto','reposabrazos','largo','ancho','superficie','colocacion_posicion',
        'espacio_piernas','espacio_silla','altura_pantalla_laptop','altura_pantalla_pc','distancia_cabeza','foto1','foto2','foto3','foto4','foto5'
        ,'estado'
    ];
    public $timestamps = false;
}
