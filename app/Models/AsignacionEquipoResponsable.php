<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsignacionEquipoResponsable extends Model
{
    protected $table = "asignacion_responsable_equipo";
    protected $primaryKey = "id";
    protected $fillable = [
        'idasignacion_proyecto','idpersona','idequipo','usercreated','idagente','fecha_inicio','fecha_fin','descripcion'
    ];
}
