<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inspecciones extends Model
{
    protected $table = "inspecciones";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto',
        'tipo',
        'fecha_hallazgo',
        'hora_hallazgo',
        'descripcion_peligro',
        'idcategoria_peligro',
        'idsubcategoria_peligro',
        'area',
        'subarea',
        'tipo_ubicacion',
        'personal',
        'cargo',
        'correo',
        'foto1',
        'foto2',
        'foto3',
        'estado',
        'idpersona'
    ];
    public $timestamps = false;
}
