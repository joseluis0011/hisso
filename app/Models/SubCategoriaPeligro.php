<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategoriaPeligro extends Model
{
    protected $table = "subcategoria_peligro";
    protected $primaryKey = "id";
    protected $fillable = [
        'idcategoria',
        'nombre',
        'descripcion'
    ];
    public $timestamps = false;
}
