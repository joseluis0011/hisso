<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    protected $table = "trabajador";
    protected $primaryKey = "id";
    protected $fillable = [
        'idagente','dni','nombres_apellidos','puesto_trabajo','area_monitoreada',
        'instalacion','descripcion_instalacion','calle_numero','distrito','area_geografica','region_geografica','usercreated',
        'estado'
    ];
    public $timestamps=false;
}
