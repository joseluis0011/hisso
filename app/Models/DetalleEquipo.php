<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleEquipo extends Model
{
    protected $table = "equipos_detalle";
    protected $primaryKey = "id";
    protected $fillable = [
        'idequipo','idpersona','motivo','created_at','updated_at'
    ];
}
