<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EppEquipos extends Model
{
    protected $table = "epp_equipos";
    protected $primaryKey = "id";
    protected $fillable = [
        'codigo','idagente','nombre','frecuencia'
    ];
    public $timestamps=false;
}
