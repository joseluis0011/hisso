<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriaPeligro extends Model
{
    protected $table = "categoria_peligro";
    protected $primaryKey = "id";
    protected $fillable = [
        'nombre',
        'descripcion'
    ];
    public $timestamps = false;
}
