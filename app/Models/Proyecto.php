<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = "proyecto";
    protected $primaryKey = "id";
    protected $fillable = [
        'idempresa',
        'codigo',
        'fecha_inicio',
        'fecha_fin',
        'lima',
        'provincia',
        'estado',
        'usercreated',
        'userupdated',
        'mes',
        'year'
    ];
}
