<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsignacionProyecto extends Model
{
    protected $table = "asignacion_proyecto";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto','usercreated','userupdated','estado','responsable','equipos'
    ];
}
