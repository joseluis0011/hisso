<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ubigeo extends Model
{
    protected $table = "ubigeo";
    protected $primaryKey = "id";
    protected $fillable = [
        'iddepartamento','idprovincia','iddistrito'
    ];
    public $timestamps=false;
}
