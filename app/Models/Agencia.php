<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agencia extends Model
{
    protected $table = "agencia";
    protected $primaryKey = "id";
    protected $fillable = [
        'idempresa','idubigeo','nombre','sede','tipo_instalacion'
    ];
    public $timestamps=false;
}
