<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetIluminacion extends Model
{
    protected $table = "det_iluminacion";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto','idtrabajador','punto_monitoreo','fecha_monitoreo','hora_monitoreo','max1','max2','max3','min1','min2',
        'min3','avg1','avg2','avg3','tipo_iluminaria','altura_iluminaria','trabajo_realizado','caracteristicas_entorno',
        'numero_trabajadores','foto1','foto2','foto3','foto4','foto5','foto6','foto7','foto8','foto9','foto10','estado'
    ];
    public $timestamps = false;
}
