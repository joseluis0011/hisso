<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
    protected $table = "distrito";
    protected $primaryKey = "id";
    protected $fillable = [
        'distrito','idProv'
    ];
    public $timestamps = false;
}
