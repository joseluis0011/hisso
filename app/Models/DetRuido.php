<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetRuido extends Model
{
    protected $table = "det_ruido";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto','idinstalacion','punto_monitoreo','fecha_monitoreo','hora_monitoreo','fuente_ruido',
        'max1','max2','max3','min1','min2','min3','lequiv1','lequiv2','lequiv3',
        'actividades','numero_trabajadores','foto','estado'
    ];
    public $timestamps = false;
}
