<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetBiologicoHisopo extends Model
{
    protected $table = "det_bio_hisopo";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto','idinstalacion','punto_monitoreo','fecha_monitoreo','hora_monitoreo',
        'codigo_muestra','descripcion_lugar','muestreo','foto1','foto2',
        'observacion','estado'
    ];
    public $timestamps = false;
}
