<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresaContacto extends Model
{
    protected $table = "empresa_contacto";
    protected $primaryKey = "id";
    protected $fillable = [
        'idempresa','nombre','correo','celular','telefono','cargo','usercreated','userupdated'
    ];
}
