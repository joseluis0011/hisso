<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = "empresa";
    protected $primaryKey = "id";
    protected $fillable = [
        'razon_social','ruc','act_economica','domicilio_fiscal','idubigeo','comentario','estado',
        'usercreated','userupdated','mes','year'
    ];
}
