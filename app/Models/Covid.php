<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Covid extends Model
{
    protected $table = "covid";
    protected $primaryKey = "id";
    protected $fillable = [
        'idproyecto','idusuario','nombre','dni','correo'
    ];
    public $timestamps = false;
}
