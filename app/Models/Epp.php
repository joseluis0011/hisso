<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Epp extends Model
{
    protected $table = "epp";
    protected $primaryKey = "id";
    protected $fillable = [
        'idagente',
        'entrega',
        'tipo_equipo',
        'usercreated'
    ];
    public $timestamps=false;
}
