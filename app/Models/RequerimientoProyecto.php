<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequerimientoProyecto extends Model
{
    protected $table = "requerimiento_proyecto";
    protected $primaryKey = "id";
    protected $fillable = [
        'idagente','tipo'
    ];
    public $timestamps=false;
}
