<?php


namespace App\Repo;


use App\Models\Agencia;
use App\Models\Cargo;
use App\Models\Departamento;
use App\Models\Empresa;
use App\Models\Equipo;
use App\Models\Persona;
use App\Models\Provincia;
use App\Models\Distrito;
use App\Models\Rol;
use App\Models\Ubigeo;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use function App\obtenerCargo;

class RepoUsuario
{
    public static function agregarUsuario($request)
    {
        return User::updateOrCreate(
            [
                'id' => $request['idusuario']
            ],
            [
                'idrol' => $request['formulario']['rol'],
                'usuario' => $request['formulario']['usuario'],
                'password' => Hash::make($request['formulario']['password']),
                'pwd' => encrypt($request['formulario']['password']),
                'estado' => 0
            ]
        );
    }
    public static function agregarPersona($request,$idusuario){
        $model = Persona::where('id',$request['formulario']['personal'])->first();
        $model->idusuario=$idusuario;
        $model->save();
        return $model;
    }
    public static function estadoUsuario($estado)
    {
        return DB::table('persona as p')
            ->join('empresa as e','e.id','=','p.idempresa')
            ->rightJoin('usuario as u','u.id','=','p.idusuario')
            ->join('rol as r','r.id','=','u.idrol')
            ->whereNotIn('r.id',[1])
            ->where('u.estado',$estado)
            ->select('p.id as idpersona','p.dni','u.usuario','e.razon_social','u.id as idusuario',
                DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name"))
            ->orderBy('p.id', 'desc')->paginate(15);
    }
    
    public static function buscarUsuario($estado, $buscar, $select)
    {
        return DB::table('persona as p')
            ->join('empresa as e','e.id','=','p.idempresa')
            ->leftJoin('usuario as u','u.id','=','p.idusuario')
            ->where('p.estado',$estado)
            ->where($select, 'like', '%' . $buscar . '%')
            ->select('p.id as idpersona','p.dni','u.usuario','u.pwd','e.razon_social',
                DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name"))
            ->orderBy('p.id', 'desc')->paginate(15);
    }
    
    public static function estado($request)
    {
        if ($request['tipo'] == 0) {
            $data = User::where('id', $request['objeto']['idusuario'])->first();
            $data->estado = 1;
            $data->save();
            if ($data) {
                return ['success' => true];
            } else {
                return ['error' => false];
            }
        } else {
            $data = User::where('id', $request['objeto']['idusuario'])->first();
            $data->estado = 0;
            $data->save();
            if ($data) {
                return ['success' => true];
            } else {
                return ['error' => false];
            }
        }
    }
    
    public static function getdata($request)
    {
        $usuario = User::where('id',$request['idusuario'])->get();
        $pwd=decrypt($usuario[0]->pwd);
        $data = DB::table('persona as p')
            ->join('usuario as u','u.id','=','p.idusuario')
            ->join('rol as r','r.id','=','u.idrol')
            ->where('u.id',$request['idusuario'])
            ->select('p.id','r.id')
            ->get();
        return ['usuario'=>$usuario,'pwd'=>$pwd,'data'=>$data];
    }
    
    public static function obtenerPassword($request)
    {
        $data= User::where('id',$request['idusuario'])->select('pwd')->get();
        $pwd=decrypt($data[0]->pwd);
        return $pwd;
    }
}
