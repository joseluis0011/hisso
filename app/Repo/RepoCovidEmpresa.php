<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\Covid;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;

    class RepoCovidEmpresa
    {
        public static function listar(){
            return Covid::where('idproyecto',Auth::user()->proyecto[0]->id)
                ->select('id','idusuario','nombre','correo','dni',DB::raw("DATE_FORMAT(fecha,'%d/%m/%Y %H:%i') as fecha"))
                ->orderBy('id','desc')->paginate(20);
        }
        public static function search($buscar){
            return Covid::where('idproyecto',Auth::user()->proyecto[0]->id)
                ->where('dni','like','%'.$buscar.'%')
                ->select('id','idusuario','nombre','correo','dni',DB::raw("DATE_FORMAT(fecha,'%d/%m/%Y %H:%i') as fecha"))
                ->orderBy('id','desc')->paginate(20);
        }
    }
