<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\Covid;
    use App\Models\Empresa;
    use App\Models\EncuestaCovid;
    use App\Models\Proyecto;
    use App\User;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Crypt;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Hash;
    use Symfony\Component\Console\Input\Input;
    use function App\obtenerRol;

    class RepoEncuestaCovid
    {
        // hisso
        public static function listCovid($id){
            return DB::table('covid')
                ->where('idproyecto',$id)
                ->select('idusuario','nombre','dni','correo',DB::raw("DATE_FORMAT(fecha,'%d/%m/%Y %H:%i') as fecha"))
                ->orderBy('id','desc')->paginate(20);
            
        }
        public static function searchCovid($id,$select,$buscar){
            return DB::table('covid')
                ->where('idproyecto',$id)
                ->where($select,'like','%'.$buscar.'%')
                ->select('idusuario','nombre','dni','correo',DB::raw("DATE_FORMAT(fecha,'%d/%m/%Y %H:%i') as fecha"))
                ->orderBy('id','desc')->paginate(20);
        }
        public static function updateEmpresa($request){
            try{
                DB::beginTransaction();
                $user = self::addUsuario($request);
                $empresa = self::updatebyEmpresa($user->id,$request);
                DB::commit();
                return ['success'=>true];
            }catch (\Exception $e){
                DB::rollback();
                return ['success'=>false,'message'=>$e->getMessage()];
            }
        }
        public static function addUsuario($request){
            return User::create([
               'idrol'=>obtenerRol('Empresa')->id,
                'usuario'=>$request['usuario'],
                'password'=>Hash::make($request['password']),
                'pwd'=>encrypt($request['password']),
                'estado'=>0
            ]);
        }
        public static function updatebyProyecto($idusuario,$request){
            $data = Proyecto::where('id',$request['codigo'])->first();
            $data->idusuario=$idusuario;
            $data->save();
            return $data;
        }
        // trabajador
        public static function agregar($request){
            return EncuestaCovid::updateOrCreate(
                [
                    'id'=>$request['id']
                ],
                [
                    'idcovid'=>Auth::user()->covid[0]->id,
                    'ultima_semana'=>$request['form']['ultima_semana'],
                    'presenta_fiebre'=>$request['form']['presenta_fiebre'],
                    'tos_dolor_garganta'=>$request['form']['tos_dolor_garganta'],
                    'congestion_nasal'=>$request['form']['congestion_nasal'],
                    'dificultad_respirar'=>$request['form']['dificultad_respirar'],
                    'perdida_olfato'=>$request['form']['perdida_olfato'],
                    'malestar_general'=>$request['form']['malestar_general'],
                    'expectoracion_flema'=>$request['form']['expectoracion_flema'],
                    'dolor_abdominal'=>$request['form']['dolor_abdominal'],
                    'otros_diagnostico'=>$request['form']['otros_diagnostico'],
                    'tomando_medicacion'=>$request['form']['tomando_medicacion'],
                    'validacion'=>$request['form']['validacion'],
                    'fecha'=>Carbon::now(),
                    'estado' => 0
                ]
            );
        }
        public static function getPreguntas(){
            return DB::table('encuestacovid')
                ->where('encuesta','=','1')
                ->get(['idencuesta']);
        }
        public static function agregarEncuesta($request){
            $preguntas = $request['preguntas'];
            $respuestas = $request['respuestas'];
            for($i = 0; $i < count($preguntas); $i++){
                $data= DB::table('respuestacovid')->insert(
                    ['fecha'=>Carbon::now(),'idusuario'=>Auth::user()->id,'pregunta'=>$preguntas[$i],'respuesta'=>$respuestas[$i]]
                );
            }
            return $data;
        }
        public static function validatorArray($request){
            $respuestas = $request['respuestas'];
            $search = array_search('si',$respuestas);
            if (in_array("si",$respuestas)){
                self::respuestacovid(1);
                self::updateFechaCovid();
                return ['success'=>true,'message'=>'no laboral'];
            }else{
                self::respuestacovid(0);
                return ['success'=>'laboral','message'=>'laboral'];
            }
        }
        public static function respuestacovid($estado){
            return DB::table('respuestacovid_filter')->insert(
              ['fecha'=>Carbon::now(),'idusuario'=>Auth::user()->id,'estado'=>$estado]
            );
        }
        public static function updateFechaCovid(){
            $model=Covid::where('idusuario',Auth::user()->id)->first();
            $model->fecha=Carbon::now();
            $model->save();
        }
        public static function verificar(){
            $data = EncuestaCovid::where('idpersona',Auth::user()->persona[0]->id)
                ->whereDate('fecha','=',Carbon::now())
                ->get();
            if (count($data) >= 1){
                return ['success'=>true,'estado'=>0];
            }else{
                return ['success'=>false,'estado'=>1];
            }
        }
        public static function generarPDF(){
            return DB::table('usuario as u')
                ->join('covid as c','c.idusuario','=','u.id')
                ->where('u.id',Auth::user()->id)
                ->select('c.nombre','c.dni','c.correo','c.id')
                ->get();
        }
        public static function getEmpresaPDF(){
            return DB::table('covid as c')
                ->join('proyecto as p','p.id','=','c.idproyecto')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->join('ubigeo as u','u.id','=','e.idubigeo')
                ->join('departamento as d','d.id','=','u.iddepartamento')
                ->join('provincia as pro','pro.id','=','u.idprovincia')
                ->join('distrito as dis','dis.id','=','u.iddistrito')
                ->where('c.idusuario',Auth::user()->id)
                ->select('e.razon_social','e.ruc','e.domicilio_fiscal','d.departamento','pro.provincia','dis.distrito')
                ->get();
        }
        public static function validarCampoNull($request){
            $data = Proyecto::where('id',$request['codigo'])->select('idusuario')->get();
            return $data;
        }
        public static function obtenerUsuario($id){
            $user = User::where('id',$id)->first(['pwd','usuario']);
            $pwd = decrypt($user->pwd);
            return [$user->usuario,$pwd];
        }
        public static function listEncuesta($select,$buscar,$idusuario){
            $fecha=explode(',',$buscar);
            return DB::table('respuestacovid_filter')
                ->where('idusuario',$idusuario)
                ->whereBetween('fecha',[$fecha[0],$fecha[1]])
                ->select('id','idusuario','estado','fecha',DB::raw("DATE_FORMAT(fecha,'%d/%m/%Y %H:%i:%S') as fecha_format"))
                ->orderBy('fecha','desc')->paginate(12);
            /*return DB::table('respuestacovid as r')
                ->join('usuario as u','u.id','=','r.idusuario')
                ->join('covid as c','c.idusuario','=','u.id')
                ->where('r.idusuario',$idusuario)
                ->whereBetween('r.fecha',[$fecha[0],$fecha[1]])
                ->select('r.id','r.pregunta','r.respuesta','c.nombre',
                    DB::raw("DATE_FORMAT(r.fecha,'%d/%m/%Y %H:%i') as fecha"))
                ->orderBy('r.fecha','desc')->paginate(12);*/
        }
        public static function searchEncuesta($select,$busqueda){
            /*$fecha=explode(',',$buscar);
            $sql = "call encuestausuarioyfecha(?,?,?)";
            $page = 1;
            $paginate = 10;
            $data= DB::select($sql,array($fecha[0],$fecha[1],$idusuario));
            $offSet = ($page * $paginate) - $paginate;
            $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);
            $data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page);
            return $data;*/
        }
        public static function eliminarTrabajador($request){
            try {
                DB::beginTransaction();
            $covid = Covid::where('id',$request['objeto']['id'])->delete();
            $user = User::where('id',$request['objeto']['idusuario'])->delete();
            $respuestacovid= DB::table('respuestacovid')->where('idusuario',$request['objeto']['idusuario'])->delete();
                DB::commit();
                return ['success'=>true];
            } catch (\Exception $e) {
                DB::rollback();
                return ['success'=>false,'message'=>$e->getMessage()];
            }
        }
        public static function searchPregunta($request){
            $time = Carbon::parse($request['objeto']['fecha'])->format('H:i:s');
            $data = DB::table('respuestacovid')
                ->where('fecha',$request['objeto']['fecha'])
                ->select('pregunta','respuesta',DB::raw("DATE_FORMAT(fecha,'%d/%m/%Y %H:%i:%S') as fecha"))
                ->get();
            if ($data){
                return ['success'=>true,$data];
            }else{
                return ['success'=>false];
            }
        }
        public static function searchTrabajador($request){
            return Covid::where('idusuario',$request['id'])->get(['nombre','dni']);
        }
        public static function agregarTrabajador($request){
            try {
                DB::beginTransaction();
            $user = self::addUser($request['dni'],$request['dni'],obtenerRol('Trabajador')->id);
            $covid = self::addTrabajador($request,$user->id);
                $userbyEmpresa = self::addUser($request['usuario'],$request['password'],obtenerRol('Empresa')->id);
                $empresa = self::updatebyProyecto($userbyEmpresa->id,$request);
                DB::commit();
                return ['success'=>true,'message'=>'Agregado Correctamente'];
            } catch (\Exception $e) {
                DB::rollback();
                return ['success'=>false,'message'=>$e->getMessage()];
            }
        }
        public static function addTrabajador($request,$idusuario){
            return Covid::create([
                'idusuario'=>$idusuario,
                'idproyecto'=>$request['codigo'],
                'nombre'=>$request['nombres'],
                'dni'=>$request['dni'],
                'correo'=>$request['correo']
            ]);
        }
        public static function addUser($usuario,$password,$rol){
            return User::create([
               'usuario'=>$usuario,
               'password'=>Hash::make($password),
               'pwd'=>encrypt($password),
               'idrol'=>$rol,
                'estado'=>0
            ]);
        }
    }
