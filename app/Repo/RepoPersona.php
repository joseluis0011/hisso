<?php


namespace App\Repo;


use App\Models\Agencia;
use App\Models\Cargo;
use App\Models\Departamento;
use App\Models\Empresa;
use App\Models\Equipo;
use App\Models\Persona;
use App\Models\Provincia;
use App\Models\Distrito;
use App\Models\Rol;
use App\Models\Ubigeo;
use App\Models\UserHasRol;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use function App\obtenerCargo;

class RepoPersona
{
    public static function agregarPersona($request,$ubigeo,$idusuario)
    {
        return Persona::updateOrCreate(
            [
                'id' => $request['idpersona']
            ],
            [
                'idubigeo' => $ubigeo,
                'idusuario'=>$idusuario,
                'idempresa' => $request['formulario']['empresa'],
                'nombre' => $request['formulario']['nombre'],
                'apellido' => $request['formulario']['apellido'],
                'dni' => $request['formulario']['dni'],
                'estado' => 0,
                'fecha_nacimiento' => isset($request['formulario']['fnacimiento']) ? $request['formulario']['fnacimiento'] : null,
                'celular' => $request['formulario']['celular'],
                'usercreated' => Auth::user()->id,
                'correo' => isset($request['formulario']['correo']) ? $request['formulario']['correo'] : '',
                'codigo_interno' => $request['formulario']['codigo'],
                'sexo' => $request['formulario']['sexo'],
                'observacion'=>isset($request['formulario']['observacion']) ? $request['formulario']['observacion'] : '',
                'direccion'=>isset($request['formulario']['direccion']) ? $request['formulario']['direccion'] : '',
                'fecha_inicio'=>isset($request['formulario']['finicio']) ? $request['formulario']['finicio'] : null,
                'fecha_fin'=>isset($request['formulario']['ffinal']) ? $request['formulario']['ffinal'] : null,
            ]
        );
    }
    public static function agregarUbigeo($request){
        return Ubigeo::updateOrCreate(
          [
              'id'=>$request['formulario']['idubigeo']
          ],
          [
              'iddepartamento'=>$request['formulario']['departamento'],
              'idprovincia'=>$request['formulario']['provincia'],
              'iddistrito'=>$request['formulario']['distrito']
          ]
        );
    }
    public static function agregarUsuario($request){
        $user=self::addUser($request);
        $userRol=self::userRol($request,$user->id);
        return $user;
    }
    public static function addUser($request){
        return User::updateOrCreate(
            [
                'id'=>$request['formulario']['idusuario']
            ],
            [
                'usuario'=>$request['formulario']['dni'],
                'password'=>Hash::make($request['formulario']['dni']),
                'estado'=>0
            ]
        );
    }
    public static function userRol($request,$idusuario){
        return UserHasRol::updateOrCreate(
            [
                'usuario_id'=>$request['formulario']['idusuario']
            ],
            [
                'usuario_id'=>$idusuario,
                'rol_id'=>$request['formulario']['idrol']
            ]
        );
    }
    public static function listarPersona($estado, $condicion)
    {
        if ($condicion == 1) {
            return DB::table('persona as p')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->whereIn('p.estado',$estado)
                ->select('p.id as idpersona','p.dni','p.celular','e.razon_social','p.estado','p.idusuario',
                    DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(p.fecha_nacimiento,'%d/%m/%Y') as fechanacimiento"))
                ->get();
        } else {
            return DB::table('usuario as u')->join('persona as p', 'p.idusuario', '=', 'u.id')
                ->where('p.estado', $estado)
                ->select('u.usuario', 'u.id as idusuario', 'p.id as idpersona', 'p.nombre', 'p.apellido',
                    'p.cargo', 'p.dni', 'p.celular', DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(p.fecha_nacimiento,'%d/%m/%Y') as fechanacimiento"))
                ->get();
        }
    }
    
    public static function buscarPersona($estado, $buscar, $select)
    {
      return  DB::table('persona as p')
            ->join('empresa as e','e.id','=','p.idempresa')
            ->whereIn('p.estado',$estado)
             ->where($select, 'like', '%' . $buscar . '%')
            ->select('p.id as idpersona','p.dni','p.celular','e.razon_social','p.estado','p.idusuario',
                DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(p.fecha_nacimiento,'%d/%m/%Y') as fechanacimiento"))
            ->orderBy('p.id', 'desc')->paginate(15);
        
    }
    
    public static function estado($request)
    {
        try {
            DB::beginTransaction();
            self::updateEstado($request);
            DB::commit();
            return response()->json(array("success" => true,'message'=>'Modificado Correctamente'));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error'=>false,'message'=>$e->getMessage()]);
        }
    }
    public static function updateEstado($request){
        $persona =Persona::where('id', $request['objeto']['idpersona'])->first();
        $usuario=User::where('id',$request['objeto']['idusuario'])->first();
        if ($request['tipo'] == 0){
            $persona->estado =1;
            $usuario->estado=1;
        }else{
            $persona->estado=0;
            $usuario->estado=0;
        }
        $persona->save();
        $usuario->save();
        return [$persona,$usuario];
    }
    public static function getdata($id)
    {
        $data= DB::table('persona as p')
            ->join('usuario as us','us.id','=','p.idusuario')
            ->join('usuario_has_rol as ur','ur.usuario_id','=','us.id')
            ->leftJoin('ubigeo as u','u.id','=','p.idubigeo')
            ->leftJoin('provincia as pro','pro.id','=','u.idprovincia')
            ->leftJoin('distrito as dis','dis.id','=','u.iddistrito')
            ->where('p.id',$id)
            ->select('p.dni','p.celular','p.nombre','p.apellido','p.correo','p.direccion','p.codigo_interno as codigo',
                'p.fecha_nacimiento as fnacimiento','p.sexo','p.idempresa as empresa',
                'u.id as idubigeo','u.iddepartamento as departamento','u.idprovincia as provincia','u.iddistrito as distrito','ur.rol_id as idrol',
                'p.fecha_inicio as finicio','p.fecha_fin as ffinal','p.observacion','us.id as idusuario')->get();
        return [$data[0]];
    }
    public static function verificarDni($request)
    {
        return User::where('usuario', $request['dni'])->first();
    }
    public static function consultaDni($dni)
    {
        $client = new Client([
            'base_uri' => 'https://dni.optimizeperu.com/api/persons/',
        ]);
        $response = $client->request('GET', $dni);
        return json_decode($response->getBody()->getContents());
    }
    public static function addCargo($request){
        return Cargo::create([
           'nombre'=>$request['cargo'],
           'estado'=>0
        ]);
    }
    public static function getEmpresa(){
        return Empresa::where('estado',0)->get();
    }
  public static function generaCodigInterno(){
      $sql = "call sp_Generar_Cod_Interno_Responsable()";
      return DB::select($sql);
  }
}
