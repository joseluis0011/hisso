<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\DetBiologicoHisopo;
    use App\Models\DetIluminacion;
    use App\Models\Proyecto;
    use App\Models\Trabajador;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Str;

    class RepoBiologico
    {
        public static function listInstalacion($id)
        {
            return DB::table('instalacion as i')
                ->leftJoin('det_bio_hisopo as bh', 'bh.idinstalacion', '=', 'i.id')
                ->where('i.idproyecto',$id)
                ->select('i.id','i.idproyecto','i.area_monitoreada','i.instalacion','i.descripcion_instalacion','i.area_geografica',
                    'bh.estado')
                ->orderBy('i.id','desc')->paginate(20);
        }
        public static function searchInstalacion($id,$select,$buscar){
            return DB::table('trabajador as t')
                ->leftJoin('det_iluminacion as di', 'di.idtrabajador', '=', 't.id')
                ->where('t.idproyecto',$id)
                ->where($select,'like','%'.$buscar.'%')
                ->select('t.id','t.idproyecto','t.nombres','t.apellido_paterno','t.apellido_materno','t.puesto_trabajo',
                    'di.estado')
                ->orderBy('t.id','desc')->paginate(20);
        }
        public static function addDetalleBiologico($request){
            if ($request->file('file')){
                $logo=$request->file;
                $random = Str::random(10);
                $filename = $logo->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/biologico',$logo,$fileserver);
                $file=asset('storage/biologico/'.$fileserver);
            }else{
                $file=$request->file;
            }
            if ($request->file('file1')){
                $logo1=$request->file1;
                $random = Str::random(10);
                $filename = $logo1->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/biologico',$logo1,$fileserver);
                $file1=asset('storage/biologico/'.$fileserver);
            }else{
                $file1=$request->file1;
            }
            return DetBiologicoHisopo::updateOrCreate(
                [
                    'id'=>$request['id']
                ],
                [
                    'idproyecto'=>$request['idproyecto'],
                    'idinstalacion'=>$request['idinstalacion'],
                    'punto_monitoreo'=>$request['punto_monitoreo'],
                    'fecha_monitoreo'=>$request['fecha_monitoreo'],
                    'hora_monitoreo'=>$request['hora_monitoreo'],
                    'codigo_muestra'=>$request['codigo_muestra'],
                    'descripcion_lugar'=>$request['descripcion_lugar'],
                    'muestreo'=>$request['muestreo'],
                    'estado'=>1,
                    'foto1'=>$file,
                    'foto2'=>$file1
                ]
            );
        }
        public static function getDetalleBiologico($request){
            return DetBiologicoHisopo::where('idinstalacion',$request['idinstalacion'])->get();
        }
        
    }
