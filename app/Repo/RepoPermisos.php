<?php
    
    
    namespace App\Repo;
    
    
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;

    class RepoPermisos
    {
        public static function permisosByRol($request){
            if(!$request->ajax()) return redirect('/');
    
            $nIdUsuario  =   $request->nIdUsuario;
    
            if (!$nIdUsuario) {
                $nIdUsuario =   Auth::id();
            }
    
            $nIdUsuario =   ($nIdUsuario   ==  NULL) ? ($nIdUsuario   =   0) :   $nIdUsuario;
    
            $rpta   =   DB::select('call sp_Usuario_getListarRolPermisosByUsuario (?)',
                [
                    $nIdUsuario
                ]);
    
            return $rpta;
        }
    }
