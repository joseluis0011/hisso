<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\Agente;
    use App\Models\Empresa;
    use App\Models\Proyecto;
    use App\Models\ProyectoDetalle;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;

    class RepoProyecto
    {
        public static function getEmpresa(){
            return Empresa::where('estado',0)->get();
        }
        public static function addProyecto($request){
            return Proyecto::updateOrCreate(
                [
                    'id'=>$request['formulario']['idproyecto']
                ],
                [
                    'idempresa'=>$request['formulario']['idempresa'],
                    'codigo'=>$request['formulario']['codigo'],
                    'fecha_inicio'=>$request['formulario']['fecha_inicio'],
                    'fecha_fin'=>$request['formulario']['fecha_fin'],
                    'lima'=>$request['formulario']['lima'],
                    'provincia'=>$request['formulario']['provincia'],
                    'estado'=>0,
                    'usercreated'=>Auth::user()->id,
                    'mes'=>Carbon::now()->format('m'),
                    'year'=>Carbon::now()->format('Y')
                ]
            );
        }
        public static function addAgentes($request,$idproyecto){
            try{
                $agente =$request['arrayListAgentes'];
                for($i = 0; $i < count($agente); $i++){
                    if ($agente[$i]['servicio'] == 6) {
                        $servicio = $agente[$i]['servicio'];
                        $monitoreo =10;
                        $categoria =18 ;
                        $puntos = null;
                    }elseif ($agente[$i]['servicio'] == 5){
                        $servicio=$agente[$i]['servicio'];
                        $monitoreo =9;
                        $categoria =17;
                        $puntos =null;
                    }elseif ($agente[$i]['servicio'] == 4){
                        $servicio=$agente[$i]['servicio'];
                        $monitoreo =8;
                        $categoria =16;
                        $puntos =null;
                    }elseif ($agente[$i]['servicio'] == 3){
                        $servicio=$agente[$i]['servicio'];
                        $monitoreo =7;
                        $categoria =15;
                        $puntos =null;
                    }elseif ($agente[$i]['servicio'] == 2){
                        $servicio=$agente[$i]['servicio'];
                        $monitoreo =6;
                        $categoria =1;
                        $puntos =$agente[$i]['puntos'];
                    }else{
                        $servicio=$agente[$i]['servicio'];
                        $monitoreo =$agente[$i]['monitoreo'];
                        $categoria =$agente[$i]['categoria'] ;
                        $puntos =$agente[$i]['puntos'];
                    }
                    self::changeValores($agente[$i]['id'],$idproyecto,$agente[$i]['codigoInterno'],$servicio,$monitoreo,$categoria,$agente[$i]['distrito'],
                        $agente[$i]['instalacion'],$agente[$i]['direccion'],$puntos);
                }
                return ['success'=>true];
            }catch (\Exception $e){
                return ['false'=>$e->getMessage()];
            }
        }
        public static function changeValores($id,$proyecto,$codigo,$servicio,$monitoreo,$categoria,$distrito,$instalacion,$direccion,$punto){
            Agente::updateOrCreate(
                [
                    'id'=>$id
                ],
                [
                    'idproyecto'=>$proyecto,
                    'codigo'=>$codigo,
                    'idtservicio'=>$servicio,
                    'idtmonitoreo'=>$monitoreo,
                    'idtagente'=>$categoria,
                    'iddistrito'=>$distrito,
                    'nombre_instalacion'=>$instalacion,
                    'direccion'=>$direccion,
                    'cant_puntos'=>$punto,
                    'usercreated'=>Auth::user()->id
                ]
            );
        }
        public static function listarProyecto($estado){
            return DB::table('proyecto as p')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->leftJoin('agente as ag','ag.idproyecto','=','p.id')
                ->groupBy('p.id')
                ->where('p.estado',$estado)
                ->select('p.id as idproyecto','p.codigo',DB::raw("DATE_FORMAT(p.fecha_inicio,'%d/%m/%Y') as fecha_inicio"),
                    'e.razon_social',DB::raw('count(ag.idtservicio) as nservicios'))
                ->orderBy('p.id','desc')->paginate(12);
            
        }
        public static function buscarProyecto($estado,$buscar,$select){
            if ($select == 'p.fecha_inicio'){
                return DB::table('proyecto as p')
                    ->join('empresa as e','e.id','=','p.idempresa')
                    ->leftJoin('agente as ag','ag.idproyecto','=','p.id')
                    ->groupBy('p.id')
                    ->where('p.estado',$estado)
                    ->whereDate($select,$buscar)
                    ->select('p.id as idproyecto','p.codigo',DB::raw("DATE_FORMAT(p.fecha_inicio,'%d/%m/%Y') as fecha_inicio"),
                        'e.razon_social',DB::raw('count(ag.idtagente) as nservicios'))
                    ->orderBy('p.id','desc')->paginate(12);
            }else{
                return DB::table('proyecto as p')
                    ->join('empresa as e','e.id','=','p.idempresa')
                    ->leftJoin('agente as ag','ag.idproyecto','=','p.id')
                    ->groupBy('p.id')
                    ->where('p.estado',$estado)
                    ->where($select,'like','%'.$buscar.'%')
                    ->select('p.id as idproyecto','p.codigo',DB::raw("DATE_FORMAT(p.fecha_inicio,'%d/%m/%Y') as fecha_inicio"),
                        'e.razon_social',DB::raw('count(ag.idtagente) as nservicios'))
                    ->orderBy('p.id','desc')->paginate(12);
            }
        }
        public static function getDataProyecto($id){
            $proyecto=Proyecto::where('id',$id)->first(['id as idproyecto','codigo','fecha_fin','fecha_inicio','lima','provincia','idempresa']);
            $agente=DB::table('agente as a')
                ->join('ms_tservicio as ts','ts.id','=','a.idtservicio')
                ->leftJoin('ms_tmonitoreo as tm','tm.id','=','a.idtmonitoreo')
                ->leftJoin('ms_tagente as ta','ta.id','=','a.idtagente')
                ->join('distrito as dis','dis.id','=','a.iddistrito')
                ->join('provincia as pro','pro.id','=','dis.idProv')
                ->join('departamento as dep','dep.id','=','pro.idDepa')
                ->where('a.idproyecto','=',$id)
                ->select('a.id','ta.nombre as nombreCategoria','a.nombre_instalacion as instalacion','a.direccion','a.cant_puntos as puntos','a.codigo as codigoInterno',
                    'ta.id as categoria','tm.id as monitoreo','ts.id as servicio','ts.servicios as nombreServ',
                    'dis.id as distrito','pro.id as provincia','dep.id as departamento')->get();
               
            return ['proyecto'=>$proyecto,$agente];
        }
        public static function estadoProyecto($request){
            $estado=self::cambiarEstado($request);
            $detalle=self::addDetalles($request);
            if ($detalle){
                return ['success'=>true];
            }else{
                return ['error'=>false];
            }
        }
        public static function cambiarEstado($request){
            $model=Proyecto::where('id',$request['objeto']['idproyecto'])->first();
            $model->estado=$request['tipo'];
            $model->save();
            return $model;
        }
        public static function addDetalles($request){
           return ProyectoDetalle::create([
               'idproyecto'=>$request['objeto']['idproyecto'],
               'idpersona'=>Auth::user()->persona[0]->id,
               'motivo'=>$request['motivo']
           ]);
        }
        public static function ProyectoDetalle($id)
        {
            return DB::table('proyecto_detalle as pd')
                ->leftJoin('persona as p', 'p.id', '=', 'pd.idpersona')
                ->where('pd.idproyecto',$id)
                ->select('pd.id','pd.motivo',DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(pd.created_at,'%d/%m/%Y') as created_at"))
                ->orderBy('pd.id','desc')->paginate(15);
        }
        public static function buscarProyectoDetalle($buscar,$select){
            if ($select == 0){
                $fecha=explode(',',$buscar);
                return DB::table('proyecto_detalle as pd')
                    ->leftJoin('persona as p', 'p.id', '=', 'pd.idpersona')
                    ->whereBetween('pd.created_at',array($fecha[0],$fecha[1]))
                    ->select('pd.id','pd.motivo',DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(pd.created_at,'%d/%m/%Y') as created_at"))
                    ->orderBy('pd.id','desc')->paginate(15);
            }else{
                return DB::table('proyecto_detalle as pd')
                    ->leftJoin('persona as p', 'p.id', '=', 'pd.idpersona')
                    ->where('p.id',$buscar)
                    ->select('pd.id','pd.motivo',DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(pd.created_at,'%d/%m/%Y') as created_at"))
                    ->orderBy('pd.id','desc')->paginate(15);
            }
        }
        public static function getDashidProyecto($request){
            return DB::table('proyecto as p')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->join('agente as a','a.idproyecto','=','p.id')
                ->join('ms_tagente as ta','ta.id','=','a.idtagente')
                ->where('p.id',$request['idproyecto'])
                ->groupBy('a.idtagente')
                ->select('ta.nombre',DB::raw('count(a.idproyecto) as numero'))
                ->get();
        }
        public static function getDashboard(){
            $totalProyecto=Proyecto::count();
            $mes=Carbon::now()->monthName;
            $mesActual=Proyecto::where('mes',Carbon::now()->format('m'))->where('year',Carbon::now()->format('Y'))->count();
            $abierto=Proyecto::where('estado',0)->count();
            $cerrado=Proyecto::where('estado',1)->count();
            $empresa=DB::table('proyecto as p')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->where('p.estado',0)
                ->select('e.razon_social','p.idempresa','p.codigo','p.id',DB::raw("CONCAT(e.razon_social,'---',p.codigo) AS full_code"))
                ->get();
            $proyectos=DB::table('agente as a')
                ->join('ms_tagente as ta','ta.id','=','a.idtagente')
                ->select('ta.nombre',DB::raw('count(a.idtagente) as numero'))
                ->groupBy('a.idtagente')
                ->get();
            return ['total'=>$totalProyecto,'mes'=>$mes,'mesActual'=>$mesActual,'abierto'=>$abierto,
                'cerrado'=>$cerrado,'empresa'=>$empresa,'proyectos'=>$proyectos];
        }
        public static function eliminarProyecto($request){
            try {
                DB::beginTransaction();
                $agente=Agente::where('idproyecto',$request['objeto']['idproyecto'])->delete();
                $proyecto= Proyecto::where('id',$request['objeto']['idproyecto'])->delete();
                DB::commit();
                return response()->json(array("success" => true,'message'=>'Eliminado Correctamente'));
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error'=>false,'message'=>$e->getMessage()]);
            }
        }
        public static function obtenerCodigo($request){
            $sql = "call sp_Codigo_Proyecto_Automatico(?)";
            return DB::select($sql,array($request['idempresa']));
        }
        public static function buscarServicio($id){
            return DB::table('ms_tservicio')->where('id','=',$id)->select('servicios as nombre')->first();
        }
        public static function buscaragente($id){
            return DB::table('ms_tagente')->where('id','=',$id)->select('nombre')->first();
        }
        public static function deleteAgente($request){
            try {
                DB::beginTransaction();
                    Agente::where('id',$request['objeto']['id'])->delete();
                DB::commit();
                return response()->json(array("success" => true,'message'=>'Eliminado Correctamente'));
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error'=>false,'message'=>$e->getMessage()]);
            }
        }
        public static function validacion($request){
            if ($request['formulario']['lima'] == 0 and $request['formulario']['provincia'] == 0){
                $resp=false;
            }else{
                $resp=true;
            }
            return $resp;
        }
    }
