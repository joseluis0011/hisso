<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\Instalacion;
    use App\Models\Proyecto;
    use App\Models\RequerimientoProyecto;
    use App\Models\Trabajador;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;

    class RepoTrabajador
    {
        public static function getProyecto(){
            return Proyecto::where('estado',0)->get(['id','codigo','idempresa']);
        }
        public static function searchEmpres($request){
            return DB::table('proyecto as p')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->where('p.id',$request['id'])
                ->select('e.razon_social','e.id as idempresa')
                ->get();
        }
        public static function listarTrabajador($estado){
            return DB::table('requerimiento_proyecto as rq')
                ->join('agente as a','a.id','=','rq.idagente')
                ->join('ms_tagente as ta','ta.id','=','a.idtagente')
                ->join('proyecto as p','p.id','=','a.idproyecto')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->where('p.estado',$estado)
                ->select('a.id as idagente','p.codigo',DB::raw("CONCAT(a.codigo,'--',ta.nombre) AS codinterno"),'e.razon_social','rq.id as idrequerimiento',
                    'rq.tipo')
                ->orderBy('rq.id','desc')->paginate(15);
        }
        public static function buscarTrabajador($select,$buscar,$estado){
            if ($select == 1){
                return DB::table('proyecto as p')
                    ->join('empresa as e','e.id','=','p.idempresa')
                    ->join('requerimiento_proyecto as rq','rq.idproyecto','=','p.id')
                    ->where('e.id',$buscar)
                    ->where('p.estado',$estado)
                    ->select('p.id as idproyecto','p.codigo','e.razon_social','rq.id as idrequerimiento',
                        'rq.trabajador','rq.instalacion')
                    ->orderBy('rq.id','desc')->paginate(15);
            }else{
                return DB::table('proyecto as p')
                    ->join('empresa as e','e.id','=','p.idempresa')
                    ->join('requerimiento_proyecto as rq','rq.id','=','p.idproyecto')
                    ->where($select,'like','%'.$buscar.'%')
                    ->where('p.estado',$estado)
                    ->select('p.id as idproyecto','p.codigo','e.razon_social','rq.id as idrequerimiento',
                        'rq.trabajador','rq.instalacion')
                    ->orderBy('rq.id','desc')->paginate(15);
            }
        }
        public static function listarData($id,$estado){
            if ($estado == 'Instalacion'){
                return Instalacion::where('idproyecto',$id)->orderBy('id','desc')->paginate(20);
            }else{
                return Trabajador::where('idproyecto',$id)->orderBy('id','desc')->paginate(20);
            }
        }
        public static function SearchData($id,$estado,$select,$buscar){
            if ($estado == 'Instalacion'){
                return Instalacion::where('idproyecto',$id)
                    ->where($select,'like','%'.$buscar.'%')
                    ->orderBy('id','desc')->paginate(20);
            }else{
                return Trabajador::where('idproyecto',$id)
                    ->where($select,'like','%'.$buscar.'%')
                    ->orderBy('id','desc')->paginate(20);
            }
        }
        public static function agregarInstalacion($request){
            if ($request->estado == 'Instalacion'){
                return Instalacion::updateOrCreate(
                    [
                        'id' =>$request['id']
                    ],
                    [
                        'idproyecto'=>$request['idproyecto'],
                        'area_monitoreada'=>$request['formulario']['area'],
                        'instalacion'=>$request['formulario']['instalacion'],
                        'descripcion_instalacion'=>$request['formulario']['descripcion'],
                        'calle_numero'=>$request['formulario']['calle'],
                        'distrito'=>$request['formulario']['distrito'],
                        'area_geografica'=>$request['formulario']['geografica'],
                        'region_geografica'=>$request['formulario']['region'],
                        'usercreated'=>Auth::user()->persona[0]->id,
                    ]
                );
            }else{
                return Trabajador::updateOrCreate(
                    [
                        'id' =>$request['id']
                    ],
                    [
                    'idproyecto'=>$request['idproyecto'],
                    'dni'=>$request['formulario']['dni'],
                    'nombres_apellidos'=>$request['formulario']['nombres'],
                    'puesto_trabajo'=>$request['formulario']['puesto'],
                    'area_monitoreada'=>$request['formulario']['area'],
                    'instalacion'=>$request['formulario']['instalacion'],
                    'descripcion_instalacion'=>$request['formulario']['descripcion'],
                    'calle_numero'=>$request['formulario']['calle'],
                    'distrito'=>$request['formulario']['distrito'],
                    'area_geografica'=>$request['formulario']['geografica'],
                    'region_geografica'=>$request['formulario']['region'],
                    'usercreated'=>Auth::user()->persona[0]->id,
                ]);
            }
        }
        public static function getdata($request){
            if ($request->estado == 'Instalacion'){
                return Instalacion::where('idproyecto',$request['idproyecto'])->where('id',$request['id'])->get();
            }else{
                return Trabajador::where('idproyecto',$request['idproyecto'])->where('id',$request['id'])->get();
            }
        }
        public static function eliminar($request){
            if ($request->estado == 'Instalacion'){
                return Instalacion::where('id',$request['objeto']['id'])->delete();
            }else{
                return Trabajador::where('id',$request['objeto']['id'])->delete();
            }
        }
        public static function eliminarData($request){
            if ($request['objeto']['tipo'] == 'Instalacion'){
                $requerimiento = RequerimientoProyecto::where('tipo','Instalacion')
                    ->where('idproyecto',$request['objeto']['idproyecto'])->delete();
                $instalacion = Instalacion::where('idproyecto',$request['objeto']['idproyecto'])->delete();
                return $instalacion;
            }else{
                $requerimiento = RequerimientoProyecto::where('tipo','Trabajador')
                    ->where('idproyecto',$request['objeto']['idproyecto'])->delete();
                $trabajador = Trabajador::where('idproyecto',$request['objeto']['idproyecto'])->delete();
                return $trabajador;
            }
        }
        public static function agregarTrabajador($request)
        {
            try {
                DB::beginTransaction();
                Trabajador::create([
                    'idagente' => $request['form']['codigo'],
                    'dni' => $request['form']['dni'],
                    'nombres_apellidos' => $request['form']['nombres'],
                    'puesto_trabajo' => $request['form']['trabajo'],
                    'area_monitoreada' => $request['form']['descripcion'],
                    'instalacion' => $request['form']['instalacion'],
                    'descripcion_instalacion' => $request['form']['area'],
                    'calle_numero' => isset($request['form']['calle']) ? $request['form']['calle'] : '',
                    'distrito' => isset($request['form']['distrito']) ? $request['form']['distrito'] : '',
                    'area_geografica' =>isset($request['form']['area_geografica']) ? $request['form']['area_geografica'] : '' ,
                    'region_geografica' =>isset($request['form']['region_geografica']) ? $request['form']['region_geografica'] : '' ,
                    'estado' => 0,
                    'usercreated' => Auth::user()->id
                ]);
                RequerimientoProyecto::create([
                    'idagente'=>$request['form']['codigo'],
                    'tipo'=>'Trabajador'
                ]);
                DB::commit();
                return response()->json(array("success" => true, 'message' => 'Guardado Correctamente'));
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error' => false, 'message' => $e->getMessage()]);
            }
        }
        public static function agregarInstalacionp($request){
            try {
                DB::beginTransaction();
                    Instalacion::create([
                        'idproyecto'=>$request['form']['codigo'],
                        'area_monitoreada'=>$request['form']['area'],
                        'instalacion'=>$request['form']['instalacion'],
                        'descripcion_instalacion'=>$request['form']['descripcion'],
                        'calle_numero'=>isset($request['form']['calle']) ? $request['form']['calle'] : '',
                        'distrito'=>isset($request['form']['distrito']) ? $request['form']['distrito'] : '',
                        'area_geografica'=>isset($request['form']['area_geografica']) ? $request['form']['area_geografica'] : '',
                        'region_geografica'=>isset($request['form']['region_geografica']) ? $request['form']['region_geografica'] : '' ,
                        'usercreated'=>Auth::user()->persona[0]->id,
                    ]);
                RequerimientoProyecto::create([
                    'idproyecto'=>$request['form']['codigo'],
                    'tipo'=>'Instalacion'
                ]);
                DB::commit();
                return response()->json(array("success" => true, 'message' => 'Guardado Correctamente'));
            }catch (\Exception $e){
                DB::rollback();
                return response()->json(['error' => false, 'message' => $e->getMessage()]);
            }
        }
    }
