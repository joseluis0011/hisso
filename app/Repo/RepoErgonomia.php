<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\DetErgonomia;
    use App\Models\DetIluminacion;
    use App\Models\Proyecto;
    use App\Models\Trabajador;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Str;

    class RepoErgonomia
    {
        public static function listTrabajador($id)
        {
            return DB::table('trabajador as t')
                ->leftJoin('det_ergonomia as de', 'de.idtrabajador', '=', 't.id')
                ->where('t.idproyecto',$id)
                ->select('t.id','t.idproyecto','t.nombres','t.apellido_paterno','t.apellido_materno','t.puesto_trabajo',
                    'de.estado')
                ->orderBy('t.id','desc')->paginate(20);
        }
        public static function searchTrabajador($id,$select,$buscar){
            return DB::table('trabajador as t')
                ->leftJoin('det_iluminacion as di', 'di.idtrabajador', '=', 't.id')
                ->where('t.idproyecto',$id)
                ->where($select,'like','%'.$buscar.'%')
                ->select('t.id','t.idproyecto','t.nombres','t.apellido_paterno','t.apellido_materno','t.puesto_trabajo',
                    'di.estado')
                ->orderBy('t.id','desc')->paginate(20);
        }
        public static function addDetErgonomia($request){
            if ($request->file('file')){
                $logo=$request->file;
                $random = Str::random(10);
                $filename = $logo->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/ergonomia',$logo,$fileserver);
                $file=asset('storage/ergonomia/'.$fileserver);
            }else{
                $file=$request->file;
            }
            if ($request->file('file1')){
                $logo1=$request->file1;
                $random = Str::random(10);
                $filename = $logo1->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/ergonomia',$logo1,$fileserver);
                $file1=asset('storage/ergonomia/'.$fileserver);
            }else{
                $file1=$request->file1;
            }
            if ($request->file('file2')){
                $logo2=$request->file2;
                $random = Str::random(10);
                $filename = $logo2->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/ergonomia',$logo2,$fileserver);
                $file2=asset('storage/ergonomia/'.$fileserver);
            }else{
                $file2=$request->file2;
            }
            if ($request->file('file3')){
                $logo3=$request->file3;
                $random = Str::random(10);
                $filename = $logo3->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/ergonomia',$logo3,$fileserver);
                $file3=asset('storage/ergonomia/'.$fileserver);
            }else{
                $file3=$request->file3;
            }
            if ($request->file('file4')){
                $logo4=$request->file4;
                $random = Str::random(10);
                $filename = $logo4->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/ergonomia',$logo4,$fileserver);
                $file4=asset('storage/ergonomia/'.$fileserver);
            }else{
                $file4=$request->file4;
            }
            return DetErgonomia::updateOrCreate(
                [
                    'id'=>$request['id']
                ],
                [
                    'idtrabajador'=>$request['idtrabajador'],
                    'idproyecto'=>$request['idproyecto'],
                    'punto_monitoreo'=>$request['punto_monitoreo'],
                    'fecha_monitoreo'=>$request['fecha_monitoreo'],
                    'hora_monitoreo'=>$request['hora_monitoreo'],
                    'trabajadores_expuestos'=>$request['trabajadores_expuestos'],
                    'codigo'=>$request['codigo'],
                    'edad'=>$request['edad'],
                    'puesto'=>$request['puesto'],
                    'hoario_trabajo'=>$request['hoario_trabajo'],
                    'turno_rotativo'=>$request['turno_rotativo'],
                    'peso_talla'=>$request['peso_talla'],
                    'porcentaje_hora_campo'=>$request['porcentaje_hora_campo'],
                    'trabajo_campo_1'=>$request['trabajo_campo_1'],
                    'trabajo_campo_2'=>$request['trabajo_campo_2'],
                    'trabajo_campo_3'=>$request['trabajo_campo_3'],
                    'trabajo_campo_4'=>$request['trabajo_campo_4'],
                    'trabajo_campo_5'=>$request['trabajo_campo_5'],
                    'porcentaje_hora_oficina'=>$request['porcentaje_hora_oficina'],
                    'trabajo_oficina_1'=>$request['trabajo_oficina_1'],
                    'trabajo_oficina_2'=>$request['trabajo_oficina_2'],
                    'trabajo_oficina_3'=>$request['trabajo_oficina_3'],
                    'trabajo_oficina_4'=>$request['trabajo_oficina_4'],
                    'trabajo_oficina_5'=>$request['trabajo_oficina_5'],
                    'dolencia_fisica'=>$request['dolencia_fisica'],
                    'discapacidad'=>$request['discapacidad'],
                    'gestacion'=>$request['gestacion'],
                    'altura_asiento'=>$request['altura_asiento'],
                    'altura_espaldar'=>$request['altura_espaldar'],
                    'alcolchonado_recubierto'=>$request['alcolchonado_recubierto'],
                    'reposabrazos'=>$request['reposabrazos'],
                    'largo'=>$request['largo'],
                    'ancho'=>$request['ancho'],
                    'superficie'=>$request['superficie'],
                    'colocacion_posicion'=>$request['colocacion_posicion'],
                    'espacio_piernas'=>$request['espacio_piernas'],
                    'espacio_silla'=>$request['espacio_silla'],
                    'altura_pantalla_laptop'=>$request['altura_pantalla_laptop'],
                    'altura_pantalla_pc'=>$request['altura_pantalla_pc'],
                    'distancia_cabeza'=>$request['distancia_cabeza'],
                    'foto1'=>$file,
                    'foto2'=>$file1,
                    'foto3'=>$file2,
                    'foto4'=>$file3,
                    'foto5'=>$file4,
                    'estado'=>1
                ]
            );
        }
       public static function getDetalleErgominia($request){
            return DetErgonomia::where('idtrabajador',$request['idtrabajador'])->get();
       }
    }
