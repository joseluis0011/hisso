<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\DetRuido;
    use App\Models\Instalacion;
    use App\Models\Proyecto;
    use App\Models\Trabajador;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Str;

    class RepoRuido
    {
        public static function listInstalacion($id){
            return DB::table('instalacion as i')
                ->leftJoin('det_ruido as dr','dr.idinstalacion','=','i.id')
                ->where('i.idproyecto',$id)
                ->select('i.id','i.idproyecto','i.area_monitoreada','i.instalacion','i.descripcion_instalacion','i.area_geografica','dr.estado')
                ->orderBy('i.id','desc')->paginate(20);
        }
        public static function searchInstalacion($id,$select,$buscar){
            return DB::table('instalacion as i')
                ->leftJoin('det_ruido as dr','dr.idinstalacion','=','i.id')
                ->where('i.idproyecto',$id)
                ->where($select,'like','%'.$buscar.'%')
                ->select('i.id','i.idproyecto','i.area_monitoreada','i.instalacion','i.descripcion_instalacion','i.area_geografica','dr.estado')
                ->orderBy('i.id','desc')->paginate(20);
        }
        public static function getDetalleRuido($request){
            return DB::table('instalacion as i')
                ->join('proyecto as p','p.id','=','i.idproyecto')
                ->where('i.id',$request['idinstalacion'])
                ->select('i.area_monitoreada','i.instalacion','i.calle_numero','p.codigo')
                ->get();
        }
        public static function addDetalleRuido($request){
            if ($request->file('file')){
                $logo=$request->file;
                $random = Str::random(10);
                $filename = $logo->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/ruido',$logo,$fileserver);
                $file=asset('storage/ruido/'.$fileserver);
            }else{
                $file=$request->file;
            }
          return DetRuido::updateOrCreate(
            [
                'id'=>$request['id']
            ],
            [
                'idproyecto'=>$request['idproyecto'],
                'idinstalacion'=>$request['idinstalacion'],
                'punto_monitoreo'=>$request['punto'],
                'fecha_monitoreo'=>$request['fecha'],
                'hora_monitoreo'=>$request['hora'],
                'fuente_ruido'=>$request['fuente_ruido'],
                'foto'=>$file,
                'numero_trabajadores'=>$request['trabajadores_expuestos'],
                'min1'=>$request['min'],
                'min2'=>$request['min1'],
                'min3'=>$request['min2'],
                'max1'=>$request['max'],
                'max2'=>$request['max1'],
                'max3'=>$request['max2'],
                'lequiv1'=>$request['lequiv'],
                'lequiv2'=>$request['lequiv1'],
                'lequiv3'=>$request['lequiv2'],
                'actividades'=>$request['actividad_medicion'],
                'estado'=>1
            ]
          );
        }
        public static function getRuido($request){
            return DetRuido::where('idinstalacion',$request['idinstalacion'])->get();
        }
    }
