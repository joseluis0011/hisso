<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\Empresa;
    use App\Models\Epp;
    use App\Models\EppEquipos;
    use App\Models\EppEquiposDetalle;
    use App\Models\EppTrabajador;
    use App\Models\Proyecto;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;

    class RepoEpp
    {
        public static function listar($id){
            return DB::table('epp')
                ->where('idagente',$id)
                ->orderBy('id','desc')->paginate(12);
        }
        public static function search($id,$select,$busqueda){
            return DB::table('epp')
                ->where('idagente',$id)
                ->where($select,'like','%'.$busqueda.'%')
                ->orderBy('id','desc')->paginate(12);
        }
        public static function addeppEquipos($request){
         return EppEquipos::updateOrCreate(
             [
                'id'=>$request['id']
             ],
             [
                 'idagente'=>$request['idagente'],
                 'codigo'=>$request['formulario']['codigo'],
                 'nombre'=>$request['formulario']['nombre'],
                  'frecuencia'=>$request['formulario']['frecuencia']
             ]
         );
        }
        public static function listEquiposforEpp($request){
            return DB::table('epp_equipos')
                ->where('idagente',$request['idagente'])
                ->select(DB::raw("CONCAT(codigo,'--',nombre) AS full_name"),'codigo','nombre','id')
                ->get();
        }
        public static function generarcodigoEpp($request){
            $sql = "call sp_Generar_Codigo_epp(?)";
            return DB::select($sql,array($request['idagente']));
        }
        public static function addEpp($request){
            return Epp::create([
               'idagente'=>$request['codigo'],
                'entrega'=>$request['entrega'],
                'tipo_equipo'=>$request['tipo_equipo'],
                'usercreated'=>Auth::user()->id
            ]);
        }
        public static function listarTrabajador($id,$idepp){
            return DB::table('epp_trabajador')
                ->where('idagente',$id)
                ->where('idepp',$idepp)
                ->select('id','idepp','idagente','nombres','dni','area',DB::raw("DATE_FORMAT(fecha_entrega,'%d/%m/%Y') as fecha_entrega"))
                ->orderBy('id','desc')->paginate(20);
        }
        public static function searchTrabajador($id,$select,$buscar){
            return DB::table('epp_trabajador')
                ->where('idagente',$id)
                ->where('dni','like','%'.$buscar.'%')
                ->orderBy('id','desc')->paginate(20);
        }
        public static function updateTrabajadores($request){
            $arrayList = $request['arrayList'];
            for($i = 0; $i < count($arrayList); $i++){
                DB::table('epp_trabajador')->where('id',$arrayList[$i]['id'])->update(
                     ['fecha_entrega'=>$arrayList[$i]['fecha_entrega']]
                 );
            }
        }
        public static function updateTrabajador($request){
            return DB::table('epp_trabajador')->where('id',$request['id'])->update(
                ['fecha_entrega'=>$request['value']]
            );
        }
        public static function addTrabajador($request){
            return EppTrabajador::updateOrCreate(
                [
                    'id'=>$request['id']
                ],
                [
                    'idepp'=>$request['idepp'],
                    'idagente'=>$request['idagente'],
                    'nombres'=>$request['form']['nombres'],
                    'dni'=>$request['form']['dni'],
                    'area'=>$request['form']['area'],
                    'fecha_entrega'=>$request['form']['fecha_entrega']
                ]
            );
        }
        public static function getTrabajador($request){
            return EppTrabajador::where('id',$request['idtrabajador'])->get();
        }
        public static function deleteTrabajador($request){
            return EppTrabajador::where('id',$request['objeto']['id'])->delete();
        }
        public static function puestoTrabajo($request){
            return EppTrabajador::where('idepp',$request['idepp'])->where('idagente',$request['idagente'])->select('id','puesto_trabajo')->get();
        }
        // equipos epp
        public static function generarcodigoEquipo($request){
            $sql = "call sp_Generar_Codigo_epp_equipos(?)";
            return DB::select($sql,array($request['idagente']));
        }
        public static function listarEquipo($idproyecto){
            return DB::table('epp_equipos')
                ->where('idagente',$idproyecto)
                ->orderBy('id','desc')->paginate(12);
        }
        public static function searchEquipo($select,$buscar){
            return DB::table('epp_equipos')
                ->where('codigp','like','%'.$buscar.'%')
                ->orderBy('id','desc')->paginate(12);
        }
        public static function validadorEquipoTrabajador($request){
            return DB::table('epp_trabajador_equipos')
                ->where('idepp',$request['idepp'])
                ->first();
        }
        public static function addEquipoTrabajador($request){
            try{
                DB::beginTransaction();
                foreach ($request['equipos'] as $equipo) {
                    DB::table('epp_trabajador_equipos')->insert(
                        ['idequipo' => $equipo, 'idagente' => $request['idagente'], 'idepp' => $request['idepp'], 'idtrabajador' => $request['puesto_trabajo']]
                    );
                }
                DB::commit();
                return response()->json(array("success" => true,'message'=>'Guardado Correctamente'));
            }catch (\Exception $e){
                DB::rollback();
                return response()->json(['error'=>false,'message'=>$e->getMessage()]);
            }
        }
        public static function listEquipoTrabajador($request){
            return DB::table('epp_trabajador_equipos as et')
                ->join('epp_equipos as ee','ee.id','=','et.idequipo')
                ->join('epp_trabajador as etra','etra.id','=','et.idtrabajador')
                ->where('et.idagente',$request['idagente'])
                ->where('et.idepp',$request['idepp'])
                ->select('ee.codigo','ee.nombre','et.id','etra.puesto_trabajo')
                ->get();
        }
        public static function eliminarEquipoTrabajador($request){
            $data = DB::table('epp_trabajador_equipos')->where('id',$request['objeto']['id'])->delete();
            if ($data){
                $res = ['success'=>true,'message'=>'Eliminado Correctamente'];
            }else{
                $res = ['success'=>false,'message'=>'Error'];
            }
            return $res;
        }
        public static function eliminarEquipo($request){
            $data = DB::table('epp_equipos')->where('id',$request['objeto']['id'])->delete();
            if ($data){
                $res = ['success'=>true,'message'=>'Eliminado Correctamente'];
            }else{
                $res = ['success'=>false,'message'=>'Error'];
            }
            return $res;
        }
        // export formato ley
        public static function getempresa($id){
            return Empresa::where('id',$id)->get();
        }
        public static function getEpp($id){
            return Epp::where('id',$id)->get();
        }
        public static function getproyecto($id){
            return Proyecto::where('id',$id)->get();
        }
        public static function getEquipos($id){
            $data = DB::table('epp_trabajador_equipos as et')
                ->join('epp_equipos as e','e.id','=','et.idequipo')
                ->where('et.idepp',$id)->select('e.nombre')->get();
            return $data->toArray();
        }
    }
