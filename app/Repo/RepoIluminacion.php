<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\DetIluminacion;
    use App\Models\Proyecto;
    use App\Models\Trabajador;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Str;
    use Intervention\Image\Facades\Image;

    class RepoIluminacion
    {
        public static function listTrabajador($id)
        {
            return DB::table('trabajador as t')
                ->leftJoin('det_iluminacion as di', 'di.idtrabajador', '=', 't.id')
                ->where('t.idproyecto',$id)
                ->select('t.id','t.idproyecto','t.nombres','t.apellido_paterno','t.apellido_materno','t.puesto_trabajo',
                    'di.estado')
                ->orderBy('t.id','desc')->paginate(20);
        }
        public static function searchTrabajador($id,$select,$buscar){
            return DB::table('trabajador as t')
                ->leftJoin('det_iluminacion as di', 'di.idtrabajador', '=', 't.id')
                ->where('t.idproyecto',$id)
                ->where($select,'like','%'.$buscar.'%')
                ->select('t.id','t.idproyecto','t.nombres','t.apellido_paterno','t.apellido_materno','t.puesto_trabajo',
                    'di.estado')
                ->orderBy('t.id','desc')->paginate(20);
        }
        public static function getDetalleIluminacion($request){
            return DB::table('trabajador as t')
                ->join('proyecto as p','p.id','=','t.idproyecto')
                ->where('t.id',$request['idtrabajador'])
                ->select('t.nombres','t.dni','p.codigo')
                ->get();
        }
        public static function addDetalleIluminacion($request){
            if ($request->file('file')){
                $logo=$request->file;
                $random = Str::random(10);
                $filename = $logo->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo,$fileserver);
                $file=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file=$request->file;
            }
            //------------------------------------------------------
            if ($request->file('file1')){
                $logo1=$request->file1;
                $random = Str::random(10);
                $filename = $logo1->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo1,$fileserver);
                $file1=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file1=$request->file1;
            }
            //------------------------------------------------------
            if ($request->file('file2')){
                $logo2=$request->file2;
                $random = Str::random(10);
                $filename = $logo2->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo2,$fileserver);
                $file2=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file2=$request->file2;
            }
            //------------------------------------------------------
            if ($request->file('file3')){
                $logo3=$request->file3;
                $random = Str::random(10);
                $filename = $logo3->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo3,$fileserver);
                $file3=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file3=$request->file3;
            }
            //------------------------------------------------------
            if ($request->file('file4')){
                $logo4=$request->file4;
                $random = Str::random(10);
                $filename = $logo4->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo4,$fileserver);
                $file4=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file4=$request->file4;
            }
            //------------------------------------------------------
            if ($request->file('file5')){
                $logo5=$request->file5;
                $random = Str::random(10);
                $filename = $logo5->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo5,$fileserver);
                $file5=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file5=$request->file5;
            }
            //------------------------------------------------------
            if ($request->file('file6')){
                $logo6=$request->file6;
                $random = Str::random(10);
                $filename = $logo6->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo6,$fileserver);
                $file6=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file6=$request->file6 ;
            }
            //------------------------------------------------------
            if ($request->file('file7')){
                $logo7=$request->file7;
                $random = Str::random(10);
                $filename = $logo7->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo7,$fileserver);
                $file7=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file7=$request->file7;
            }
            //------------------------------------------------------
            if ($request->file('file8')){
                $logo8=$request->file8;
                $random = Str::random(10);
                $filename = $logo8->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo8,$fileserver);
                $file8=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file8=$request->file8;
            }
            //------------------------------------------------------
            if ($request->file('file9')){
                $logo9=$request->file9;
                $random = Str::random(10);
                $filename = $logo9->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/iluminacion',$logo9,$fileserver);
                $file9=asset('storage/iluminacion/'.$fileserver);
            }else{
                $file9=$request->file9;
            }
            return DetIluminacion::updateOrCreate(
                [
                    'id'=>$request['idiluminacion']
                ],
                [
                    'idproyecto'=>$request['idproyecto'],
                    'idtrabajador'=>$request['idtrabajador'],
                    'punto_monitoreo'=>$request['punto'],
                    'fecha_monitoreo'=>$request['fecha'],
                    'hora_monitoreo'=>$request['hora'],
                    'max1'=>$request['max'],
                    'max2'=>$request['max1'],
                    'max3'=>$request['max2'],
                    'min1'=>$request['min'],
                    'min2'=>$request['min1'],
                    'min3'=>$request['min2'],
                    'avg1'=>$request['avg'],
                    'avg2'=>$request['avg1'],
                    'avg3'=>$request['avg2'],
                    'tipo_iluminaria'=>$request['tipo_iluminaria'],
                    'altura_iluminaria'=>$request['altura_iluminaria'],
                    'trabajo_realizado'=>$request['trabajo_realizado'],
                    'caracteristicas_entorno'=>$request['caracteristica'],
                    'numero_trabajadores'=>$request['trabajadores_expuestos'],
                    'foto1'=>$file,
                    'foto2'=>$file1,
                    'foto3'=>$file2,
                    'foto4'=>$file3,
                    'foto5'=>$file4,
                    'foto6'=>$file5,
                    'foto7'=>$file6,
                    'foto8'=>$file7,
                    'foto9'=>$file8,
                    'foto10'=>$file9,
                    'estado'=>1
                ]
            );
        }
        public static function getIluminacion($request){
            return DetIluminacion::where('idtrabajador',$request['idtrabajador'])->get();
        }
    }
