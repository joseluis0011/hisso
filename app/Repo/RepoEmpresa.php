<?php


namespace App\Repo;


use App\Models\Departamento;
use App\Models\Distrito;
use App\Models\Empresa;
use App\Models\EmpresaContacto;
use App\Models\HProyectoEmpresa;
use App\Models\Provincia;
use App\Models\Ubigeo;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RepoEmpresa
{   /*https://api.sunat.cloud/ruc/*/
    public static function apiRuc($ruc){
        $data = file_get_contents("https://api.apis.net.pe/v1/ruc?numero=" . $ruc);
        $info = json_decode($data, true);
        if ($data === "" || $info['estado'] === 'INACTIVO') {
            return ['nada'=>false];
        }else{
            $datos = array(
                0 => $info['nombre'],
                1 => $info['direccion'],
                2 =>$info['departamento'],
                3=>$info['provincia'],
                4 =>$info['distrito']
            );
            return self::searchUbigeo($datos);
        }
    }
    public static function searchUbigeo($datos){
        $depa=self::searchDepatamento($datos);
        $prov=self::searchProvincia($datos);
        $dist=self::searchDistrito($datos);
        return ['data'=>$datos,'depa'=>$depa,'prov'=>$prov,'distrito'=>$dist];
    }
    public static function searchDepatamento($data){
        $dep= Departamento::where('departamento',$data[2])->select('id as departamento')->get();
        return $dep[0];
    }
    public static function searchProvincia($data){
        $prov =Provincia::where('provincia',$data[3])->select('id as provincia')->get();
        return $prov[0];
    }
    public static function searchDistrito($data){
        $dist=Distrito::where('distrito',$data[4])->select('id as distrito')->get();
        return $dist[0];
    }
    public static function searchRuc($ruc){
        return Empresa::where('ruc',$ruc)->first();
    }
    public static function agregarUbigeo($request){
        return Ubigeo::updateOrCreate(
            [
                'id'=>$request['idubigeo']
            ],
            [
                'iddepartamento'=>$request['formulario']['departamento'],
                'idprovincia'=>$request['formulario']['provincia'],
                'iddistrito'=>$request['formulario']['distrito'],
            ]
        );
    }
    public static function agregarEmpresa($request,$idubigeo){
        return Empresa::updateOrCreate(
            [
                'id'=>$request['idempresa']
            ],
            [
                'razon_social'=>$request['formulario']['razon_social'],
                'ruc'=>$request['formulario']['ruc'],
                'act_economica'=>$request['formulario']['actividad_economica'],
                'domicilio_fiscal'=>$request['formulario']['domicilio_fiscal'],
                'comentario'=>$request['formulario']['comentarios'],
                'usercreated'=>Auth::user()->id,
                'estado'=>0,
                'idubigeo'=>$idubigeo,
                'mes'=>Carbon::now()->format('m'),
                'year'=>Carbon::now()->format('Y')
            ]
        );
    }
    public static function agregarContacto($request,$idempresa){
        try{
            $arrayList =$request['contacto'];
            for($i = 0; $i < count($arrayList); $i++){
                EmpresaContacto::updateOrCreate(
                    [
                        'id'=>$arrayList[$i]['id']
                    ],
                    [
                        'idempresa' => $idempresa,
                        'nombre' => $arrayList[$i]['nombre'],
                        'correo' => $arrayList[$i]['correo'],
                        'celular' => $arrayList[$i]['celular'],
                        'telefono' => isset($arrayList[$i]['telefono']) ? $arrayList[$i]['telefono'] : null,
                        'cargo' => $arrayList[$i]['cargo'],
                        'usercreated' => Auth::user()->id,

                    ]
                );
            }
            return true;
        }catch (\Exception $e){
            return false;
        }
    }
    public static function hEmpresaProyectoTrabajadores($request,$idempresa){
        try{
            DB::beginTransaction();
            $arrayList =$request['trabajadores'];
            for($i = 0; $i < count($arrayList); $i++){
                HProyectoEmpresa::updateOrCreate(
                    [
                        'id'=>$arrayList[$i]['id']
                    ],
                    [
                        'mes' => $arrayList[$i]['idmes'],
                        'year' => $arrayList[$i]['year'],
                        'idempresa' => $idempresa,
                        'cant_trab_emp_proy' => $arrayList[$i]['numero'],
                        'domicilio_fiscal' => isset($arrayList[$i]['domicilio_fiscal']) ? $arrayList[$i]['domicilio_fiscal'] : null,
                        'usercreated' => Auth::user()->id,
                
                    ]
                );
            }
            DB::commit();
            return response()->json(array("success" => true));
        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['error'=>false,'message'=>$e->getLine()]);
        }
    }
    public static function getdata($id){
        $empresa = self::getEmpresa($id);
        $contacto = self::getContacto($id);
        $sql= "call sp_Convertir_fecha(?)";
        $hproyectoEmpresa = DB::select($sql,array($id));
        return ['empresa'=>$empresa,'contacto'=>$contacto,'hempresa'=>$hproyectoEmpresa];
    }
    public static function getEmpresax(){
        return Empresa::select('id','razon_social')->get();
    }
    public static function getEmpresa($idempresa){
        return DB::table('empresa as e')
            ->join('ubigeo as u','u.id','=','e.idubigeo')
            ->where('e.id',$idempresa)
            ->select('e.id as idempresa','e.razon_social','e.ruc','e.act_economica','e.domicilio_fiscal','e.comentario',
                'u.id as idubigeo','u.iddepartamento','u.idprovincia','u.iddistrito')->get();
    }
    public static function getContacto($idempresa){
        return EmpresaContacto::where('idempresa',$idempresa)->get(['id','nombre','correo','celular','telefono','cargo']);
    }
    public static function dashboard(){
        $total= Empresa::count();
        $mesActual = Empresa::where('mes','=',Carbon::now()->format('m'))->where('year',Carbon::now()->format('Y'))->count();
        $mes=Carbon::now()->monthName;
        return ['total'=>$total,'mesActual'=>$mesActual,'mes'=>$mes,
            [
            self::fechaxMeses('01'),
            self::fechaxMeses('02'),
            self::fechaxMeses('03'),
            self::fechaxMeses('04'),
            self::fechaxMeses('05'),
            self::fechaxMeses('06'),
            self::fechaxMeses('07'),
            self::fechaxMeses('08'),
            self::fechaxMeses('09'),
            self::fechaxMeses('10'),
            self::fechaxMeses('11'),
            self::fechaxMeses('12'),
            ]
        ];
    }
    public static function fechaxMeses($mes){
        return Empresa::where('mes','=',Carbon::now()->format($mes))
            ->where('year','=',Carbon::now()->format('Y'))
            ->count();
    }
    public static function deleteEmpresa($request){
        try{
            DB::beginTransaction();
            EmpresaContacto::where('id',$request['objeto']['id'])->delete();
            DB::commit();
            return response()->json(array("success" => true,'message'=>'Eliminado Correctamente'));
        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['error'=>false,'message'=>$e->getMessage()]);
        }
    }
}
