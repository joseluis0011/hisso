<?php


namespace App\Repo;


use App\Models\DetalleEquipo;
use App\Models\Empresa;
use App\Models\Equipo;
use App\Models\Persona;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RepoEquipo
{
    public static function agregarEquipo($request){
        return Equipo::updateOrCreate(
            [
                'id'=>$request['id']
            ],
            [
                'nombre'=>$request['formulario']['nombre'],
                'marca'=>$request['formulario']['marca'],
                'modelo'=>$request['formulario']['modelo'],
                'serie'=>$request['formulario']['serie'],
                'fecha_calibracion'=>$request['formulario']['fecha'],
                'estado'=>0,
                'mes'=>Carbon::now()->format('m'),
                'year'=>Carbon::now()->format('Y')
            ]
        );
    }
    public static function estadoEquipo($estado,$condicion){
        if ($condicion == 1){
            return Equipo::whereIn('estado',$estado)
                ->select('*',DB::raw("DATE_FORMAT(fecha_calibracion,'%d/%m/%Y') as fechacalibracion,DATE_FORMAT(fecha_termino,'%d/%m/%Y') as fechatermino")
            )->orderBy('id','desc')->paginate(15);
        }else{
            return Equipo::where('estado',$estado)->get();
        }
    }
    public static function buscarEquipo($estado,$buscar,$select){
        return Equipo::where($select,'like','%'.$buscar.'%')
            ->whereIn('estado',$estado)
            ->select('*',DB::raw("DATE_FORMAT(fecha_calibracion,'%d/%m/%Y') as fecha"))
            ->orderBy('id','desc')->paginate(15);

    }
    public static function estado($request){
        if ($request['tipo']== 0){
            $estado=self::cambiarEstadoEquipo($request);
            $addequipo=self::addEquipos($request['objeto']['id'],$request['motivo']);
            if ($addequipo){
                return ['success'=>true];
            }else{
                return ['error'=>false];
            }
        }else{
            $data=Equipo::where('id',$request['objeto']['id'])->first();
            $data->estado=0;
            $data->save();
            if ($data){
                return ['success'=>true];
            }else{
                return ['error'=>false];
            }
        }
    }
    public static function cambiarEstadoEquipo($request){
        $data=Equipo::where('id',$request['objeto']['id'])->first();
        $data->estado=1;
        $data->save();
        return $data;
    }
    public static function addEquipos($idquipo,$motivo){
        return DetalleEquipo::create([
            'idequipo'=>$idquipo,
            'idpersona'=>Auth::user()->persona[0]->id,
            'motivo'=>$motivo
        ]);
    }
    public static function getdata($id){
        return Equipo::where('id',$id)->first();
    }
    public static function estadoEquipoDetalle($condicion,$id){
        if ($condicion == 1){
            return  DB::table('equipos_detalle as ed')
                ->leftJoin("persona as p",'p.id','=','ed.idpersona')
                ->where('ed.idequipo',$id)
                ->select('ed.idequipo','ed.motivo',
                    DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(ed.created_at,'%d/%m/%Y') as created_at"))
                ->orderBy('ed.id','desc')->paginate(15);
        }else{
            return  DB::table('equipos_detalle as ed')
                ->join("persona as p",'p.id','=','ed.idpersona')
                ->where('ed.idequipo',$id)
                ->select('ed.id','p.nombre','p.apellido','ed.motivo')
                ->get();
        }
    }
    public static function buscarEquipoDetalle($buscar,$select){
        if ($select == 0){
            $fecha=explode(',',$buscar);
            return  DB::table('equipos_detalle as ed')
                ->join("persona as p",'p.id','=','ed.idpersona')
                ->whereBetween('ed.created_at',array($fecha[0],$fecha[1]))
                ->select('ed.id','ed.motivo', DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(ed.created_at,'%d/%m/%Y') as created_at"))
                ->orderBy('ed.id','desc')->paginate(15);
        }else{
            return  DB::table('equipos_detalle as ed')
                ->join("persona as p",'p.id','=','ed.idpersona')
                ->where('p.id',$buscar)
                ->select('ed.id','ed.motivo', DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,DATE_FORMAT(ed.created_at,'%d/%m/%Y') as created_at"))
                ->orderBy('ed.id','desc')->paginate(15);
        }
    }
    public static function getPersona(){
        return Persona::where('estado',0)->get();
    }
    public static function getDashboard(){
        $mes=Carbon::now()->monthName;
        $year=Carbon::now()->format('Y');
        $data=DB::select("select UPPER(`nombre`) as equipo, count(nombre) as cantidad from equipos where year=$year and estado=0 group by nombre having cantidad > 0");
        $total=Equipo::count();
        $equipos=Equipo::where('estado',0)->get(['nombre','serie','id']);
        $mesActual = Equipo::where('mes','=',Carbon::now()->format('m'))->where('year',Carbon::now()->format('Y'))->count();
        return ['mes'=>$mes,'data'=>$data,'total'=>$total,'mesActual'=>$mesActual,
            'activo'=>self::estadoDEquipo(0),'inactivo'=>self::estadoDEquipo(1),'equipo'=>$equipos
        ];
        // $data= DB::select("SELECT LEFT(UPPER(`nombre`), INSTR( UPPER(`nombre`),' ') -1 ) AS equipo, COUNT(LEFT(UPPER(`nombre`), INSTR(UPPER(`nombre`),' ') -1)) AS cantidad FROM equipos where year=$year group by equipo having cantidad > 0");
    }
    public static function estadoDEquipo($estado){
        return Equipo::where('estado',$estado)->count();
    }
    public static function getequipo($request){
        return Equipo::where('id',$request['idequipo'])->select('nombre','marca','modelo','serie',DB::raw("DATE_FORMAT(fecha_calibracion,'%d/%m/%Y') as fecha_calibracion"),DB::raw("DATE_FORMAT(fecha_termino,'%d/%m/%Y') as fecha_termino"))->get();
    }
}
