<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\Agente;
    use App\Models\Cargo;
    use App\Models\Departamento;
    use App\Models\Distrito;
    use App\Models\Empresa;
    use App\Models\Persona;
    use App\Models\Provincia;
    use App\Models\Proyecto;
    use App\Models\Rol;
    use Illuminate\Support\Facades\DB;

    class RepoComun
    {
        public static function searchCodigo($request){
            if (self::searchProyecto($request)){
                return self::searchProyecto($request);
            }else{
                return ['success'=>false];
            }
        }
        public static function searchProyecto($request){
            $id= DB::table('agente as a')
                ->join('ms_tagente as ta','ta.id','=','a.idtagente')
                ->where('a.codigo',$request['codigo'])
                ->where('ta.nombre',$request['tipo'])
                ->first(['a.id']);
            if ($id){
                return ['success'=>true,'id'=>$id];
            }else{
                return ['success'=>false];
            }
        }
        public static function getObtenerDataById($request){
            if ($request['idinstalacion']){
                return DB::table('instalacion as i')
                    ->join('proyecto as p','p.id','=','i.idproyecto')
                    ->where('i.id',$request['idinstalacion'])
                    ->select('i.area_monitoreada','i.instalacion','i.calle_numero','p.codigo')
                    ->get();
            }else{
                return DB::table('trabajador as t')
                    ->join('proyecto as p','p.id','=','t.idproyecto')
                    ->where('t.id',$request['idtrabajador'])
                    ->select('t.nombres','t.dni','p.codigo')
                    ->get();
            }
        }
        public static function verificarId($request){
            $agente=DB::table('agente as a')
                ->join('ms_tagente as ta','ta.id','=','a.idtagente')
                ->where('a.id',$request['idagente'])
                ->where('ta.nombre',$request['tipo'])
                ->first();
            if ($agente){
                return ['success'=>true,'codigo'=>$agente->codigo];
            }else{
                return ['success'=>false];
            }
        }
        public static function getRol(){
            return Rol::select('estado','id','nombre')->get();
            //return Rol::whereNotIn('id',[1])->get();
        }
        public static function getResponsable(){
            return Persona::where('estado',0)
                ->whereNull('idusuario')
                ->select('id',DB::raw("CONCAT(nombre,',',apellido) AS full_name"))->get();
        }
        public static function getcargo(){
            return Cargo::where('estado',0)->get();
        }
        public static function searchEmpresa($request){
            $data= DB::table('empresa as e')
                ->join('proyecto as p','p.idempresa','=','e.id')
                ->where('p.id',$request['id'])
                ->select('e.razon_social','e.id')
                ->get();
            $agente=DB::table('agente as a')
                ->join('ms_tagente as ta','ta.id','=','a.idtagente')
                ->where('a.idproyecto',$request['id'])
                ->select(DB::raw("CONCAT(a.codigo,'--',ta.nombre) AS full_code"),'a.id')
                ->get();
            return ['empresa'=>$data,'codigo'=>$agente];
        }
        public static function obteneriDproyecto($request){
            return DB::table('proyecto as p')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->join('agente as a','a.idproyecto','=','p.id')
                ->where('a.id',$request['idagente'])
                ->select('p.codigo','e.id as idempresa','e.razon_social')
                ->get();
            //return Proyecto::where('id',$request['idproyecto'])->first(['id']);
        }
        public static function obteneridProyectoandEpp($request){
            return DB::table('proyecto as p')
                ->join('agente as a','a.idproyecto','=','p.id')
                ->join('epp as ep','ep.idagente','=','a.id')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->where('ep.idagente',$request['idagente'])
                ->where('ep.id',$request['idepp'])
                ->select('ep.entrega','p.codigo','e.razon_social','e.id as idempresa')
                ->get();
        }
        public static function getServicios(){
           return DB::table('ms_tservicio')->where('estado','=',0)->select('id','servicios')->get();
        }
        public static function getMonitoreo($request){
           return DB::table('ms_tmonitoreo')->where('estado','=',0)
               ->where('idservicio',$request['idservicio'])
               ->select('id','monitoreo')->get();
        }
        public static function getCategoria($request){
           return DB::table('ms_tagente')->where('estado','=',0)
               ->where('idmonitoreo',$request['idcategoria'])
               ->select('id','nombre')->get();
        }
        public static function getDepartamento(){
            return Departamento::all();
        }
        public static function getProvincia($request){
            return Provincia::where('idDepa',$request['id'])->get();
        }
        public static function getDistrito($request){
            return Distrito::where('idProv',$request['id'])->get();
        }
    }
