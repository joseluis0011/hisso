<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\CategoriaPeligro;
    use App\Models\Inspecciones;
    use App\Models\SubCategoriaPeligro;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Str;

    class RepoInspecciones
    {
        public static function addinspeccion($request){
            if ($request->file('file')){
                $logo=$request->file;
                $random = Str::random(10);
                $filename = $logo->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/inspecciones',$logo,$fileserver);
                $file=asset('storage/inspecciones/'.$fileserver);
            }else{
                $file=$request->file;
            }
            if ($request->file('file1')){
                $logo1=$request->file1;
                $random = Str::random(10);
                $filename = $logo1->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/inspecciones',$logo1,$fileserver);
                $file1=asset('storage/inspecciones/'.$fileserver);
            }else{
                $file1=$request->file1;
            }
            if ($request->file('file2')){
                $logo2=$request->file2;
                $random = Str::random(10);
                $filename = $logo2->getClientOriginalName();
                $fileserver = $random.'_'.$filename;
                Storage::putFileAs('public/inspecciones',$logo2,$fileserver);
                $file2=asset('storage/inspecciones/'.$fileserver);
            }else{
                $file2=$request->file2;
            }
            return Inspecciones::updateOrCreate(
                [
                    'id'=>$request['id']
                ],
                [
                    'idproyecto'=>$request['idproyecto'],
                    'tipo'=>$request['tipo'],
                    'fecha_hallazgo'=>$request['fecha_hallazgo'],
                    'hora_hallazgo'=>$request['hora_hallazgo'],
                    'descripcion_peligro'=>$request['descripcion_peligro'],
                    'idcategoria_peligro'=>$request['idcategoria_peligro'],
                    'idsubcategoria_peligro'=>$request['idsubcategoria_peligro'],
                    'area'=>$request['area'],
                    'subarea'=>$request['subarea'],
                    'tipo_ubicacion'=>$request['tipo_ubicacion'],
                    'personal'=>$request['personal'],
                    'cargo'=>$request['cargo'],
                    'correo'=>$request['correo'],
                    'foto1'=>$file,
                    'foto2'=>$file1,
                    'foto3'=>$file2,
                    'estado'=>0,
                    'idpersona'=>Auth::user()->persona[0]->id
                ]
            );
        }
        public static function listarInspeccion($estado,$id){
            return DB::table('inspecciones as i')
                ->join('persona as p','p.id','=','i.idpersona')
                ->where('i.idproyecto',$id)
                ->where('i.estado',$estado)
                ->select('i.id as idinspeccion','i.tipo','i.hora_hallazgo',
                    DB::raw("DATE_FORMAT(i.fecha_hallazgo,'%d/%m/%Y') as fecha_hallazgo,CONCAT(p.nombre,',',p.apellido) AS full_name"))
                ->orderBy('i.id','desc')->paginate(15);
        }
        public static function buscarInspeccion($estado,$buscar,$select,$id){
            return DB::table('persona as p')
                ->join('inspecciones as i','i.idpersona','=','p.id')
                ->where('i.idproyecto',$id)
                ->where('i.estado',$estado)
                ->where($select,'like','%'.$buscar.'%')
                ->select('i.id as idinspeccion','i.tipo','i.hora_hallazgo',
                    DB::raw("DATE_FORMAT(i.fecha_hallazgo,'%d/%m/%Y') as fecha_hallazgo,CONCAT(p.nombre,',',p.apellido) AS full_name"))
                ->orderBy('i.id','desc')->paginate(15);
        }
        public static function getlistCategoriaPeligro(){
            return CategoriaPeligro::all();
        }
        public static function getSubcategoriaPeligro($id){
            return SubCategoriaPeligro::where('idcategoria',$id)->get();
        }
        public static function getdata($request){
            return Inspecciones::where('id',$request['id'])
                ->get();
        }
        public static function estado($request){
                $model=Inspecciones::where('id',$request['objeto']['idinspeccion'])->first();
            if ($request->tipo == 0){
                $model->estado=1;
            }else{
                $model->estado =0;
            }
            $model->save();
            if ($model){
                $resp=['success'=>true];
            }else{
                $resp=['success'=>false];
            }
            return $resp;
        }
    }
