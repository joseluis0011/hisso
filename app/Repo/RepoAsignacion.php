<?php
    
    
    namespace App\Repo;
    
    
    use App\Models\Agente;
    use App\Models\AsignacionEquipoResponsable;
    use App\Models\AsignacionProyecto;
    use App\Models\Equipo;
    use App\Models\Persona;
    use App\Models\Proyecto;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;

    class RepoAsignacion
    {
        public static function getCodigo($request){
            $proyectos= self::verificarProyecto();
            $responsables = DB::table('usuario as u')
                ->join('usuario_has_rol as ur','ur.usuario_id','=','u.id')
                ->join('persona as p','p.idusuario','=','u.id')
                ->join('rol as r','r.id','=','ur.rol_id')
                ->whereNotIn('r.id',[1,5])
                ->select('p.id','p.nombre','p.apellido',DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name"))->get();
            $equipos= Equipo::whereNotIn('estado',[1])->select('nombre','id','serie',DB::raw("CONCAT(nombre,'--',serie) AS full_equipo"))->get();
            $agente=DB::table('agente as a')
                ->join('ms_tagente as ta','ta.id','=','a.idtagente')
                ->where('a.idproyecto',$request['idproyecto'])
                ->select('a.id','a.codigo',DB::raw("CONCAT(a.codigo,'--',ta.nombre) AS full_name_servicio"))->get();
            $fproyecto=Proyecto::where('id',$request['idproyecto'])->get(['fecha_inicio','fecha_fin']);
            return ['equipos'=>$equipos,'proyectos'=>$proyectos,'responsables'=>$responsables,'codigos'=>$agente,'fechas'=>$fproyecto];
        }
        public static function verificarProyecto(){
            $aProyecto=AsignacionProyecto::select('idproyecto')->get();
            $proyectos= DB::table('proyecto as p')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->where('p.estado','=',0)
                ->whereNotIn('p.id',$aProyecto)
                ->select('p.codigo','e.razon_social','p.id')->get();
            return $proyectos;
        }
        public static function getProyectos($request){
            return DB::table('proyecto as p')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->where('p.id',$request['idproyecto'])
                ->select('p.id','e.razon_social')
                ->get();
        }
        public static function addproyecto($request){
            return AsignacionProyecto::updateOrCreate(
                [
                    'id'=>$request['formulario']['id']
                ],
                [
                    'idproyecto'=>$request['formulario']['codigo'],
                    'responsable'=>implode('-',$request['arrResponsable']),
                    'equipos'=>implode('-',$request['arrEquipo']),
                    'usercreated'=>Auth::user()->id,
                    'estado'=>0
                ]
            );
        }
        public static function addEquipoResponsable($request,$proyecto){
            try {
                DB::beginTransaction();
                $arrayList =$request['arrayListResponsableEquipo'];
                for($i = 0; $i < count($arrayList); $i++){
                    $addEqResp = self::addEqResp($proyecto,$arrayList[$i]['id'],$arrayList[$i]['responsable'],$arrayList[$i]['equipos'],
                        $arrayList[$i]['codigo_interno'],$arrayList[$i]['finicio'],$arrayList[$i]['ffin'],$arrayList[$i]['descripcion']);
                    $updateEquipo=self::updateState(0,$arrayList[$i]['equipos']);
                    $updatePersona=self::updateState(1,$arrayList[$i]['responsable']);
                }
                DB::commit();
                return response()->json(array("success" => true,'message'=>'Agregado Correctamente'));
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error'=>false,'message'=>$e->getMessage()]);
            }
        }
        public static function addEqResp($proyecto,$id,$responsable,$equipo,$idagente,$fechainicio,$fechafin,$descripcion){
         return AsignacionEquipoResponsable::updateOrCreate(
                [
                    'id'=>$id
                ],
                [
                    'idasignacion_proyecto'=>$proyecto,
                    'idagente'=>$idagente,
                    'idpersona'=>$responsable,
                    'idequipo'=>$equipo,
                    'fecha_inicio'=>$fechainicio,
                    'fecha_fin'=>$fechafin,
                    'usercreated'=>Auth::user()->id,
                    'descripcion'=>$descripcion
                ]
            );
        }
        public static function updateState($tipo,$value){
            if ($tipo == 0){
                $model=Equipo::where('id',$value)->first();
                $model->estado=2;
                $model->save();
            }else{
                $model=Persona::where('id',$value)->first();
                $model->estado=2;
                $model->save();
            }
            return $model;
        }
        public static function getItemEquipo($request){
            return Equipo::where('id',$request['form']['equipos'])->select('nombre','serie')->first();
        }
        public static function getItemResponsable($request){
            return Persona::where('id',$request['form']['responsable'])->select(DB::raw("CONCAT(nombre,',',apellido) AS nombre"))->first();
        }
        public static function getItemAgente($request){
            return Agente::where('id',$request['form']['codigo_interno'])->select('codigo')->first();
        }
        public static function listarAsignacion($estado){
            return DB::table('asignacion_proyecto as ap')
                ->join('proyecto as p','p.id','=','ap.idproyecto')
                ->join('empresa as e','e.id','=','p.idempresa')
                ->where('ap.estado',$estado)
                ->select('ap.id','p.id as idproyecto','e.razon_social','p.codigo','ap.responsable','ap.equipos','ap.estado')
                ->orderBy('ap.id','desc')->paginate(12);
        }
        public static function searchAsignacion($estado,$buscar,$select){
            if ($select == 'ap.fecha_inicio'){
                return DB::table('asignacion_proyecto as ap')
                    ->join('proyecto as p','p.id','=','ap.idproyecto')
                    ->join('empresa as e','e.id','=','p.idempresa')
                    ->where('ap.estado',$estado)
                    ->whereDate($select,'=',$buscar)
                    ->select('ap.id','p.id as idproyecto','e.razon_social','p.codigo','ap.personal','ap.equipos',
                        DB::raw("DATE_FORMAT(ap.fecha_inicio,'%d/%m/%Y') as fecha_inicio"),
                        DB::raw("DATE_FORMAT(ap.fecha_fin,'%d/%m/%Y') as fecha_fin"))
                    ->orderBy('ap.id','desc')->paginate(12);
            }else{
                return DB::table('asignacion_proyecto as ap')
                    ->join('proyecto as p','p.id','=','ap.idproyecto')
                    ->join('empresa as e','e.id','=','p.idempresa')
                    ->where('ap.estado',$estado)
                    ->where($select,'like','%'.$buscar.'%')
                    ->select('ap.id','p.id as idproyecto','e.razon_social','p.codigo','ap.personal','ap.equipos',
                        DB::raw("DATE_FORMAT(ap.fecha_inicio,'%d/%m/%Y') as fecha_inicio"),
                        DB::raw("DATE_FORMAT(ap.fecha_fin,'%d/%m/%Y') as fecha_fin"))
                    ->orderBy('ap.id','desc')->paginate(12);
            }
        }
        public static function getData($request){
            $codigo=Proyecto::select('codigo','id')->get();
            $asignacion =AsignacionProyecto::where('idproyecto',$request['id'])->first(['id','idproyecto as codigo']);
            $asiResp=DB::table('asignacion_responsable_equipo as are')
                ->join('asignacion_proyecto as ap','ap.id','=','are.idasignacion_proyecto')
                ->join('agente as a','a.id','=','are.idagente')
                ->join('persona as p','p.id','=','are.idpersona')
                ->join('equipos as e','e.id','=','are.idequipo')
                ->where('ap.id',$asignacion->id)
                ->select('are.id','a.codigo','a.id as codigo_interno','p.id as responsable',DB::raw("CONCAT(p.nombre,',',p.apellido) AS nombreResponsable"),
                    'e.id as equipos','e.nombre as nombreEquipo','e.serie as serieEquipo','are.fecha_inicio as finicio','are.fecha_fin as ffin','are.descripcion')
                ->get();
            return ['asignacion'=>$asignacion,'asigRes'=>$asiResp,'codigo'=>$codigo];
        }
        public static function cerrarProyecto($request){
            try {
                DB::beginTransaction();
                self::searchEquipoResponsable($request);
                self::updateStateAsignacion($request);
                self::updateStateProyecto($request);
                DB::commit();
                return response()->json(array("success" => true,'message'=>'Proyecto Cerrado Correctamente'));
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error'=>false,'message'=>$e->getMessage()]);
            }
        }
        public static function updateStateProyecto($request){
            $model= Proyecto::where('id',$request['objeto']['idproyecto'])->first();
            $model->estado=1;
            $model->save();
            return $model;
        }
        public static function updateStateAsignacion($request){
            $model=AsignacionProyecto::where('id',$request['objeto']['id'])->first();
            $model->estado=1;
            $model->save();
            return $model;
        }
        public static function searchEquipoResponsable($request){
            $data= AsignacionEquipoResponsable::where('idasignacion_proyecto',$request['objeto']['id'])->get(['idequipo','idpersona']);
            foreach ($data as $d=>$dat){
                self::updateEquipo($dat['idequipo']);
                self::updateResponsable($dat['idpersona']);
            }
        }
        public static function updateEquipo($idequipo){
            $model= Equipo::where('id',$idequipo)->first();
            $model->estado=0;
            $model->save();
            return $model;
        }
        public static function updateResponsable($idresponsable){
            $model=Persona::where('id',$idresponsable)->first();
            $model->estado=0;
            $model->save();
            return $model;
        }
        public static function eliminarResponsable($request){
            try {
                DB::beginTransaction();
                self::updateEquipo($request['objeto']['equipos']);
                self::updateResponsable($request['objeto']['responsable']);
                AsignacionEquipoResponsable::where('id',$request['objeto']['id'])->delete();
                DB::commit();
                return response()->json(array("success" => true,'message'=>'Eliminado de la Lista'));
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error'=>false,'message'=>$e->getMessage()]);
            }
        }
        public static function searchid($request){
            $equipo = Equipo::where('id',$request['idequipo'])->get(['nombre']);
            return $equipo;
        }
    }
