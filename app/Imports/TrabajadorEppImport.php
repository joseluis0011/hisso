<?php

namespace App\Imports;

use App\Models\EppTrabajador;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class TrabajadorEppImport implements ToModel,WithHeadingRow,WithValidation,SkipsEmptyRows
{
    use Importable;
    
    protected $id;
    protected $idepp;
    public function __construct(int $idproyecto,int $idepp)
    {
        $this->id = $idproyecto;
        $this->idepp = $idepp;
    }
    /**
     * https://www.youtube.com/watch?v=Q2AUH9w9XaA
     * https://www.youtube.com/results?search_query=laravel+excel+import+validation
     * @param array $row
     *
     * @return Model|Model[]|null
     */
    public function model(array $row)
    {
        return EppTrabajador::create([
            'idepp'=>$this->idepp,
            'idagente'=>$this->id,
            'nombres'=> $row['nombres_y_apellidos'],
            'dni'=> $row['dni'],
            'area'=> $row['area'],
            'puesto_trabajo'=> $row['puesto_de_trabajo'],
        ]);
    }
    
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            '*.nombres_y_apellidos' => 'required',
            '*.dni'=>'required',
            '*.area' => 'required',
            '*.puesto_de_trabajo' => 'required'
        ];
    }
    public function customValidationMessages()
    {
        return [
            'nombres_y_apellidos.required' => 'El Campo Nombre y Apellidos son obligatorios',
            '1.nombres_y_apellidos.required' => 'El Campo Nombre y Apellidos son obligatorios',
            'dni.required' => 'El Campo DNI son obligatorios',
            '1.dni.required' => 'El Campo DNI son obligatorios',
            'area.required' => 'El Campo AREA son obligatorios',
            '1.area.required' => 'El Campo AREA son obligatorios',
            'puesto_de_trabajo.required' => 'El Campo Puesto de Trabajo son obligatorios',
            '1.puesto_de_trabajo.required' => 'El Campo Puesto de Trabajo son obligatorios',
        ];
    }
}
