<?php

namespace App\Imports;

use App\Models\TemTrabajador;
use App\Models\Trabajador;
use Illuminate\Console\OutputStyle;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Maatwebsite\Excel\Concerns\WithValidation;

class TrabajadorImport implements ToModel,WithHeadingRow,WithValidation
{
    protected $id;
    public function __construct(int $idagente)
    {
        $this->id = $idagente;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }
    public function model(array $row){
        return Trabajador::create([
            'idagente'=>$this->id,
            'dni'=> $row['dni'],
            'nombres_apellidos'=> $row['nombres_y_apellidos'],
            'puesto_trabajo'=> $row['puesto_de_trabajo'],
            'area_monitoreada'=> $row['area_monitoreada'],
            'instalacion'=> $row['instalacion'],
            'descripcion_instalacion'=> $row['descripcion_de_la_instalacion'],
            'calle_numero'=> $row['calle_y_numero'],
            'distrito'=> $row['distrito'],
            'area_geografica'=> $row['area_geografica'],
            'region_geografica'=> $row['region_geografica'],
            'estado'=>0,
            'usercreated'=>Auth::user()->persona[0]->id
        ]);
    }
    
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'dni' => 'required'
        ];
    }
    public function customValidationAttributes()
    {
        return ['dni'=> 'el Campo DNI es Obligatorio en el Excel'];
    }
}
