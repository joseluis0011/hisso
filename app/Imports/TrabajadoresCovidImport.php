<?php

namespace App\Imports;

use App\Models\Covid;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use function App\obtenerRol;

class TrabajadoresCovidImport implements ToCollection,WithHeadingRow,WithValidation
{
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $user = User::create([
               'idrol'=>obtenerRol('Trabajador')->id,
                'usuario'=>$row['dni'],
                'password'=>Hash::make($row['dni']),
                'pwd'=>bcrypt($row['dni']),
                'estado'=>0
            ]);
            
            Covid::create([
                'idproyecto'=>$this->id,
                'nombre'=> $row['nombre'],
                'dni'=> $row['dni'],
                'correo'=> $row['correo'],
                'idusuario'=>$user->id
            ]);
        }
    }
    
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
          'dni' =>'required|unique:covid'
        ];
    }
    public function customValidationMessages()
    {
        return [
            'dni' => 'El DNI ya esta Registrado',
        ];
    }
}
