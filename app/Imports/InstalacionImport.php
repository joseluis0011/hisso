<?php

namespace App\Imports;

use App\Models\Instalacion;
use App\Models\TempInstalacion;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class InstalacionImport implements ToModel,WithHeadingRow,WithValidation
{
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return  Instalacion::create([
            'idproyecto'=>$this->id,
            'area_monitoreada'=> $row['area_monitoreada'],
            'instalacion'=> $row['instalacion'],
            'descripcion_instalacion'=> $row['descripcion_de_la_instalacion'],
            'calle_numero'=> $row['calle_y_numero'],
            'distrito'=> $row['distrito'],
            'area_geografica'=> $row['area_geografica'],
            'region_geografica'=> $row['region_geografica'],
            'estado'=>0,
            'usercreated'=>Auth::user()->persona[0]->id
        ]);
    }
    
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'area_monitoreada' => 'required'
        ];
    }
    public function customValidationAttributes()
    {
        return ['area_monitoreada'=> 'el Campo Area Monitoreada es Obligatorio en el Excel'];
    }
}
