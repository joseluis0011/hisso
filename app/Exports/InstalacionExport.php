<?php

namespace App\Exports;

use App\Models\Instalacion;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class InstalacionExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    use Exportable;
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'RAZÓN SOCIAL',
            'CODIGO PROYECTO',
            'AREA MONITOREADA',
            'INSTALACION',
            'DESCRIPCION INSTALACION',
            'CALLE Y NUMERO',
            'DISTRITO',
            'AREA GEOGRAFICA',
            'REGION GEOGRAFICA'
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
    
    /**
     * @return Collection
     */
    public function collection()
    {
        return DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','e.id')
            ->join('instalacion as i','i.idproyecto','=','p.id')
            ->where('i.idproyecto',$this->id)
            ->select('e.razon_social','p.codigo','i.area_monitoreada','i.instalacion','i.descripcion_instalacion',
                'i.calle_numero','i.distrito','i.area_geografica','i.region_geografica')
            ->get();
    }
}
