<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class InspeccionesExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return  DB::table('inspecciones as i')
            ->join('persona as p','p.id','=','i.idpersona')
            ->join('categoria_peligro as cp','cp.id','=','i.idcategoria_peligro')
            ->join('subcategoria_peligro as subp','subp.id','=','i.idsubcategoria_peligro')
            ->select(DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name,
            (case i.tipo when 1 then 'Condición' when 2 then 'Acto Subestandar' end) AS tipo,
            DATE_FORMAT(i.fecha_hallazgo,'%d/%m/%Y') as fecha_hallazgo"),'i.hora_hallazgo','i.descripcion_peligro','cp.nombre as idcategoria_peligro',
                'subp.nombre as idsubcategoria_peligro','i.area','i.subarea',DB::raw("(case i.tipo_ubicacion when 1 then 'Interna' when 2 then 'Externa' end) AS tipo_ubicacion"),'i.personal','i.cargo','i.correo',
                DB::raw("(case i.estado when 0 then 'activo' when 1 then 'inactivo' end) AS estado"),'i.foto1','i.foto2','i.foto3')
            ->get();
    }
    
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'NOMBRES',
            'TIPO',
            'FECHA HALLAZGO',
            'HORA HALLAZGO',
            'DESCRIPCION PELIGRO',
            'CATEGORIA PELIGRO',
            'SUBCATEGORIA PELIGRO',
            'AREA',
            'SUB. AREA',
            'TIPO UBICACION',
            'RESPONSABLE',
            'CARGO',
            'CORREO',
            'ESTADO',
            'FOTO 1',
            'FOTO 2',
            'FOTO 3'
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
