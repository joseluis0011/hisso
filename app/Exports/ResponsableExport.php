<?php

namespace App\Exports;

use App\Models\Persona;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use function App\obtenerCargo;

class ResponsableExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('persona as p')
            ->join('empresa as e','e.id','=','p.idempresa')
            ->join('ubigeo as u','u.id','=','p.idubigeo')
            ->join('departamento as dep','dep.id','=','u.iddepartamento')
            ->join('provincia as pro','pro.id','=','u.idprovincia')
            ->join('distrito as dis','dis.id','=','u.iddistrito')
            ->select(DB::raw("CONCAT(p.nombre,',',p.apellido) AS full_name"),
                'p.dni', 'p.correo', 'p.celular','p.sexo',DB::raw("DATE_FORMAT(p.fecha_nacimiento,'%d/%m/%Y') as fechanacimiento"),
                'p.codigo_interno','e.razon_social','dep.departamento','pro.provincia','dis.distrito',
                DB::raw("(case p.estado when 0 then 'activo' when 1 then 'inactivo' end) AS estado"))->get();
    }

    public function headings(): array
    {
        return [
            'NOMBRES Y APELLIDOS',
            'DNI',
            'CORREO',
            'CELULAR',
            'SEXO',
            'FECHA NACIMIENTO',
            'CODIGO INTERNO',
            'EMPRESA',
            'DEPARTAMENTO',
            'PROVINCIA',
            'DISTRITO',
            'ESTADO'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
