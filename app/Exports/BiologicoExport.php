<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class BiologicoExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','=','e.id')
            ->join('instalacion as i','i.idproyecto','=','p.id')
            ->join('det_bio_hisopo as dr', 'dr.idinstalacion', '=', 'i.id')
            ->where('dr.idproyecto', $this->id)
            ->select('e.razon_social','e.ruc','e.domicilio_fiscal','e.act_economica','e.numero_trabajadores',
                'dr.punto_monitoreo','i.area_monitoreada',DB::raw("DATE_FORMAT(dr.fecha_monitoreo,'%d/%m/%Y') as fecha_monitoreo"),
                'dr.hora_monitoreo','dr.codigo_muestra','dr.descripcion_lugar','dr.muestreo','foto1','foto2')
            ->get();
    }
    
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'RAZON SOCIAL',
            'RUC',
            'DOMICILIO FISCAL',
            'ACTIVIDAD ECONOMICA',
            'NUMERO DE TRABAJADORES',
            'PUNTO MONITOREO',
            'AREA MONITOREADA',
            'FECHA MONITOREO',
            'HORA MONITOREO',
            'CODIGO MUESTRA',
            'DESCRIPCION LUGAR',
            'MUESTREO',
            'FOTO 1',
            'FOTO 2'
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
