<?php

namespace App\Exports;

use App\Models\Covid;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class EncuestaCovidExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','=','e.id')
            ->join('covid as c','c.idproyecto','=','p.id')
            ->where('c.idproyecto',$this->id)
            ->select('e.razon_social','p.codigo','c.nombre','c.dni','c.correo',
                DB::raw("DATE_FORMAT(c.fecha,'%d/%m/%Y %H:%i:%S') as fecha"))
            ->get();
    }
    
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'RAZON SOCIAL',
            'CODIGO',
            'NOMBRE',
            'DNI',
            'CORREO',
            'FECHA'
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
