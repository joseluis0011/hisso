<?php

namespace App\Exports;

use App\Models\DetErgonomia;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ErgonomiaExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','=','e.id')
            ->join('trabajador as t','t.idproyecto','=','p.id')
            ->join('det_ergonomia as de', 'de.idtrabajador', '=', 't.id')
            ->where('de.idproyecto', $this->id)
            ->select('e.razon_social','e.ruc','e.domicilio_fiscal','e.act_economica','e.numero_trabajadores',
                'de.punto_monitoreo',DB::raw("DATE_FORMAT(de.fecha_monitoreo,'%d/%m/%Y') as fecha_monitoreo"),'t.nombres','t.apellido_paterno','t.apellido_materno','t.puesto_trabajo',
                'de.hora_monitoreo','foto1','foto2','foto3','foto4','foto5')
            ->get();
    }
    
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'RAZON SOCIAL',
            'RUC',
            'DOMICILIO FISCAL',
            'ACTIVIDAD ECONOMICA',
            'NUMERO DE TRABAJADORES',
            'PUNTO MONITOREO',
            'FECHA',
            'NOMBRES',
            'APELLIDO PATERNO',
            'APELLIDO MATERNO',
            'PUESTO DE TRABAJO',
            'HORA MONITOREO',
            'FOTO 1',
            'FOTO 2',
            'FOTO 3',
            'FOTO 4',
            'FOTO 5',
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
