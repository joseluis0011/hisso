<?php

namespace App\Exports;

use App\Models\Equipo;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class EquiposExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Equipo::select('nombre','marca','modelo','serie',
            DB::raw("DATE_FORMAT(fecha_calibracion,'%d/%m/%Y') as fechacalibracion,
            DATE_FORMAT(fecha_termino,'%d/%m/%Y') as fechatermino,
            (case estado when 0 then 'activo' when 1 then 'inactivo' end) AS estado"))->get();
    }

    public function headings(): array
    {
        return [
            'NOMBRE EQUIPO',
            'MARCA',
            'MODELO',
            'SERIE',
            'FECHA DE CALIBRACION',
            'FECHA DE TERMINO',
            'ESTADO'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
