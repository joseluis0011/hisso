<?php

namespace App\Exports;

use App\Models\DetRuido;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RuidoExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','=','e.id')
            ->join('instalacion as i','i.idproyecto','=','p.id')
            ->join('det_ruido as dr', 'dr.idinstalacion', '=', 'i.id')
            ->where('dr.idproyecto', $this->id)
            ->select('e.razon_social','e.ruc','e.domicilio_fiscal','e.act_economica','e.numero_trabajadores',
                'dr.punto_monitoreo','i.area_monitoreada',DB::raw("DATE_FORMAT(dr.fecha_monitoreo,'%d/%m/%Y') as fecha_monitoreo"),
                'dr.hora_monitoreo','dr.min1','dr.min2','dr.min3','dr.max1','dr.max2','dr.max3',
                'dr.lequiv1','dr.lequiv2','dr.lequiv3','dr.foto')
            ->get();
    }
    
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'RAZON SOCIAL',
            'RUC',
            'DOMICILIO FISCAL',
            'ACTIVIDAD ECONOMICA',
            'NUMERO DE TRABAJADORES',
            'PUNTO MONITOREO',
            'AREA MONITOREADA',
            'FECHA MONITOREO',
            'HORA MONITOREO',
            'MIN 1',
            'MIN 2',
            'MIN 3',
            'MAX 1',
            'MAX 2',
            'MAX 3',
            'LEQUIV 1',
            'LEQUIV 2',
            'LEQUIV 3',
            'FOTO'
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
