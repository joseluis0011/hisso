<?php

namespace App\Exports;

use App\Models\Empresa;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class EmpresaExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Empresa::select('razon_social','ruc','act_economica','domicilio_fiscal','monitoreo','frecuencia','numero_trabajadores',
            DB::raw("(case estado when 0 then 'activo' when 1 then 'inactivo' end) AS estado"))->get();
    }

    public function headings(): array
    {
        return [
            'RAZÓN SOCIAL',
            'RUC',
            'ACTIVIDAD ECONOMICA',
            'DOMICILIO FISCAL',
            'CUENTA CON PROGRAMA DE MONITOREO',
            'FRECUENCIA',
            'N° TRABAJADORES',
            'ESTADO'
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                             ],
                        ]
                   ]
        ];
    }
}
