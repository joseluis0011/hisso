<?php

namespace App\Exports;

use App\Models\Trabajador;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TrabajadorExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    use Exportable;
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','e.id')
            ->join('trabajador as t','t.idproyecto','=','p.id')
            ->where('t.idproyecto',$this->id)
            ->select('e.razon_social','p.codigo','t.dni','t.nombres','t.apellido_paterno','t.apellido_materno','t.puesto_trabajo',
                't.area_monitoreada','t.instalacion','t.descripcion_instalacion',
                't.calle_numero','t.distrito','t.area_geografica','t.region_geografica')
            ->get();
    }
    
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'RAZÓN SOCIAL',
            'CODIGO PROYECTO',
            'DNI',
            'NOMBRES',
            'APELLIDO PATERNO',
            'APELLIDO MATERNO',
            'PUESTO DE TRABAJO',
            'AREA MONITOREADA',
            'INSTALACION',
            'DESCRIPCION INSTALACION',
            'CALLE Y NUMERO',
            'DISTRITO',
            'AREA GEOGRAFICA',
            'REGION GEOGRAFICA'
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
