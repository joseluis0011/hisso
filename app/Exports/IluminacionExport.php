<?php

namespace App\Exports;

use App\Models\DetIluminacion;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class IluminacionExport implements FromCollection,WithHeadings,ShouldAutoSize,WithStyles
{
    protected $id;
    public function __construct(int $idproyecto)
    {
        $this->id = $idproyecto;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','=','e.id')
            ->join('trabajador as t','t.idproyecto','=','p.id')
            ->join('det_iluminacion as di', 'di.idtrabajador', '=', 't.id')
            ->where('di.idproyecto', $this->id)
            ->select('e.razon_social','e.ruc','e.domicilio_fiscal','e.act_economica','e.numero_trabajadores',
                'di.punto_monitoreo',DB::raw("DATE_FORMAT(di.fecha_monitoreo,'%d/%m/%Y') as fecha_monitoreo"),'t.nombres','t.apellido_paterno','t.apellido_materno','t.puesto_trabajo',
                'di.hora_monitoreo','di.tipo_iluminaria','di.max1','di.max2','di.max3','di.min1','di.min2','di.min3',
                'di.avg1','di.avg2','di.avg3','foto1','foto2','foto3','foto4','foto5','foto6','foto7','foto8','foto9','foto10')
            ->get();
    }
    
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'RAZON SOCIAL',
            'RUC',
            'DOMICILIO FISCAL',
            'ACTIVIDAD ECONOMICA',
            'NUMERO DE TRABAJADORES',
            'PUNTO MONITOREO',
            'FECHA',
            'NOMBRES',
            'APELLIDO PATERNO',
            'APELLIDO MATERNO',
            'PUESTO DE TRABAJO',
            'HORA MONITOREO',
            'TIPO ILUMINARIA',
            'MAX 1',
            'MAX 2',
            'MAX 3',
            'MIN 1',
            'MIN 2',
            'MIN 3',
            'AVG 1',
            'AVG 2',
            'AVG 3',
            'FOTO 1',
            'FOTO 2',
            'FOTO 3',
            'FOTO 4',
            'FOTO 5',
            'FOTO 6',
            'FOTO 7',
            'FOTO 8',
            'FOTO 9',
            'FOTO 10',
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => [
                'font' => [
                    'bold' => true,
                    'size' => 14,
                    'background' => ['argb' => 'EB2B02'],
                ],
                'borders' => [
                    'outline' => [
                        'borderStyle' => '#69D032',
                        'color' => ['argb' => 'EB2B02'],
                    ],
                ]
            ]
        ];
    }
}
