<?php

namespace App\Exports;

use App\Models\Proyecto;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProyectoExport implements FromCollection
{
    /**
    * @return array
    */
    public function collection()
    {
        $data =DB::select(DB::raw("SELECT proyecto.idempresa, empresa.razon_social, proyecto.codigo, idproyecto, sum(ILUMINACION) ILUMINACION, sum(RUIDO) RUIDO,sum(Ergonomia) ERGONOMIA FROM( select idproyecto, if (nombre = 'Iluminación', TRUE, FALSE) AS ILUMINACION, '0' as ruido, '0' as ergonomia from agente a UNION ALL select idproyecto, '0' as iluminacion, if (nombre = 'Ruido', TRUE, FALSE) AS RUIDO, '0' as ergonomia from agente b UNION ALL select idproyecto, '0' as iluminacion, '0' as ruido, if (nombre = 'Ergonomia', TRUE, FALSE) AS Ergonomia from agente b ) pro , proyecto, empresa where pro.idproyecto =proyecto.id and empresa.id=proyecto.idempresa group by idproyecto"));
        return  $data[0];
    }
}
