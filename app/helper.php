<?php
namespace App;

use App\Models\Cargo;
use App\Models\Rol;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

if (! function_exists('authUser')){
    function authUser()
    {
        $data= DB::table('usuario as u')
            ->join('usuario_has_rol as ur','ur.usuario_id','=','u.id')
            ->join('rol as r','r.id','=','ur.rol_id')
            ->where('u.id',Auth::user()->id)
            ->select('u.id','u.usuario','r.nombre')
            ->get();
        return $data[0];
    }
}
if (! function_exists('obtenerCargo')){
    function obtenerCargo()
    {
        return Cargo::where('nombre','Monitor')->select('id')->first();
    }
}
if (! function_exists('obtenerRol')){
    function obtenerRol($rol)
    {
        return Rol::where('nombre',$rol)->select('id')->first();
    }
}
