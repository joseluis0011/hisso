<?php

namespace App;

use App\Models\Covid;
use App\Models\Empresa;
use App\Models\Proyecto;
use App\Models\Rol;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "usuario";
    protected $primaryKey = "id";
    protected $fillable = [
         'usuario','password','estado','pwd','idrol'
    ];
    public $timestamps=false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function Persona(){
        return $this->hasMany('App\Models\Persona','idusuario');
    }
    public function Rol(){
        return $this->belongsTo(Rol::class,'idrol');
    }
    public function Covid(){
        return $this->hasMany(Covid::class,'idusuario');
    }
    public function Proyecto(){
        return $this->hasMany(Proyecto::class,'idusuario');
    }
}
