<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EquiposTrabajadorEPPRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('idepp');
        $idproyecto = $this->input('idproyecto');
        return [
            'idequipo'=>'required|unique:epp_trabajador_equipos,idequipo',
            'idproyecto'=>'required'
        ];
    }
    public function messages()
    {
        return [
          'idequipo.required'=>'Seleccione algun Equipo',
            'idequipo.unique'=>'El equipo ya esta en la lista'
        ];
    }
}
