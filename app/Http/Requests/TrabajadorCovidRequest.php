<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrabajadorCovidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id');
        $idproyecto = $this->input('codigo');
        return [
            'nombres'=>'required',
            'dni'=>'bail|required|unique:covid,dni,'.$id.',id,idproyecto,'.$idproyecto,
            'correo'=>'required',
            'password' => "required"
        ];
    }
    public function messages()
    {
        return [
          'nombres.required'=>'El campo nombres y Apellido es Obligatorio',
          'dni.required'=>'El campo dni es Obligatorio',
          'dni.unique'=>'El campo dni ya existe',
          'correo.required'=>'El campo Correo es Obligatorio',
            'password.required'=>'El campo Contraseña es Obligatorio'
        ];
    }
}
