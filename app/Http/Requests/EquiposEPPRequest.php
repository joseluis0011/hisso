<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EquiposEPPRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'formulario.nombre'=>'required',
            'formulario.codigo'=>'required',
            'formulario.frecuencia'=>'required'
        ];
    }
    public function messages()
    {
        return [
          'formulario.nombre.required'=>'El campo Nombre es Obligatorio',
          'formulario.codigo.required'=>'El campo Codigo es Obligatorio',
            'formulario.frecuencia.required'=>'El campo Frecuencia es Obligatorio'
        ];
    }
}
