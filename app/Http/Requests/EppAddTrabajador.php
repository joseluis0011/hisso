<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EppAddTrabajador extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form.nombres'=>'required',
            'form.dni'=>'required',
            'form.area'=>'required',
            'form.fecha_entrega'=>'required'
        ];
    }
    public function messages()
    {
        return [
          'form.nombres.required'=>'El Campo Nombres es Obligatorio',
          'form.dni.required'=>'El Campo DNI es Obligatorio',
          'form.area.required'=>'El Campo area es Obligatorio',
          'form.fecha_entrega.required'=>'El Campo Fecha Entrega es Obligatorio'
        ];
    }
}
