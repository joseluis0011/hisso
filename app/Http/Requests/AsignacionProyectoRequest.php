<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AsignacionProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'formulario.codigo'=>'required',
            'formulario.empresa'=>'required',
            'arrayListResponsableEquipo'=>'required|array',
            'arrayListResponsableEquipo.*'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'formulario.codigo.required'=>'El Campo es Obligatorio',
            'formulario.empresa.required'=>'El Campo es Obligatorio',
            'arrayListResponsableEquipo.required'=>'Debe Agregar algun Responsable con su Equipo de Medición'
        ];
    }
}
