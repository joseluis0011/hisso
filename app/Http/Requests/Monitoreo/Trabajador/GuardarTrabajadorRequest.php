<?php

namespace App\Http\Requests\Monitoreo\Trabajador;

use Illuminate\Foundation\Http\FormRequest;

class GuardarTrabajadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('form.codigo');
        return [
            'form.codinterno'=>'required|unique:trabajador,idagente,'.$id['form.codinterno'],
            'form.codigo'=>'required',
            'form.dni'=>'required',
            'form.nombres'=>'required',
            'form.trabajo'=>'required',
            'form.descripcion'=>'required',
            'form.instalacion'=>'required',
            'form.area'=>'required'
        ];
    }
    public function messages()
    {
        return [
          'form.codinterno.required'=>'Debe Completar el Campo',
            'form.codinterno.unique'=>'El Codigo ya esta Registrado',
            'form.codigo.required'=>'Debe Completar el Campo',
            'form.dni.required'=>'Debe Completar el Campo',
            'form.nombres.required'=>'Debe Completar el Campo',
            'form.trabajo.required'=>'Debe Completar el Campo',
            'form.descripcion.required'=>'Debe Completar el Campo',
            'form.instalacion.required'=>'Debe Completar el Campo',
            'form.area.required'=>'Debe Completar el Campo'
        ];
    }
}
