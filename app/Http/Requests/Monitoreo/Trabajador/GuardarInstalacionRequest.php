<?php

namespace App\Http\Requests\Monitoreo\Trabajador;

use Illuminate\Foundation\Http\FormRequest;

class GuardarInstalacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('form.codigo');
        return [
            'form.codigo'=>'required|unique:instalacion,idproyecto,'.$id['form.codigo'],
            'form.descripcion'=>'required',
            'form.instalacion'=>'required',
            'form.area'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'form.codigo.required'=>'Debe Completar el Campo',
            'form.codigo.unique'=>'El Codigo ya esta Registrado',
            'form.descripcion.required'=>'Debe Completar el Campo',
            'form.instalacion.required'=>'Debe Completar el Campo',
            'form.area.required'=>'Debe Completar el Campo'
        ];
    }
}
