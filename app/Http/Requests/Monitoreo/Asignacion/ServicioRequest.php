<?php

namespace App\Http\Requests\Monitoreo\Asignacion;

use Illuminate\Foundation\Http\FormRequest;

class ServicioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form.codigo_interno'=>'required',
            'form.responsable'=>'required',
            'form.equipos'=>'required',
            'form.finicio'=>'required|date|after_or_equal:fecha_inicio|before_or_equal:fecha_fin',
            'form.ffin'=>'required|date|after_or_equal:fecha_inicio|before_or_equal:fecha_fin'
        ];
    }
    public function messages()
    {
        return [
            'form.codigo_interno.required'=>'El Campo debe ser Obligatorio',
            'form.responsable.required'=>'El Campo debe ser Obligatorio',
            'form.equipos.required'=>'El Campo debe ser Obligatorio',
            'form.finicio.required'=>'El Campo debe ser Obligatorio',
            'form.finicio.date'=>'El Campo debe ser una Fecha Valida',
            'form.finicio.after_or_equal'=>'Debe ser una fecha dentro del Periodo designado en Proyectos',
            'form.finicio.before_or_equal'=>'Debe ser una fecha dentro del Periodo designado en Proyectos',
            'form.ffin.required'=>'El Campo debe ser Obligatorio',
            'form.ffin.date'=>'El Campo debe ser una Fecha Valida',
            'form.ffin.after_or_equal'=>'Debe ser una fecha dentro del Periodo designado en Proyectos',
            'form.ffin.before_or_equal'=>'Debe ser una fecha dentro del Periodo designado en Proyectos',
        ];
    }
}
