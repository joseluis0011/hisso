<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EncuestaCovidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'respuestas'=>'required|array|min:12',
            'respuestas.*'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'respuestas.required'=>'Todas las Preguntas son Obligatorios',
            'respuestas.min'=>'Todas las Preguntas son Obligatorios'
        ];
    }
}
