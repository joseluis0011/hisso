<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgenteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form.servicio'=>'required',
            'form.monitoreo'=>'required_if:form.servicio,==,1',
            'form.categoria'=>'required_if:form.servicio,==,1',
            'form.departamento'=>'required',
            'form.provincia'=>'required',
            'form.distrito'=>'required',
            'form.instalacion'=>'required',
            'form.direccion'=>'required',
            'form.puntos'=>'required_if:form.servicio,1,2,4|numeric'
        ];
    }
    public function messages()
    {
        return [
          'form.servicio.required'=>'El Campo es Obligatorio',
            'form.monitoreo.required_if'=>'El Campo es Obligatorio',
            'form.categoria.required_if'=>'El Campo es Obligatorio',
            'form.departamento.required'=>'El Campo es Obligatorio',
            'form.provincia.required'=>'El Campo es Obligatorio',
            'form.distrito.required'=>'El Campo es Obligatorio',
            'form.instalacion.required'=>'El Campo es Obligatorio',
                'form.direccion.required'=>'El Campo es Obligatorio',
            'form.puntos.required_if'=>'El Campo es Obligatorio',
            'form.puntos.numeric'=>'El Campo debe ser numerico'
        ];
    }
}
