<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'formulario.usuario'=>'required|unique:usuario',
            'formulario.password'=>'required',
            'formulario.rol'=>'required',
            'formulario.personal'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'formulario.usuario.required' => 'El Campo Usuario es Obligatorio',
            'formulario.password.required' => 'El Campo Contraseña es Obligatorio',
            'formulario.rol.required' => 'El Campo Rol es Obligatorio',
            'formulario.personal.required' => 'El Campo Responsable de Ejecución es Obligatorio'
        ];
    }
}
