<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function App\authUser;

class LoginController extends Controller
{
    public function login(Request $request){
        $usuario = $request->usuario;
        $password =$request->password;
        
        $rpta=Auth::attempt(['usuario' => $usuario,'password'=>$password,'estado'=>0]);
        if ($rpta){
            return response()->json([
                'authUser' => Auth::user(),
                'code' =>200
            ]);
        }else{
            return response()->json([
                'code'=> 401
            ]);
        }
    }
    public function logout(Request $request){
        Auth::logout();
        return response()->json([
            'code'=> 204
        ]);
    }
}
