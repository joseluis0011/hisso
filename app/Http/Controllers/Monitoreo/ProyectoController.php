<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\ProyectoExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\AgenteRequest;
use App\Repo\RepoEmpresa;
use App\Repo\RepoProyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ProyectoController extends Controller
{
    public function getEmpresa(){
        return RepoProyecto::getEmpresa();
    }
    public function agregar(Request $request){
        $this->validacion($request);
        if (RepoProyecto::validacion($request) == true){
            try {
                DB::beginTransaction();
                    $proyecto=RepoProyecto::addProyecto($request);
                    $agente=RepoProyecto::addAgentes($request,$proyecto->id);
                DB::commit();
                return response()->json(array("success" => true,'message'=>'Guardado Correctamente'));
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['error'=>false,'message'=>$e->getMessage()]);
            }
        }else{
            return ['false'=>false,'message'=>'Los Campos Cant.Lima y Cant.Provincia no pueden ser 0'];
        }
    }
    public function validacion($data)
    {
        $rules = [
            'formulario.codigo' => "bail|required|unique:proyecto,codigo,".$data['formulario']['idproyecto'],
            'formulario.idempresa' => "required",
            'formulario.fecha_inicio' => "required|date",
            'formulario.fecha_fin' => "required|date|after_or_equal:formulario.fecha_inicio",
            'formulario.lima' => "required|integer",
            'formulario.provincia' => "required|integer",
            'arrayListAgentes'=>"required"
        ];
        $message = [
            'formulario.codigo.required' => 'El Campo Codigo Proyecto es Obligatorio',
            'formulario.codigo.unique' => 'El Codigo Proyecto Ya esta Registrado',
            'formulario.idempresa.required' => "El Campo Empresa es Obligatorio",
            'formulario.fecha_inicio.required' => "El Campo Fecha de Inicio es Obligatorio",
            'formulario.fecha_inicio.date' => "El Campo Fecha de Inicio debe ser una Fecha Valida",
            'formulario.fecha_fin.required' => "El Campo Fecha de Fin es Obligatorio",
            'formulario.fecha_fin.date' => "El Campo Fecha de Fin debe se una Fecha Valida",
            'arrayListAgentes.required'=>'Debe Tener Agregado al menos 1 Agente',
            'formulario.fecha_fin.after_or_equal'=> 'El Campo Fecha Fin debe ser una Fecha posterior o igual a la Fecha de Inicio'
        
        ];
        $this->validate($data, $rules, $message);
    }
    public function index(Request $request){
        if ($request->tipo == 0) {
            if ($request->buscar == "") {
                $activo = RepoProyecto::listarProyecto(0);
            } else {
                $activo = RepoProyecto::buscarProyecto(0, $request->buscar,$request->select);
            }
            return [
                'pagination' => [
                    'total' => $activo->total(),
                    'current_page' => $activo->currentPage(),
                    'per_page' => $activo->perPage(),
                    'last_page' => $activo->lastPage(),
                    'from' => $activo->firstItem(),
                    'to' => $activo->lastItem(),
                ],
                'activo' => $activo
            ];
        } else {
            if ($request->buscar == '') {
                $inactivo = RepoProyecto::listarProyecto( 1);
            } else {
                $inactivo = RepoProyecto::buscarProyecto(1, $request->buscar,$request->select);
            }
            return [
                'pagination1' => [
                    'total' => $inactivo->total(),
                    'current_page' => $inactivo->currentPage(),
                    'per_page' => $inactivo->perPage(),
                    'last_page' => $inactivo->lastPage(),
                    'from' => $inactivo->firstItem(),
                    'to' => $inactivo->lastItem(),
                ],
                'inactivo' => $inactivo
            ];
        }
    }
    public function getdata(Request $request){
        return RepoProyecto::getDataProyecto($request['id']);
    }
    public function estadoProyecto(Request $request){
        return RepoProyecto::estadoProyecto($request);
    }
    public function getListarDetalle(Request $request){
        if ($request->buscar == '') {
            $activo = RepoProyecto::ProyectoDetalle($request->id);
        } else {
            $activo = RepoProyecto::buscarProyectoDetalle($request->buscar,$request->select);
        }
        return [
            'pagination' => [
                'total' => $activo->total(),
                'current_page' => $activo->currentPage(),
                'per_page' => $activo->perPage(),
                'last_page' => $activo->lastPage(),
                'from' => $activo->firstItem(),
                'to' => $activo->lastItem(),
            ],
            'detalle' => $activo
        ];
    }
    public function exportExcel(Request $request){
        return Excel::download(new ProyectoExport(),'Proyecto.xlsx');
    }
    public function getDashidProyecto(Request $request){
        return RepoProyecto::getDashidProyecto($request);
    }
    public function getDashboard(){
        return RepoProyecto::getDashboard();
    }
    public function eliminarProyecto(Request $request){
        return RepoProyecto::eliminarProyecto($request);
    }
    public function obtenerCodigo(Request $request){
        return RepoProyecto::obtenerCodigo($request);
    }
    public function valAgentes(AgenteRequest $request){
        $servicio = $request['form']['servicio'];
        if ($servicio == 1){
            $nombreCategoria = RepoProyecto::buscaragente($request['form']['categoria']);
        }else{
            $nombreCategoria = RepoProyecto::buscarServicio($request['form']['servicio']);
        }
        return ['success'=>true,$servicio,$nombreCategoria];
    }
    public function deleteAgente(Request $request){
        return RepoProyecto::deleteAgente($request);
    }
}
