<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\EncuestaCovidExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\TrabajadorCovidRequest;
use App\Imports\TrabajadoresCovidImport;
use App\Repo\RepoEncuestaCovid;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class EncuestaConvidController extends Controller
{
    public function listtrabajdores(Request $request){
        if ($request->buscar == ''){
            $lista=RepoEncuestaCovid::listCovid($request['idproyecto']);
        }else{
            $lista=RepoEncuestaCovid::searchCovid($request['idproyecto'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function importTrabajador(Request $request){
        try{
            DB::beginTransaction();
            $this->validacion($request);
            $data= Excel::import(new TrabajadoresCovidImport($request->codigo),$request->file('file'));
            if ($data){
                $usuario = RepoEncuestaCovid::addUsuario($request);
                RepoEncuestaCovid::updatebyProyecto($usuario->id,$request);
                DB::commit();
                return ['success'=>true,'message'=>'Se Agrego Correctamente'];
            }else{
                return ['success'=>false,'message'=>'Error al Agregar'];
            }
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e){
            DB::rollback();
            $fallas = $e->failures();
        
            foreach ($fallas as $falla) {
                $falla->row(); // fila en la que ocurrió el error
                $falla->attribute(); // el número de columna o la "llave" de la columna
                $falla->errors(); // Errores de las validaciones de laravel
                $falla->values(); // Valores de la fila en la que ocurrió el error.
            }
            return $falla;
        }
    }
    public function validacion($data)
    {
        $rules = [
            'file' => "required",
            'codigo' => "required",
            'password' => "required"
        ];
        $message = [
            'file.required' => 'El Campo Subir Data es Obligatorio',
            'codigo.required' => 'El Campo Codigo es Obligatorio',
            'usuario.required' => 'El Campo usuario es Obligatorio',
            'password.required' => "El Campo Contraseña es Obligatorio"
        ];
        $this->validate($data, $rules, $message);
    }
    public function listEncuesta(Request $request){
        $lista=RepoEncuestaCovid::listEncuesta($request->select,$request->buscar,$request->idusuario);
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function validarEmpresa(Request $request){
        $data =RepoEncuestaCovid::validarCampoNull($request);
        if ($data[0]->idusuario == null){
            return ['success'=>true,'estado'=>0];
        }else{
            $user = RepoEncuestaCovid::obtenerUsuario($data[0]->idusuario);
            return ['success'=>false,'estado'=>1,$user];
        }
    }
    public function eliminartrabajador(Request $request){
        return RepoEncuestaCovid::eliminarTrabajador($request);
    }
    public function searchPregunta(Request $request){
       return RepoEncuestaCovid::searchPregunta($request);
    }
    public function searchTrabajador(Request $request){
        return RepoEncuestaCovid::searchTrabajador($request);
    }
    public function exportTrabajador(Request $request){
        return Excel::download(new EncuestaCovidExport($request->idproyecto),'Encuesta Covid.xlsx');
    }
    public function agregarTrabajador(TrabajadorCovidRequest $request){
        return RepoEncuestaCovid::agregarTrabajador($request);
    }
}
