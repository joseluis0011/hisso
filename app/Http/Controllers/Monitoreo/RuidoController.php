<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\RuidoExport;
use App\Http\Controllers\Controller;
use App\Models\Instalacion;
use App\Repo\RepoRuido;
use App\Repo\RepoTrabajador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpWord\TemplateProcessor;

class RuidoController extends Controller
{
    public function index(Request $request){
        if ($request->buscar == ''){
            $lista=RepoRuido::listInstalacion($request['idproyecto']);
        }else{
            $lista=RepoRuido::searchInstalacion($request['idproyecto'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function addInstalacion(Request $request){
        $data= RepoTrabajador::agregarInstalacion($request);
        if ($data){
            return ['success'=>true];
        }else{
            return ['success' => false];
        }
    }
    public function addDetalleRuido(Request $request){
            $this->validacion($request);
        try {
            DB::beginTransaction();
                $data=RepoRuido::addDetalleRuido($request);
            DB::commit();
            return response()->json(array("success" => true));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error'=>false]);
        }
    }
    public function validacion($data){
        $rules = [
            'punto' => "required",
            'fecha' => "required",
            'hora' => "required",
            'max' => "required",
            'max1' => "required",
            'max2' => "required",
            'min' => "required",
            'min2' => "required",
            'lequiv' => "required",
            'lequiv1' => "required",
            'lequiv2' => "required",
            'fuente_ruido' => "required",
            'actividad_medicion' => "required",
            'trabajadores_expuestos' => "required",
            'file' => "required"
        ];
        $message = [
            'punto.required' => 'El Campo Punto Monitoreo es Obligatorio',
            'fecha.required' => 'El Campo Fecha es Obligatorio',
            'hora.required' => 'El Campo Hora es Obligatorio',
            'max.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'max1.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'max2.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'min.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'min1.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'min2.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'lequiv.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'lequiv1.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'lequiv2.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'fuente_ruido.required' => "El Campo Fuente de Ruido es Obligatorio",
            'actividad_medicion.required' => "El Campo Actividad que se Realiza Durante la Medicion es Obligatorio",
            'trabajadores_expuestos.required' => "El Campo Numero de Trabajadores Expuestos es Obligatorio",
            'file.required' => "El Campo Fuente de Ruido (foto) es Obligatorio"
        ];
        $this->validate($data, $rules, $message);
    }
    public function getRuido(Request $request){
      return RepoRuido::getRuido($request);
    }
    public function exportExcelInstalacion(Request $request){
        return Excel::download(new RuidoExport($request->idproyecto),'Monitoreo Ruido.xlsx');
    }
    public function exportFormatoLey(Request $request){
        $empresa = DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','=','e.id')
            ->where('p.id',$request->idproyecto)
            ->select('e.razon_social','e.ruc','e.domicilio_fiscal','e.act_economica','e.numero_trabajadores')
            ->get();
        $instalacion = Instalacion::where('idproyecto',$request->idproyecto)->first();
        $templateProcessor = new TemplateProcessor('formato_ley/formato_ley.docx');
        $templateProcessor->setValue('razon_social',$empresa[0]->razon_social);
        $templateProcessor->setValue('ruc',$empresa[0]->ruc);
        $templateProcessor->setValue('domicilio_fiscal',$empresa[0]->domicilio_fiscal);
        $templateProcessor->setValue('act_economica',$empresa[0]->act_economica);
        $templateProcessor->setValue('numero_trabajadores',$empresa[0]->numero_trabajadores);
        $templateProcessor->setValue('agente','Ruido');
        $templateProcessor->setValue('calle_numero',$instalacion->calle_numero);
        $templateProcessor->setValue('instalacion',$instalacion->instalacion);
        $templateProcessor->setValue('descripcion_instalacion',$instalacion->descripcion_instalacion);
        $templateProcessor->setValue('area_geografica',$instalacion->area_geografica);
        $templateProcessor->setValue('region_geografica',$instalacion->region_geografica);
        $templateProcessor->setValue('area_monitoreada',$instalacion->area_monitoreada);
        $templateProcessor->saveAs('Formato_Ruido.docx');
        return response()->download('Formato_Ruido.docx')->deleteFileAfterSend(true);
    }
}
