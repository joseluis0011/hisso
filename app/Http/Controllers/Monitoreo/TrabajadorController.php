<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\InstalacionExport;
use App\Exports\TrabajadorExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Monitoreo\Trabajador\GuardarInstalacionRequest;
use App\Http\Requests\Monitoreo\Trabajador\GuardarTrabajadorRequest;
use App\Imports\InstalacionImport;
use App\Imports\TrabajadorImport;
use App\Models\Instalacion;
use App\Models\RequerimientoProyecto;
use App\Models\TempInstalacion;
use App\Models\TemTrabajador;
use App\Models\Trabajador;
use App\Repo\RepoTrabajador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class TrabajadorController extends Controller
{
    public function getProyecto(){
        return RepoTrabajador::getProyecto();
    }
    public function getEmpresa(Request $request){
        return RepoTrabajador::searchEmpres($request);
    }
    public function importTrabajador(Request $request){
        $this->validacion($request);
        try{
            $file =$request->file('file');
            $codigo=$request->codinterno;
            $data=Excel::import(new TrabajadorImport($codigo),$file);
            if ($data) {
                RequerimientoProyecto::create([
                    'idagente' => $codigo,
                    'tipo' => 'Trabajador'
                ]);
            }
            return ['success'=>true,'message'=>'Guardado Correctamente'];
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e){
            $failures = $e->failures();
    
            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
                return ['success'=>false,'message'=>$failure];
            }
        }
    }
    public function importInstalacion(Request $request){
        $this->validacion($request);
        try{
            $file =$request->file('file');
            $codigo=$request->codigo;
            $data=Excel::import(new InstalacionImport($codigo),$file);
            if ($data) {
                RequerimientoProyecto::create([
                    'idproyecto' => $codigo,
                    'tipo' => 'Instalacion'
                ]);
            }
            return ['success'=>true,'message'=>'Guardado Correctamente'];
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e){
            $failures = $e->failures();
    
            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
                return ['success'=>false,'message'=>$failure];
            }
        }
    }
    public function validacion($data)
    {
       if ($data->tipo == 'trabajador'){
           $rules = [
               'file' => "bail|required|mimes:xls,xlsx",
               'codinterno' => "required|unique:trabajador,idagente,".$data['codinterno'],
               'codigo'=>'required'
           ];
           $message = [
               'codinterno.required' => 'El Campo Codigo es Obligatorio',
               'codinterno.unique' => 'El Campo Codigo ya esta Registrado',
               'file.required'=>'El Campo es Obligatorio',
               'file.mimes' => 'Formato invalido',
               'codigo.required'=>'El Campo Codigo es Obligatorio'
    
           ];
           $this->validate($data, $rules, $message);
       }else{
           $rules = [
               'file' => "bail|required|mimes:xls,xlsx",
               'codigo' => "required|unique:instalacion,idproyecto,".$data['codigo'],
           ];
           $message = [
               'codigo.required' => 'El Campo Codigo es Obligatorio',
               'codigo.unique' => 'El Campo Codigo ya esta Registrado',
               'file.required'=>'El Campo es Obligatorio',
               'file.mimes' => 'Formato invalido'
    
           ];
           $this->validate($data, $rules, $message);
       }
    }
    public function index(Request $request){
        if ($request->tipo == 0){
            if ($request->buscar == ''){
                $activo=RepoTrabajador::listarTrabajador(0);
            }else{
                $activo=RepoTrabajador::buscarTrabajador($request->select,$request->buscar,0);
            }
            return [
                'pagination' => [
                    'total'        => $activo->total(),
                    'current_page' => $activo->currentPage(),
                    'per_page'     => $activo->perPage(),
                    'last_page'    => $activo->lastPage(),
                    'from'         => $activo->firstItem(),
                    'to'           => $activo->lastItem(),
                ],
                'activo' => $activo
            ];
        }else{
            if ($request->buscar == ''){
                $inactivo=RepoTrabajador::listarTrabajador(1);
            }else{
                $inactivo=RepoTrabajador::buscarTrabajador($request->select,$request->buscar,1);
            }
            return [
                'pagination1' => [
                    'total'        => $inactivo->total(),
                    'current_page' => $inactivo->currentPage(),
                    'per_page'     => $inactivo->perPage(),
                    'last_page'    => $inactivo->lastPage(),
                    'from'         => $inactivo->firstItem(),
                    'to'           => $inactivo->lastItem(),
                ],
                'inactivo' => $inactivo
            ];
        }
    }
    public function verTrabajador(Request $request){
        if ($request->buscar == ''){
            $lista=RepoTrabajador::listarData($request['id'],$request['estado']);
        }else{
            $lista=RepoTrabajador::SearchData($request['id'],$request['estado'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function getdata(Request $request){
        return DB::table('proyecto as p')
            ->join('empresa as e','e.id','=','p.idempresa')
            ->where('p.id',$request['id'])
            ->select('e.razon_social')
            ->get();
    }
    public function agregarInstalacion(Request $request){
       $resp =RepoTrabajador::agregarInstalacion($request);
       if ($resp){
           return ['success'=>true];
       }else{
           return ['success'=>false];
       }
    }
    public function getInstalacion(Request $request){
        return RepoTrabajador::getdata($request);
    }
    public function eliminar(Request $request){
       $resp = RepoTrabajador::eliminar($request);
       if ($resp){
           return ['success'=>true];
       }else{
           return ['success'=>false];
       }
    }
    public function eliminarData(Request $request){
        $resp = RepoTrabajador::eliminarData($request);
        if ($resp){
            return ['success'=>true];
        }else{
            return['success'=>false];
        }
    }
    public function exportExcel(Request $request){
        if ($request->estado == 'Instalacion'){
            return Excel::download(new InstalacionExport($request->id),'Instalacion.xlsx');
        }else{
            return Excel::download(new TrabajadorExport($request->id),'Trabajador.xlsx');
        }
    }
    public function formatoTrabajador(){
        $path = public_path().'/formato_excel/trabajador.xlsx';
        return response()->download($path);
    }
    public function formatoInstalacion(){
        $path = public_path().'/formato_excel/instalacion.xlsx';
        return response()->download($path);
    }
    public function guardarTrabajador(GuardarTrabajadorRequest $request){
        return RepoTrabajador::agregarTrabajador($request);
    }
    public function guardarInstalacion(GuardarInstalacionRequest $request){
        return RepoTrabajador::agregarInstalacionp($request);
    }
}
