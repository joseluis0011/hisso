<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\EquiposExport;
use App\Http\Controllers\Controller;
use App\Repo\RepoEquipo;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EquipoController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tipo == 0) {
            if ($request->buscar == '') {
                $activo = RepoEquipo::estadoEquipo([0,2], 1);
            } else {
                $activo = RepoEquipo::buscarEquipo([0,2], $request->buscar,$request->select);
            }
            return [
                'pagination' => [
                    'total' => $activo->total(),
                    'current_page' => $activo->currentPage(),
                    'per_page' => $activo->perPage(),
                    'last_page' => $activo->lastPage(),
                    'from' => $activo->firstItem(),
                    'to' => $activo->lastItem(),
                ],
                'equipoActivo' => $activo
            ];
        } else {
            if ($request->buscar == '') {
                $inactivo = RepoEquipo::estadoEquipo([1], 1);
            } else {
                $inactivo = RepoEquipo::buscarEquipo([1], $request->buscar,$request->select);
            }
            return [
                'pagination1' => [
                    'total' => $inactivo->total(),
                    'current_page' => $inactivo->currentPage(),
                    'per_page' => $inactivo->perPage(),
                    'last_page' => $inactivo->lastPage(),
                    'from' => $inactivo->firstItem(),
                    'to' => $inactivo->lastItem(),
                ],
                'equipoInactivo' => $inactivo
            ];
        }
    }
    public function agregarEquipo(Request $request)
    {
        $this->validacion($request);
        $dato = RepoEquipo::agregarEquipo($request);
        if ($dato) {
            return ['success' => true];
        } else {
            return ['error' => false];
        }
    }
    public function estadoEquipo(Request $request){
        return RepoEquipo::estado($request);
        // https://es.stackoverflow.com/questions/24454/enviar-email-con-informacion-de-mysql
    }
    public function validacion($data)
    {
        $rules = [
            'formulario.nombre' => "required",
            'formulario.marca' => "required",
            'formulario.modelo' => "required",
            'formulario.serie' => "bail|required|unique:equipos,serie,".$data['id'],
            'formulario.fecha' => "required"
        ];
        $message = [
            'formulario.nombre.required' => 'El Campo Nombre es Obligatorio',
            'formulario.serie.unique' => 'La Serie del Equipo Ya esta Registrado',
            'formulario.marca.required' => "El Campo Marca es Obligatorio",
            'formulario.modelo.required' => "El Campo Modelo es Obligatorio",
            'formulario.serie.required' => "El Campo Serie es Obligatorio",
            'formulario.fecha.required' => "El Campo Fecha de Calibracion es Obligatorio"
        ];
        $this->validate($data, $rules, $message);
    }
    public function getdata(Request $request){
        return RepoEquipo::getdata($request['id']);
    }
    public function exportExcel(Request $request){
        return Excel::download(new EquiposExport,'Equipos.xlsx');
    }
    public function detalle(Request $request){
        if ($request->buscar == '') {
            $activo = RepoEquipo::estadoEquipoDetalle( 1,$request->id);
        } else {
            $activo = RepoEquipo::buscarEquipoDetalle($request->buscar,$request->select);
        }
        return [
            'pagination' => [
                'total' => $activo->total(),
                'current_page' => $activo->currentPage(),
                'per_page' => $activo->perPage(),
                'last_page' => $activo->lastPage(),
                'from' => $activo->firstItem(),
                'to' => $activo->lastItem(),
            ],
            'detalle' => $activo
        ];
    }
    public function addDetalle(Request $request){
        $this->validacionDetalle($request);
        $data = RepoEquipo::addEquipos($request['id'],$request['motivo']);
        if ($data){
            return ['success'=>true];
        }else{
            return ['success'=>false];
        }
    }
    public function validacionDetalle($data)
    {
        $rules = [
            'motivo' => "required|unique:equipos_detalle,motivo,".$data['id']
        ];
        $message = [
            'motivo.required' => 'El Campo Motivo es Obligatorio',
            'motivo.unique' => 'El Motivo del Equipo Ya esta Registrado'
        ];
        $this->validate($data, $rules, $message);
    }
    public function getpersona(){
        return RepoEquipo::getPersona();
    }
    public function getDashboard(){
        return RepoEquipo::getDashboard();
    }
    public function getequipo(Request $request){
        return RepoEquipo::getequipo($request);
    }
}
