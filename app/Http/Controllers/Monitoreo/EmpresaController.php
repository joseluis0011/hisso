<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\EmpresaExport;
use App\Http\Controllers\Controller;
use App\Repo\RepoEmpresa;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class EmpresaController extends Controller
{
    public function apiRuc(Request $request)
    {
        $dato = RepoEmpresa::searchRuc($request->ruc);
        if ($dato) {
            return ['error' => false];
        } else {
            return RepoEmpresa::apiRuc($request->ruc);
        }
    }
    public function agregarEmpresa(Request $request)
    {
        $this->validacion($request);
        try {
            DB::beginTransaction();
            $ubigeo=RepoEmpresa::agregarUbigeo($request);
            $empresa = RepoEmpresa::agregarEmpresa($request,$ubigeo->id);
            $contacto = RepoEmpresa::agregarContacto($request,$empresa->id);
            $hEmpresa=RepoEmpresa::hEmpresaProyectoTrabajadores($request,$empresa->id);
            DB::commit();
            return response()->json(array("success" => true,'message'=>'Guardado Correctamente'));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error'=>false,'message'=>$e->getMessage()]);
        }
    }
    public function validacion($data)
    {
        $rules = [
            'formulario.ruc' => "bail|required|digits:11|unique:empresa,ruc,".$data['formulario']['idempresa'],
            'formulario.razon_social' => "required",
            'formulario.actividad_economica' => "required",
            'formulario.domicilio_fiscal' => "required",
            'formulario.provincia' =>'required',
            'formulario.departamento' =>'required',
            'formulario.distrito' =>'required',
            'contacto'=>"required"
        ];
        $message = [
            'formulario.ruc.required' => 'El Campo Ruc es Obligatorio',
            'formulario.ruc.digits' => 'El Campo Ruc debe Tener 11 Digitos',
            'formulario.ruc.unique' => 'El Ruc Ya esta Registrado',
            'formulario.razon_social.required' => "El Campo Razon Social es Obligatorio",
            'formulario.actividad_economica.required' => "El Campo Actividad Economica es Obligatorio",
            'formulario.actividad_economica.regex' => "El Campo Actividad Economica tiene un formato invalido",
            'formulario.domicilio_fiscal.required' => "El Campo Domicilio Fiscal es Obligatorio",
            'formulario.domicilio_fiscal.regex' => "El Campo Domicilio Fiscal tiene un formato invalido",
            'formulario.provincia.required' => "El Campo Numero de Trabajadores es Obligatorio",
            'formulario.departamento.required' => "El Campo Numero de Trabajadores es Obligatorio",
            'formulario.distrito.required' => "El Campo Numero de Trabajadores es Obligatorio",
            'contacto.required'=>'Debe añadir al menos un Contacto'
        ];
        $this->validate($data, $rules, $message);
    }
    public function valContacto($data){
        $rules = [
            'contacto.nombre' => "required|regex:/^[\pL\s\-]+$/u",
            'contacto.correo' => "required_with:contacto.nombre|email",
            'contacto.celular' => "required_with:contacto.correo|digits:9",
            'contacto.cargo' => "required"
        ];
        $message = [
            'contacto.nombre.regex'=>'El Campo Nombre debe ser solo Letras',
            'contacto.nombre.required'=>'El Campo Nombre es Obligatorio',
            'contacto.correo.required_with'=>'El Campo Correo es Obligatorio',
            'contacto.correo.email'=>'El Campo Correo debe ser una Direccion Email Valida',
            'contacto.celular.required_with'=>'El Campo Celular es Obligatorio',
            'contacto.celular.digits'=>'El Campo Celular es debe tener 9 Digitos',
            'contacto.cargo.required'=>'El Campo Cargo es Obligatorio'
        ];
        $this->validate($data, $rules, $message);
    }
    public function addContacto(Request $request){
        $this->valContacto($request);
        return response()->json(['success'=>true]);
    }
    public function estadoEmpresa(Request $request){
        return RepoEmpresa::estado($request);
    }
    public function getdata(Request $request){
        return RepoEmpresa::getdata($request->id);
    }
    public function exportExcel(){
        return Excel::download(new EmpresaExport,'clientes.xlsx');
    }
    public function dashboard(){
        return RepoEmpresa::dashboard();
    }
    public function addTrabajador(Request $request){
        $this->valTrabajador($request);
        switch ($request['trabajadores']['idmes']){
            case '01':
                $mes='Enero';
                break;
            case '02':
                $mes='Febrero';
                break;
            case '03':
                $mes='Marzo';
                break;
            case '04':
                $mes='Abril';
                break;
            case '05':
                $mes='Mayo';
                break;
            case '06':
                $mes='Junio';
                break;
            case '07':
                $mes='Julio';
                break;
            case '08':
                $mes='Agosto';
                break;
            case '09':
                $mes='Setiembre';
                break;
            case '10':
                $mes='Octubre';
                break;
            case '11':
                $mes='Noviembre';
                break;
            case '12':
                $mes='Diciembre';
                break;
        }
        return response()->json(['success'=>true,'mes'=>$mes,'year'=>$request['trabajadores']['year']]);
    }
    public function valTrabajador($data){
        $rules = [
            'trabajadores.idmes' => [
                'required',
                Rule::unique('hproyectoempresa','mes')->where('idempresa',$data['idempresa'])
            ],
            'trabajadores.numero'=>"required",
            'trabajadores.year'=>'required'
        ];
        $message = [
            'trabajadores.idmes.required'=>'El Campo Mes es Obligatorio',
            'trabajadores.idmes.unique'=>'El Campo Mes Ya esta Agregado',
            'trabajadores.numero.required'=>'El Campo Numero Trabajadores es Obligatorio',
            'trabajadores.year.required'=>'El Campo Año es Obligatorio'
        ];
        $this->validate($data, $rules, $message);
    }
    public function delete(Request $request){
        return RepoEmpresa::deleteEmpresa($request);
    }
    public function getEmpresa(){
        return RepoEmpresa::getEmpresax();
    }
}
