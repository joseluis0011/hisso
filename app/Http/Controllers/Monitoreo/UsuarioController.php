<?php

namespace App\Http\Controllers\Monitoreo;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsuarioRequest;
use App\Repo\RepoPersona;
use App\Repo\RepoUsuario;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public function index(Request $request){
        if ($request->tipo == 0) {
            if ($request->buscar == '') {
                $activo = RepoUsuario::estadoUsuario(0);
            } else {
                $activo = RepoUsuario::buscarUsuario(0, $request->buscar,$request->select);
            }
            return [
                'pagination' => [
                    'total' => $activo->total(),
                    'current_page' => $activo->currentPage(),
                    'per_page' => $activo->perPage(),
                    'last_page' => $activo->lastPage(),
                    'from' => $activo->firstItem(),
                    'to' => $activo->lastItem(),
                ],
                'activo' => $activo
            ];
        } else {
            if ($request->buscar == '') {
                $inactivo = RepoUsuario::estadoUsuario(1);
            } else {
                $inactivo = RepoUsuario::buscarUsuario(1, $request->buscar,$request->select);
            }
            return [
                'pagination1' => [
                    'total' => $inactivo->total(),
                    'current_page' => $inactivo->currentPage(),
                    'per_page' => $inactivo->perPage(),
                    'last_page' => $inactivo->lastPage(),
                    'from' => $inactivo->firstItem(),
                    'to' => $inactivo->lastItem(),
                ],
                'inactivo' => $inactivo
            ];
        }
    }
    public function addUsuario(Request $request){
        $this->validacion($request);
        try{
           $usuario = RepoUsuario::agregarUsuario($request);
           $persona = RepoUsuario::agregarPersona($request,$usuario->id);
            return ['success'=>true];
        }catch (\Exception $e){
            return ['success'=>false,'message'=>$e->getMessage()];
        }
    }
    public function obpassword(Request $request){
        return RepoUsuario::obtenerPassword($request);
    }
    public function estadoUsuario(Request $request){
        return RepoUsuario::estado($request);
    }
    public function getData(Request $request){
        return RepoUsuario::getdata($request);
    }
    public function validacion($data)
    {
        $rules = [
            'formulario.usuario'=>"required|unique:usuario,usuario,".$data['idusuario'],
            'formulario.password'=>'required',
            'formulario.rol'=>'required',
            'formulario.personal'=>'required'
        ];
        $message = [
            'formulario.usuario.required' => 'El Campo Usuario es Obligatorio',
            'formulario.password.required' => 'El Campo Contraseña es Obligatorio',
            'formulario.rol.required' => 'El Campo Rol es Obligatorio',
            'formulario.personal.required' => 'El Campo Responsable de Ejecución es Obligatorio'
    
        ];
        $this->validate($data, $rules, $message);
    }
}
