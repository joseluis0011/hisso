<?php

namespace App\Http\Controllers\Monitoreo;

use App\Http\Controllers\Controller;
use App\Repo\RepoHomologacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomologacionController extends Controller
{
    // Contratista
    public function addencuesta(Request $request){
     $data= RepoHomologacion::addEncuesta($request);
         if ($data){
             return ['success'=>true];
         }else{
             return ['success'=>false];
         }
    }
   public function getinfo(){
        return RepoHomologacion::getinfomation();
   }
   // Revisor
    public function listClientes(Request $request){
        if ($request->buscar == ''){
            $lista=RepoHomologacion::listClientes();
        }else{
            $lista=RepoHomologacion::searchClientes($request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function addCliente(Request $request){
        try{
            DB::beginTransaction();
            $usuario = RepoHomologacion::addUsuario($request,5);
            $cliente = RepoHomologacion::addCliente($request,$usuario->id);
            DB::commit();
            return ['success'=>true];
        }catch ( \Exception $e){
            DB::rollback();
            return ['success'=>false,'message'=>$e->getMessage()];
        }
    }
    public function getdataCliente(Request $request){
        return RepoHomologacion::getDataCliente($request);
    }
    public function addClienteHijo(Request $request){
        return RepoHomologacion::addUsuario($request,4);
    }
    public function listContratistas(Request $request){
        if ($request->buscar == ''){
            $lista=RepoHomologacion::listContratistas($request['id']);
        }else{
            $lista=RepoHomologacion::searchContratista($request['id'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    // cliente
    public function listarContratista(Request $request){
        if ($request->buscar == ''){
            $lista=RepoHomologacion::listContratistasbyCliente();
        }else{
            $lista=RepoHomologacion::searchContratistabyCliente($request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
}
