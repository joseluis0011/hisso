<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\IluminacionExport;
use App\Exports\InstalacionExport;
use App\Http\Controllers\Controller;
use App\Models\Trabajador;
use App\Repo\RepoIluminacion;
use App\Repo\RepoTrabajador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpWord\TemplateProcessor;

class IluminacionController extends Controller
{
    public function index(Request $request){
        if ($request->buscar == ''){
            $lista=RepoIluminacion::listTrabajador($request['idagente']);
        }else{
            $lista=RepoIluminacion::searchTrabajador($request['idagente'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function addTrabajador(Request $request){
        $data= RepoTrabajador::agregarInstalacion($request);
        if ($data){
            return ['success'=>true];
        }else{
             return ['success' => false];
        }
    }
    public function addDetalleIluminacion(Request $request){
        $this->validacion($request);
        try {
            DB::beginTransaction();
                $add = RepoIluminacion::addDetalleIluminacion($request);
            DB::commit();
            return response()->json(array("success" => true));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error'=>false]);
        }
    
    }
    public function validacion($data){
        $rules = [
            'punto' => "bail|required",
            'fecha' => "bail|required",
            'hora' => "bail|required",
            'max' => "bail|required",
            'max1' => "bail|required",
            'max2' => "bail|required",
            'min' => "bail|required",
            'min2' => "bail|required",
            'avg' => "bail|required",
            'avg1' => "bail|required",
            'avg2' => "bail|required",
            'tipo_iluminaria' => "bail|required",
            'trabajo_realizado' => "bail|required",
            'caracteristica' => "bail|required",
            'altura_iluminaria' => "bail|required",
            'trabajadores_expuestos' => "bail|required"
        ];
        $message = [
            'punto.required' => 'El Campo Punto Monitoreo es Obligatorio',
            'fecha.required' => 'El Campo Fecha es Obligatorio',
            'hora.required' => 'El Campo Hora es Obligatorio',
            'max.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'max1.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'max2.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'min.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'min1.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'min2.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'avg.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'avg1.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'avg2.required' => "Completa todos los Campos de Nivel de Iluminacion",
            'tipo_iluminaria.required' => "El Campo Tipo Iluminaria es Obligatorio",
            'trabajo_realizado.required' => "El Campo Trabajo Realizado Durante la Medicion es Obligatorio",
            'caracteristica.required' => "El Campo Caracteristicas del Entorno es Obligatorio",
            'altura_iluminaria.required' => "El Campo Altura Iluminaria es Obligatorio",
            'trabajadores_expuestos.required' => "El Campo Numero de Trabajadores Expuestos es Obligatorio",
        ];
        $this->validate($data, $rules, $message);
    }
    public function getIluminacion(Request $request){
        return RepoIluminacion::getIluminacion($request);
    }
    public function exportExcelTrabajadores(Request $request){
        return Excel::download(new IluminacionExport($request->idproyecto),'Monitoreo Iluminacion.xlsx');
    }
    public function exportFormatoLey(Request $request){
        $empresa = DB::table('empresa as e')
            ->join('proyecto as p','p.idempresa','=','e.id')
            ->where('p.id',$request->idproyecto)
            ->select('e.razon_social','e.ruc','e.domicilio_fiscal','e.act_economica','e.numero_trabajadores')
            ->get();
        $trabajador = Trabajador::where('idproyecto',$request->idproyecto)->first();
        $templateProcessor = new TemplateProcessor('formato_ley/formato_ley.docx');
        $templateProcessor->setValue('razon_social',$empresa[0]->razon_social);
        $templateProcessor->setValue('ruc',$empresa[0]->ruc);
        $templateProcessor->setValue('domicilio_fiscal',$empresa[0]->domicilio_fiscal);
        $templateProcessor->setValue('act_economica',$empresa[0]->act_economica);
        $templateProcessor->setValue('numero_trabajadores',$empresa[0]->numero_trabajadores);
        $templateProcessor->setValue('agente','Iluminación');
        $templateProcessor->setValue('calle_numero',$trabajador->calle_numero);
        $templateProcessor->setValue('instalacion',$trabajador->instalacion);
        $templateProcessor->setValue('descripcion_instalacion',$trabajador->descripcion_instalacion);
        $templateProcessor->setValue('area_geografica',$trabajador->area_geografica);
        $templateProcessor->setValue('region_geografica',$trabajador->region_geografica);
        $templateProcessor->setValue('area_monitoreada',$trabajador->area_monitoreada);
        $templateProcessor->saveAs('Formato_Iluminacion.docx');
        return response()->download('Formato_Iluminacion.docx')->deleteFileAfterSend(true);
    }
}
