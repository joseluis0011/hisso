<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\BiologicoExport;
use App\Http\Controllers\Controller;
use App\Repo\RepoBiologico;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BiologicoController extends Controller
{
    public function index(Request $request){
        if ($request->buscar == ''){
            $lista=RepoBiologico::listInstalacion($request['idproyecto']);
        }else{
            $lista=RepoBiologico::searchInstalacion($request['idproyecto'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function addDetalleBiologico(Request $request){
      $data = RepoBiologico::addDetalleBiologico($request);
      if ($data){
          return ['success'=> true];
      }else{
          return ['success'=>false];
      }
    }
    public function getBiologico(Request $request){
        return RepoBiologico::getDetalleBiologico($request);
    }
    public function exportExcelInstalacion(Request $request){
        return Excel::download(new BiologicoExport($request->idproyecto),'Monitoreo Biologico.xlsx');
    }
}
