<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\ResponsableExport;
use App\Http\Controllers\Controller;
use App\Repo\RepoEmpresa;
use App\Repo\RepoPersona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PersonaController extends Controller
{
    public function agregarPersona(Request $request){
        $this->validacion($request);
        try{
            DB::beginTransaction();
            $usuario=RepoPersona::agregarUsuario($request);
            if ($request['formulario']['departamento'] AND $request['formulario']['provincia'] AND $request['formulario']['distrito']){
                $ubigeo=RepoPersona::agregarUbigeo($request);
                $persona=RepoPersona::agregarPersona($request,$ubigeo->id,$usuario->id);
            }else{
                $persona=RepoPersona::agregarPersona($request,null,$usuario->id);
            }
            DB::commit();
            return response()->json(array("success" => true));
        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['error'=>false,'message' => $e->getMessage()]);
        }
    }
    public function apiDni(Request $request){
       $data=RepoPersona::verificarDni($request);
       if ($data){
           return ['existe'=>true,'message'=>'Dni Ya esta Registrado'];
       }else{
            $dni=RepoPersona::consultaDni($request['dni']);
            if ($dni){
                return ['success'=>true,'dni'=>$dni];
            }else{
                return ['success'=>false,'message'=>'No se encontro la Persona intente manualmente'];
            }
       }
    }
    public function index(){
        $activo = RepoPersona::listarPersona([0,2], 1);
        $inactivo = RepoPersona::listarPersona([1], 1);
        return ['activo'=>$activo,'inactivo'=>$inactivo];
    }
    public function getdata(Request $request){
        return RepoPersona::getdata($request['id']);
    }
    public function estadoMonitor(Request $request){
        return RepoPersona::estado($request);
    }
    public function exportExcel(){
        return Excel::download(new ResponsableExport,'personal.xlsx');
    }
    public function validacion($data)
    {
        $rules = [
            'formulario.dni' => "required|digits:8|unique:persona,dni,".$data['idpersona'],
            'formulario.nombre' => "required",
            'formulario.apellido' => "required",
            'formulario.celular' => "required|digits:9",
            'formulario.sexo' => "required",
            'formulario.empresa' => "required",
            'formulario.idrol'=>'required',
            'formulario.provincia'=>'required_with:formulario.departamento',
            'formulario.distrito'=>'required_with:formulario.provincia',
            'formulario.fnacimiento'=>'nullable|sometimes|date|before:-18 years'
        ];
        $message = [
            'formulario.dni.required' => 'El Campo DNI es Obligatorio',
            'formulario.dni.unique' => 'El Campo DNI Ya esta Registrado',
            'formulario.dni.digits' => 'El Campo DNI debe tener 8 Digitos',
            'formulario.nombre.required' => "El Campo Nombre es Obligatorio",
            'formulario.apellido.required' => "El Campo Apellido es Obligatorio",
            'formulario.celular.required' => "El Campo Celular Obligatorio",
            'formulario.celular.digits' => "El Campo Celular debe tener 9 Digitos",
            'formulario.sexo.required' => "El Campo Sexo es Obligatorio",
            'formulario.empresa.required' => "El Campo Empresa es Obligatorio",
            'formulario.idrol.required' => "El Campo Cargo es Obligatorio",
            'formulario.provincia.required_with'=>'El Campo Provincia es Obligatorio si el Departamento esta Seleccionado',
            'formulario.distrito.required_with'=>'El Campo Distrito es Obligatorio si el Provincia esta Seleccionado',
            'formulario.fnacimiento.before'=>'El Campo Fecha Nacimiento - El Responsable debe tener mas de 18 años',
            'formulario.fnacimiento.date'=>'El Campo Fecha Nacimiento debe ser una Fecha Valida'
        ];
        $this->validate($data, $rules, $message);
    }
    public function getempresa(){
        return RepoPersona::getEmpresa();
    }
    public function addcargo(Request $request){
        $this->validacionCargo($request);
        $data= RepoPersona::addCargo($request);
        if (!$data){
            return ['success'=>false];
        }else{
            return ['success'=>true];
        }
    }
    public function validacionCargo($data)
    {
        $rules = [
            'cargo' => "required|unique:cargo,nombre,".$data['id'],
        ];
        $message = [
            'cargo.required' => 'El Campo Nombre es Obligatorio',
            'cargo.unique' => 'El Campo Nombre Ya esta Registrado',
        ];
        $this->validate($data, $rules, $message);
    }
    public function generaCodigoInterno(){
        return RepoPersona::generaCodigInterno();
    }
}
