<?php

namespace App\Http\Controllers\Monitoreo;

use App\Http\Controllers\Controller;
use App\Http\Requests\AsignacionProyectoRequest;
use App\Http\Requests\Monitoreo\Asignacion\ServicioRequest;
use App\Repo\RepoAsignacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AsignacionController extends Controller
{
    public function getCodigo(Request $request){
        return RepoAsignacion::getCodigo($request);
    }
    public function getProyectos(Request $request){
        return RepoAsignacion::getProyectos($request);
    }
    public function addOrUpdate(AsignacionProyectoRequest $request){
        try {
            DB::beginTransaction();
        $proyecto=RepoAsignacion::addproyecto($request);
         $qres=RepoAsignacion::addEquipoResponsable($request,$proyecto->id);
            DB::commit();
            return response()->json(array("success" => true,'message'=>'Guardado Correctamente'));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error'=>false,'message'=>$e->getMessage()]);
        }
    }
    public function listar(Request $request){
        if ($request->tipo == 0) {
            if ($request->buscar == "") {
                $activo = RepoAsignacion::listarAsignacion(0);
            } else {
                $activo = RepoAsignacion::searchAsignacion(0, $request->buscar,$request->select);
            }
            return [
                'pagination' => [
                    'total' => $activo->total(),
                    'current_page' => $activo->currentPage(),
                    'per_page' => $activo->perPage(),
                    'last_page' => $activo->lastPage(),
                    'from' => $activo->firstItem(),
                    'to' => $activo->lastItem(),
                ],
                'activo' => $activo
            ];
        } else {
            if ($request->buscar == '') {
                $inactivo = RepoAsignacion::listarAsignacion( 1);
            } else {
                $inactivo = RepoAsignacion::searchAsignacion(1, $request->buscar,$request->select);
            }
            return [
                'pagination1' => [
                    'total' => $inactivo->total(),
                    'current_page' => $inactivo->currentPage(),
                    'per_page' => $inactivo->perPage(),
                    'last_page' => $inactivo->lastPage(),
                    'from' => $inactivo->firstItem(),
                    'to' => $inactivo->lastItem(),
                ],
                'inactivo' => $inactivo
            ];
        }
    }
    public function getData(Request $request){
        return RepoAsignacion::getData($request);
    }
    public function cerrarProyecto(Request $request){
        return RepoAsignacion::cerrarProyecto($request);
    }
    public function valServicio(ServicioRequest $request){
        try {
            DB::beginTransaction();
        $equipo= RepoAsignacion::getItemEquipo($request);
        $responsable = RepoAsignacion::getItemResponsable($request);
        $codigo = RepoAsignacion::getItemAgente($request);
        if ($request['old_from']){
            RepoAsignacion::updateResponsable($request['old_from'][0]);
            RepoAsignacion::updateEquipo($request['old_from'][1]);
        }
            DB::commit();
            return ['success'=>true,'equipos'=>$equipo,'responsable'=>$responsable,'codigo'=>$codigo];
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error'=>false,'message'=>$e->getMessage()]);
        }
    }
    public function eliminarResponsable(Request $request){
        return RepoAsignacion::eliminarResponsable($request);
    }
    public function searchid(Request $request){
        return RepoAsignacion::searchid($request);
    }
}
