<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\ErgonomiaExport;
use App\Http\Controllers\Controller;
use App\Repo\RepoErgonomia;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ErgonomiaController extends Controller
{
    public function index(Request $request){
        if ($request->buscar == ''){
            $lista=RepoErgonomia::listTrabajador($request['idproyecto']);
        }else{
            $lista=RepoErgonomia::searchTrabajador($request['idproyecto'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function addDetalleErgonomia(Request $request){
        $data= RepoErgonomia::addDetErgonomia($request);
        if ($data){
            return ['success'=>true];
        }else{
            return ['success'=>false];
        }
    }
    public function getErgonomia(Request $request){
        return RepoErgonomia::getDetalleErgominia($request);
    }
    public function exportExcelTrabajadores(Request $request){
        return Excel::download(new ErgonomiaExport($request->idproyecto),'Monitoreo Ergonomia.xlsx');
    }
}
