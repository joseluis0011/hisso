<?php

namespace App\Http\Controllers\Monitoreo;

use App\Http\Controllers\Controller;
use App\Http\Requests\EppAddTrabajador;
use App\Imports\TrabajadorEppImport;
use App\Repo\RepoEpp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpWord\TemplateProcessor;

class EppController extends Controller
{
    public function listar(Request $request){
        if ($request->buscar == ''){
            $lista=RepoEpp::listar($request['idagente']);
        }else{
            $lista=RepoEpp::search($request['idagente'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
   public function generarCodigo(Request $request){
        return RepoEpp::generarcodigoEpp($request);
   }
    public function equipoSeguridad(Request $request){
        return RepoEpp::listEquiposforEpp($request);
    }
    public function agregar(Request $request){
        try{
            DB::beginTransaction();
            $epp =RepoEpp::addEpp($request);
            Excel::import(new TrabajadorEppImport($request->codigo,$epp->id),$request->file('file'));
            DB::commit();
            return ['success'=>true,'message'=>'Agregado Correctamente','data'=>$epp->id];
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            DB::rollback();
            $failures = $e->failures();
            $error=[];
            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $error[]=$failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }
            return ['success'=>false,'message'=>$error];
        }
    }
    public function listTrabajador(Request $request){
        if ($request->buscar == ''){
            $lista=RepoEpp::listarTrabajador($request['idagente'],$request['idepp']);
        }else{
            $lista=RepoEpp::searchTrabajador($request['idagente'],$request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    public function updateTrabajadores(Request $request){
        if (RepoEpp::updateTrabajador($request)){
            return ['success'=>true,'message'=>'Agregado Correctamente'];
        }else{
            return ['success'=>false,'message'=>'Error'];
        }
        //return RepoEpp::updateTrabajadores($request);
    }
    public function addTrabajador(EppAddTrabajador $request){
        if (RepoEpp::addTrabajador($request)){
            return ['success'=>true,'message'=>'Agregado Correctamente'];
        }else{
            return ['success'=>false,'message'=>'Error'];
        }
    }
    public function getTrabajador(Request $request){
        return RepoEpp::getTrabajador($request);
    }
    public function deleteTrabajador(Request $request){
        if (RepoEpp::deleteTrabajador($request)){
            return ['success'=>true];
        }else{
            return ['success'=>false];
        }
    }
    public function formatoLey(Request $request){
        $data =json_decode($request['array'],true);
        //$headers=array_keys($data[0]);
        //array_unshift($data,$headers);
        $empresa = RepoEpp::getempresa($request['idempresa']);
        $epp =  RepoEpp::getEpp($request['idepp']);
        $idproyecto = RepoEpp::getproyecto($request['idagente']);
        $equipos = RepoEpp::getEquipos($request['idepp']);
        $arr = array_column($equipos,'nombre');
        $array=implode(";",$arr);
        // ----------------
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('formato_ley/epp.xlsx');
        $sheet =$spreadsheet->getActiveSheet();
        $sheet->setCellValue('A7',$empresa[0]['razon_social'])->mergeCells('A7:F7');
        $sheet->setCellValue('G7',$empresa[0]['ruc'])->mergeCells('G7:J7');
        $sheet->setCellValue('K7',$empresa[0]['domicilio_fiscal'])->mergeCells('K7:X7');
        $sheet->setCellValue('Y7',$empresa[0]['act_economica'])->mergeCells('Y7:AK7');
        $sheet->setCellValue('AL7',$empresa[0]['numero_trabajadores'])->mergeCells('AL7:AQ7');
        if ($epp[0]['tipo_equipo'] == 0){
            $sheet->setCellValue('P11','x');
        }else{
            $sheet->setCellValue('AK11','x');
        }
        $sheet->fromArray([$array],null,'A13')->mergeCells('A13:AQ13');
        $row =16;
        $contador =1;
        foreach ($data as $dat){ // B16:O16
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->getHighestRow()+1;
            $sheet->insertNewRowBefore($row);
            $sheet->setCellValue('A'.$row,$contador);
            $sheet->setCellValue('B'.$row,$dat['nombres'])->mergeCells('B'.$row.':O'.$row);
            $sheet->setCellValue('P'.$row,$dat['dni'])->mergeCells('P'.$row.':S'.$row);
            $sheet->setCellValue('T'.$row,$dat['area'])->mergeCells('T'.$row.':Z'.$row);
            if ($dat['fecha_entrega'] == "null"){
                $sheet->setCellValue('AA'.$row,'')->mergeCells('AA'.$row.':AE'.$row);
            }else{
                $sheet->setCellValue('AA'.$row,$dat['fecha_entrega'])->mergeCells('AA'.$row.':AE'.$row);
            }
            $contador = $contador +1;
            $row++;
        }
        //$sheet->fromArray($data,null,'B16');
        // styles
        $sheet->getStyle('A7')->getFont()->setSize(9);
        $sheet->getStyle('G7')->getFont()->setSize(9);
        $sheet->getStyle('K7')->getFont()->setSize(9);
        $sheet->getStyle('Y7')->getFont()->setSize(9);
        $sheet->getStyle('AL7')->getFont()->setSize(9);
        $date = date('d-m-y-'.substr((string)microtime(), 1, 8));
        $date = str_replace(".", "", $date);
        $filename = "Epp_".$date.".xlsx";
        try{
            $writer = new Xlsx($spreadsheet);
            $writer->save('php://output');
            $content = file_get_contents($filename);
            return ['success'=>true];
        }catch (\Exception $e){
            return ['success'=>false,'message'=>$e->getMessage()];
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=".$filename);
        header('Cache-Control: max-age=0');
        unlink($filename);
        exit($content);
    
        //dd($array);
    }
    public function puestoTrabajo(Request $request){
        return RepoEpp::puestoTrabajo($request);
    }
}
