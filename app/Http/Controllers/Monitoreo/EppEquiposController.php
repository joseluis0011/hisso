<?php

namespace App\Http\Controllers\Monitoreo;

use App\Http\Controllers\Controller;
use App\Http\Requests\EquiposEPPRequest;
use App\Http\Requests\EquiposTrabajadorEPPRequest;
use App\Repo\RepoEpp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EppEquiposController extends Controller
{
    public function generarCodigo(Request $request){
        return RepoEpp::generarcodigoEquipo($request);
    }
    public function agregar(EquiposEPPRequest $request){
        try {
            DB::beginTransaction();
            $equipo = RepoEpp::addeppEquipos($request);
            DB::commit();
            return ['success'=>true,'message'=>'Agregado Correctamente'];
        } catch (\Exception $e) {
            DB::rollback();
            return ['success'=>false,'message'=>$e->getMessage()];
        }
    }
    public function listar(Request $request){
        if ($request->buscar == ''){
            $lista=RepoEpp::listarEquipo($request->idagente);
        }else{
            $lista=RepoEpp::searchEquipo($request->select,$request->buscar);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
    ///  ////
    public function agregarEquipoTrabajador(Request $request){
        return RepoEpp::addEquipoTrabajador($request);
    }
    public function listEquipoTrabajador(Request $request){
        return RepoEpp::listEquipoTrabajador($request);
    }
    public function eliminarEquipoTrabajador(Request $request){
        return RepoEpp::eliminarEquipoTrabajador($request);
    }
    public function eliminarEquipo(Request $request){
        return RepoEpp::eliminarEquipo($request);
    }
}
