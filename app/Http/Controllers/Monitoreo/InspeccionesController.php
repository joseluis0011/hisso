<?php

namespace App\Http\Controllers\Monitoreo;

use App\Exports\InspeccionesExport;
use App\Http\Controllers\Controller;
use App\Repo\RepoInspecciones;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InspeccionesController extends Controller
{
    public function addinspeccion(Request $request){
        $data= RepoInspecciones::addinspeccion($request);
        if ($data){
            return ['success'=>true];
        }else{
            return ['success'=>false];
        }
    }
    public function index(Request $request){
        if ($request->tipo == 0) {
            if ($request->buscar == '') {
                $activo = RepoInspecciones::listarInspeccion(0,$request->id);
            } else {
                $activo = RepoInspecciones::buscarInspeccion(0, $request->buscar,$request->select,$request->id);
            }
            return [
                'pagination' => [
                    'total' => $activo->total(),
                    'current_page' => $activo->currentPage(),
                    'per_page' => $activo->perPage(),
                    'last_page' => $activo->lastPage(),
                    'from' => $activo->firstItem(),
                    'to' => $activo->lastItem(),
                ],
                'activo' => $activo
            ];
        } else {
            if ($request->buscar == '') {
                $inactivo = RepoInspecciones::listarInspeccion(1,$request->id);
            } else {
                $inactivo = RepoInspecciones::buscarInspeccion(1, $request->buscar,$request->select,$request->id);
            }
            return [
                'pagination1' => [
                    'total' => $inactivo->total(),
                    'current_page' => $inactivo->currentPage(),
                    'per_page' => $inactivo->perPage(),
                    'last_page' => $inactivo->lastPage(),
                    'from' => $inactivo->firstItem(),
                    'to' => $inactivo->lastItem(),
                ],
                'inactivo' => $inactivo
            ];
        }
    }
    public function getlistcategoriaPeligro(){
        return RepoInspecciones::getlistCategoriaPeligro();
    }
    public function getlistsubcategoriaPeligro(Request $request){
        return RepoInspecciones::getSubcategoriaPeligro($request['id']);
    }
    public function getdata(Request $request){
        return RepoInspecciones::getdata($request);
    }
    public function estadoinspeccion(Request $request){
        return RepoInspecciones::estado($request);
    }
    public function exportExcel(){
        return Excel::download(new InspeccionesExport,'Inspecciones.xlsx');
    }
}
