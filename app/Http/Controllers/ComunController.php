<?php

namespace App\Http\Controllers;

use App\Repo\RepoComun;
use Illuminate\Http\Request;

class ComunController extends Controller
{
    public function obtenerCodigo(Request $request){
        return RepoComun::searchProyecto($request);
    }
    public function obtenerDataDetalle(Request $request){
        return RepoComun::getObtenerDataById($request);
    }
    public function verificarId(Request $request){
        return RepoComun::verificarId($request);
    }
    public function getRol(Request $request){
        return RepoComun::getRol();
    }
    public function getResponsable(){
        return RepoComun::getResponsable();
    }
    public function getCargo(){
        return RepoComun::getcargo();
    }
    public function searchEmpresa(Request $request){
        return RepoComun::searchEmpresa($request);
    }
    public function obteneridProyecto(Request $request){
        return RepoComun::obteneriDproyecto($request);
    }
    public function obteneridProyectoandEpp(Request $request){
        return RepoComun::obteneridProyectoandEpp($request);
    }
    public function getServicios(){
        return RepoComun::getServicios();
    }
    public function getMonitoreo(Request $request){
        return RepoComun::getMonitoreo($request);
    }
    public function getCategoria(Request $request){
        return RepoComun::getCategoria($request);
    }
    public function getdepartamento(){
        return RepoComun::getDepartamento();
    }
    public function getprovincia(Request $request){
        return RepoComun::getProvincia($request);
    }
    public function getdistrito(Request $request){
        return RepoComun::getDistrito($request);
    }
}
