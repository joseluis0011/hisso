<?php

namespace App\Http\Controllers\Trabajador;

use App\Http\Controllers\Controller;
use App\Http\Requests\EncuestaCovidRequest;
use App\Repo\RepoEncuestaCovid;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\PDF;
class EncuestaCovidController extends Controller
{
    public function agregar(EncuestaCovidRequest $request){
        $data =RepoEncuestaCovid::agregarEncuesta($request);
         if ($data){
             return RepoEncuestaCovid::validatorArray($request);
         }else{
             return ['success'=>false];
         }
    }
    public function verificar(){
        return RepoEncuestaCovid::verificar();
    }
    public function download(){
        $data = RepoEncuestaCovid::generarPDF();
        $empresa = RepoEncuestaCovid::getEmpresaPDF();
        $now = Carbon::now();
        $hours = Carbon::parse($now);
        $hours->addHours(6);
           return \PDF::loadView('reporte.pase.index',['data'=>$data,'now'=>$now,'addhours'=>$hours,'empresa'=>$empresa])
               ->setPaper('a4','landscape')
               ->download('Pase-laboral.pdf');
    }
    public function preguntas(){
        return RepoEncuestaCovid::getPreguntas();
    }
}
