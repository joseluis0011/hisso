<?php

namespace App\Http\Controllers;

use App\Repo\RepoPermisos;
use Illuminate\Http\Request;

class PermisosController extends Controller
{
    public function permisosUsuario(Request $request){
        return RepoPermisos::permisosByRol($request);
    }
}
