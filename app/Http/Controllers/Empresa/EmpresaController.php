<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use App\Repo\RepoCovidEmpresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    public function listar(Request $request){
        if ($request->buscar == ''){
            $lista=RepoCovidEmpresa::listar();
        }else{
            $lista=RepoCovidEmpresa::search($request['buscar']);
        }
        return [
            'pagination' => [
                'total'        => $lista->total(),
                'current_page' => $lista->currentPage(),
                'per_page'     => $lista->perPage(),
                'last_page'    => $lista->lastPage(),
                'from'         => $lista->firstItem(),
                'to'           => $lista->lastItem(),
            ],
            'lista' => $lista
        ];
    }
}
