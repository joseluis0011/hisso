-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 10-08-2021 a las 19:59:48
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hisso_prueba`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `encuestausuarioyfecha` (IN `fechainicio` DATE, IN `fechafinal` DATE, IN `idusuario` INT)  NO SQL
BEGIN
SET @sql = NULL;
SELECT
    GROUP_CONCAT(DISTINCT
    CONCAT(
    'max(CASE WHEN ce.fecha = ''',
    date_format(fecha, '%Y-%m-%d'),
    ''' THEN r.respuesta END) AS `',
    date_format(fecha, '%Y-%m-%d'), '`'
     )
    ) INTO @sql
    FROM calendario
    where fecha>=fechainicio
    and fecha <= fechafinal;
    SET @sql
     = CONCAT('select ce.pregunta,', @sql, '
    from (select c.fecha, e.pregunta, e.idencuesta from calendario       c join encuestacovid e) ce
  LEFT JOIN respuestacovid r on ce.fecha = r.fecha and ce.idencuesta = r.pregunta and r.idusuario=',idusuario,' group by ce.pregunta order by ce.pregunta');
   PREPARE stmt FROM @sql;
   EXECUTE stmt;
 END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `reporte_proyecto` ()  BEGIN
insert into reporte_proyecto_requisitos SELECT NULL, empresa.razon_social, proyecto.codigo, proyecto.idempresa, proyecto.cantidad_trabajadores, proyecto.cantidad_sedes,proyecto.lima,proyecto.provincia, 
if (sum(ILUMINACION) = '1', 'si', 'no') AS ILUMINACION, 
if (sum(RUIDO) = '1', 'si', 'no') AS RUIDO,
if (sum(Ergonomia) = '1', 'si', 'no') AS ERGONOMIA,
if (sum(ASESORIA) = '1', 'si', 'no') AS ASESORIA,
if (sum(BIOLOGICO) = '1', 'si', 'no') AS BIOLOGICO,
if (sum(HUMEDAD) = '1', 'si', 'no') AS HUMEDAD,
if (sum(IPERC) = '1', 'si', 'no') AS IPERC,
if (sum(INSPECCION) = '1', 'si', 'no') AS INSPECCION,
if (sum(INVESTIGACION) = '1', 'si', 'no') AS INVESTIGACION,
if (sum(PSICOSOCIAL) = '1', 'si', 'no') AS PSICOSOCIAL,
if (sum(SUPERVISION) = '1', 'si', 'no') AS SUPERVISION, proyecto.estado FROM(
        select 
        idproyecto, 
        if (nombre = 'Iluminación', TRUE, FALSE) AS ILUMINACION, 
        '0' as ruido,
        '0' as ergonomia,
'0' as asesoria,
'0' as biologico,
'0' as humedad,
'0' as iperc,
'0' as inspeccion,
'0' as investigacion,
'0' as psicosocial,
'0' as supervision
        from agente
        UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
        if (nombre = 'Ruido', TRUE, FALSE)  AS RUIDO,
        '0' as ergonomia,
'0' as asesoria,
'0' as biologico,
'0' as humedad,
'0' as iperc,
'0' as inspeccion,
'0' as investigacion,
'0' as psicosocial,
'0' as supervision
        from agente
        UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
        '0' as ergonomia,
if (nombre = 'Asesoria', TRUE, FALSE)  AS Asesoria,
'0' as biologico,
'0' as humedad,
'0' as iperc,
'0' as inspeccion,
'0' as investigacion,
'0' as psicosocial,
'0' as supervision
        from agente
UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
'0' as ergonomia,
'0' as asesoria,
if (nombre = 'Biologico', TRUE, FALSE)  AS Biologico,
'0' as humedad,
'0' as iperc,
'0' as inspeccion,
'0' as investigacion,
'0' as psicosocial,
'0' as supervision
from agente
UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
        '0' as ergonomia,
'0' as asesoria,
'0' as biologico,
if (nombre = 'Humedad', TRUE, FALSE)  AS Humedad,
'0' as iperc,
'0' as inspeccion,
'0' as investigacion,
'0' as psicosocial,
'0' as supervision
from agente
UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
        '0' as ergonomia,
'0' as asesoria,
'0' as biologico,
'0' as humedad,
if (nombre = 'Iperc', TRUE, FALSE)  AS Iperc,
'0' as inspeccion,
'0' as investigacion,
'0' as psicosocial,
'0' as supervision
from agente
UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
        '0' as ergonomia,
'0' as asesoria,
'0' as biologico,
'0' as humedad,
'0' as iperc,
if (nombre = 'Inspección', TRUE, FALSE)  AS Inspección,
'0' as investigacion,
'0' as psicosocial,
'0' as supervision
from agente
UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
        '0' as ergonomia,
'0' as asesoria,
'0' as biologico,
'0' as humedad,
'0' as iperc,
'0' as inspeccion,
if (nombre = 'Investigación', TRUE, FALSE)  AS Investigación,
'0' as psicosocial,
'0' as supervision
from agente
UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
        '0' AS ergonomia,
'0' as asesoria,
'0' as biologico,
'0' as humedad,
'0' as iperc,
'0' as inspeccion,
'0' as investigacion,
if (nombre = 'Psicosocial', TRUE, FALSE)  AS psicosocial,
'0' as supervision
from agente
UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
        '0' AS ergonomia,
'0' as asesoria,
'0' as biologico,
'0' as humedad,
'0' as iperc,
'0' as inspeccion,
'0' as investigacion,
'0' as psicosocial,
if (nombre = 'Supervision', TRUE, FALSE)  AS Supervision
from agente
UNION ALL
        select 
        idproyecto,
        '0' as iluminacion,
         '0' as ruido,
        if (nombre = 'Ergonomia', TRUE, FALSE)  AS Ergonomia,
'0' as asesoria,
'0' as biologico,
'0' as humedad,
'0' as iperc,
'0' as inspeccion,
'0' as investigacion,
'0' as psicosocial,
'0' as supervision
from agente
        ) pro , proyecto, empresa
        where pro.idproyecto =proyecto.id
        and empresa.id=proyecto.idempresa
        group by idproyecto;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Codigo_Proyecto_Automatico` (IN `iempresa` INT)  BEGIN
Declare cod int(11);
declare verifica char(60) default null;
select count(proyecto.id)+1 into @cod from proyecto where idempresa=iempresa; /*iempresa*/

set verifica = (Select razon_social from empresa, proyecto where empresa.id=proyecto.idempresa and proyecto.idempresa=iempresa LIMIT 1);

if verifica is null 
THEN Select concat(substring(RAZON_SOCIAL,1,5),'/',lpad(1,3,'0'),'-',YEAR(CURRENT_DATE)) CodigoProyecto from empresa where empresa.id=iempresa;
ELSE Select concat(substring(RAZON_SOCIAL,1,5),'/',lpad(@cod,3,'0'),'-',proyecto.year) CodigoProyecto
from empresa, proyecto
where empresa.id=proyecto.idempresa
and proyecto.idempresa=iempresa LIMIT 1;
END IF; 

END$$

CREATE DEFINER=`jose`@`localhost` PROCEDURE `sp_Codigo_Proyecto_Interno_Automatico` (IN `iempresa` INT(10), IN `itagente` INT(10))  NO SQL
BEGIN

Select concat(substring(RAZON_SOCIAL,1,5),'/',lpad(1,3,'0'), '-',proyecto.year,'/',substring(nombre_instalacion,1,9),'-', '-',substring(ms_tagente.nombre,1,3)) CodigoProyecto from empresa, proyecto, agente, ms_tagente where empresa.id=proyecto.idempresa and agente.idproyecto = proyecto.id and ms_tagente.id = agente.idtagente and proyecto.idempresa=iempresa and agente.idtagente=itagente;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Convertir_fecha` (IN `llave` INT)  BEGIN
SELECT id,mes idmes, year, 
case 
WHEN mes='01' THEN 'Enero'
WHEN mes='02' THEN 'Febrero'
WHEN mes='03' THEN 'Marzo' 
WHEN mes='04' THEN 'Abril' 
WHEN mes='05' THEN 'Mayo'
WHEN mes='06' THEN 'Junio'
WHEN mes='07' THEN 'Julio'
WHEN mes='08' THEN 'Agosto'
WHEN mes='09' THEN 'Septiembre'
WHEN mes='10' THEN 'Octubre'
WHEN mes='11' THEN 'Noviembre'
WHEN mes='12' THEN 'Diciembre'
END mes, cant_trab_emp_proy numero, domicilio_fiscal from hisso.hproyectoempresa where idempresa=llave;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Generar_Codigo_epp` (IN `p_idproyecto` INT)  BEGIN
    DECLARE contador INT; 
    BEGIN
        SET contador= (SELECT COUNT(*)+1 FROM epp WHERE idagente = p_idproyecto); 
        IF(contador<10)THEN
            SET @p_codigo_secundario= CONCAT('Registro de entrega de equipos 00',contador);
            ELSE IF(contador<100) THEN
                SET @p_codigo_secundario= CONCAT('Registro de entrega de equipos 00',contador);
                ELSE IF(contador<1000)THEN
                    SET @p_codigo_secundario= CONCAT('Registro de entrega de equipos 00',contador);
                END IF;
            END IF;
        END IF; 
        select @p_codigo_secundario as codigo;
    END;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Generar_Codigo_epp_equipos` (IN `p_idproyecto` INT)  BEGIN
    DECLARE contador INT; 
    BEGIN
        SET contador= (SELECT COUNT(*)+1 FROM epp_equipos WHERE idagente = p_idproyecto); 
        IF(contador<10)THEN
            SET @p_codigo_secundario= CONCAT('EQP00',contador);
            ELSE IF(contador<100) THEN
                SET @p_codigo_secundario= CONCAT('EQP0',contador);
                ELSE IF(contador<1000)THEN
                    SET @p_codigo_secundario= CONCAT('EQP',contador);
                END IF;
            END IF;
        END IF; 
        select @p_codigo_secundario as codigo;
    END;
END$$

CREATE DEFINER=`jose`@`localhost` PROCEDURE `sp_Generar_Cod_Interno_Responsable` ()  BEGIN
select CONCAT('R', lpad(max(persona.id)+1,5,'0')) CODIGO_INTERNO from persona;
END$$

CREATE DEFINER=`jose`@`localhost` PROCEDURE `sp_Usuario_getListarRolPermisosByUsuario` (IN `nidusuario` INT)  NO SQL
BEGIN
SELECT permisos.id,permisos.nombre FROM permisos 
INNER JOIN permisos_usuario ON permisos.id = permisos_usuario.permisos_id
AND permisos_usuario.usuario_id = nidusuario
UNION
SELECT p.id,p.nombre FROM usuario_has_rol urol
INNER JOIN rol_has_permisos rp ON urol.rol_id = rp.rol_id
INNER JOIN permisos p ON rp.permisos_id = p.id
WHERE urol.usuario_id = nidusuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verificarcalibracion` ()  NO SQL
UPDATE equipos as e
INNER JOIN equipos_detalle as ed
ON
e.id = ed.idequipo
SET
ed.motivo = 'falta calibrar' , e.estado=1
WHERE
 date(e.fecha_termino) >= curdate()$$

CREATE DEFINER=`jose`@`localhost` PROCEDURE `verificar_calibracion_equipo` ()  BEGIN
	DECLARE var_id_equipo int;
	DECLARE calibracion_equipo CURSOR FOR
    SELECT id FROM equipos WHERE fecha_termino = curdate();
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET @hecho = TRUE;
    OPEN calibracion_equipo;
    loop1: LOOP
    FETCH calibracion_equipo INTO var_id_equipo;
    IF @hecho THEN
        update equipos set estado=1 where id = var_id_equipo;
    insert into equipos_detalle(idequipo, idpersona, motivo,created_at) values (var_id_equipo,null,'Falta Calibracion',curdate());
 	LEAVE loop1;
 	END IF;
    END LOOP loop1;
    CLOSE calibracion_equipo;
END$$

--
-- Funciones
--
CREATE DEFINER=`jose`@`localhost` FUNCTION `practicando1` () RETURNS INT(11) BEGIN
	DECLARE var_id_equipo int;
	DECLARE calibracion_equipo CURSOR FOR
    SELECT id FROM equipos WHERE fecha_termino = curdate();
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET @hecho = TRUE;
    OPEN calibracion_equipo;
    loop1: LOOP
    FETCH calibracion_equipo INTO var_id_equipo;
    IF @hecho THEN
        return var_id_equipo;
    
 	LEAVE loop1;
 	END IF;
    END LOOP loop1;
    CLOSE calibracion_equipo;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agente`
--

CREATE TABLE `agente` (
  `id` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `idtservicio` int(11) NOT NULL,
  `idtmonitoreo` int(11) DEFAULT NULL,
  `idtagente` int(11) DEFAULT NULL,
  `iddistrito` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `nombre_instalacion` text NOT NULL,
  `direccion` text NOT NULL,
  `cant_puntos` text,
  `usercreated` bigint(5) NOT NULL,
  `userupdated` bigint(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agente`
--

INSERT INTO `agente` (`id`, `idproyecto`, `idtservicio`, `idtmonitoreo`, `idtagente`, `iddistrito`, `codigo`, `nombre_instalacion`, `direccion`, `cant_puntos`, `usercreated`, `userupdated`, `created_at`, `updated_at`) VALUES
(48, 15, 4, 8, 16, 260, 'MGC I/001-2021/01', 'dddd', 'ddd', NULL, 1, NULL, '2021-07-16 20:55:31', '2021-07-16 20:55:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion_proyecto`
--

CREATE TABLE `asignacion_proyecto` (
  `id` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `responsable` text NOT NULL,
  `equipos` text NOT NULL,
  `estado` bigint(5) NOT NULL,
  `usercreated` bigint(5) NOT NULL,
  `userupdated` bigint(5) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacion_proyecto`
--

INSERT INTO `asignacion_proyecto` (`id`, `idproyecto`, `responsable`, `equipos`, `estado`, `usercreated`, `userupdated`, `created_at`, `updated_at`) VALUES
(7, 15, 'EDUARDO ANTONIO,ALE ALARCON', 'OTROS,OTROS', 0, 1, NULL, '2021-07-16 20:56:58', '2021-07-16 20:56:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion_responsable_equipo`
--

CREATE TABLE `asignacion_responsable_equipo` (
  `id` int(11) NOT NULL,
  `idasignacion_proyecto` int(11) NOT NULL,
  `idagente` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `descripcion` text,
  `usercreated` bigint(5) NOT NULL,
  `userupdated` bigint(5) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacion_responsable_equipo`
--

INSERT INTO `asignacion_responsable_equipo` (`id`, `idasignacion_proyecto`, `idagente`, `idpersona`, `idequipo`, `fecha_inicio`, `fecha_fin`, `descripcion`, `usercreated`, `userupdated`, `created_at`, `updated_at`) VALUES
(18, 6, 41, 2, 25, '2021-07-17', '2021-07-19', 'vfvfvf', 1, NULL, '2021-07-11 22:20:12', '2021-07-11 22:20:12'),
(19, 6, 41, 2, 2, '2021-07-21', '2021-07-22', NULL, 1, NULL, '2021-07-11 22:20:12', '2021-07-11 22:20:12'),
(20, 6, 43, 3, 11, '2021-07-23', '2021-07-29', NULL, 1, NULL, '2021-07-11 22:30:43', '2021-07-11 22:30:43'),
(21, 7, 48, 3, 25, '2021-07-01', '2021-07-31', 'cwdcwd', 1, NULL, '2021-07-16 20:56:58', '2021-07-16 20:56:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendario`
--

CREATE TABLE `calendario` (
  `idcalendario` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `estado` bigint(5) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `nombre`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Analista de sistemas', 0, NULL, NULL),
(2, 'Coordinador de Operaciones y Comercial', 1, NULL, NULL),
(3, 'Supervisor Junior SST', 0, NULL, NULL),
(4, 'Asistente Comercial', 1, NULL, NULL),
(9, 'Gerente General', 1, NULL, NULL),
(10, 'Supervisor de SST', 0, NULL, NULL),
(11, 'Asistente administrativo', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_peligro`
--

CREATE TABLE `categoria_peligro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria_peligro`
--

INSERT INTO `categoria_peligro` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Biológicos', ''),
(2, 'Físicos', ''),
(3, 'Químicos', ''),
(4, 'Eléctricos', ''),
(5, 'Psicosociales', ''),
(6, 'Ergonómicos', ''),
(7, 'Mecánicos', ''),
(8, 'Locativos', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `covid`
--

CREATE TABLE `covid` (
  `id` int(11) NOT NULL,
  `idproyecto` int(10) NOT NULL,
  `idusuario` int(10) NOT NULL,
  `nombre` text,
  `dni` text,
  `correo` text,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuestionario`
--

CREATE TABLE `cuestionario` (
  `id` int(11) NOT NULL,
  `pregunta` int(11) NOT NULL,
  `subpregunta` int(11) NOT NULL,
  `tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `id` int(5) NOT NULL DEFAULT '0',
  `departamento` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id`, `departamento`) VALUES
(1, 'AMAZONAS'),
(2, 'ANCASH'),
(3, 'APURIMAC'),
(4, 'AREQUIPA'),
(5, 'AYACUCHO'),
(6, 'CAJAMARCA'),
(7, 'CUSCO'),
(8, 'HUANCAVELICA'),
(9, 'HUANUCO'),
(10, 'ICA'),
(11, 'JUNIN'),
(12, 'LA LIBERTAD'),
(13, 'LAMBAYEQUE'),
(14, 'LIMA'),
(15, 'LORETO'),
(16, 'MADRE DE DIOS'),
(17, 'MOQUEGUA'),
(18, 'PASCO'),
(19, 'PIURA'),
(20, 'PUNO'),
(21, 'SAN MARTIN'),
(22, 'TACNA'),
(23, 'TUMBES'),
(24, 'UCAYALI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_ergonomia`
--

CREATE TABLE `detalle_ergonomia` (
  `id` int(11) NOT NULL,
  `activdad_campo` text NOT NULL,
  `hora_campo` decimal(15,2) NOT NULL,
  `activdad_oficina` text NOT NULL,
  `hora_oficina` decimal(15,2) NOT NULL,
  `iddet_ergonomia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_bio_hisopo`
--

CREATE TABLE `det_bio_hisopo` (
  `id` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `idinstalacion` int(11) NOT NULL,
  `punto_monitoreo` varchar(45) NOT NULL,
  `fecha_monitoreo` date NOT NULL,
  `hora_monitoreo` varchar(45) NOT NULL,
  `codigo_muestra` varchar(45) DEFAULT NULL,
  `descripcion_lugar` text,
  `muestreo` varchar(45) DEFAULT NULL,
  `foto1` text,
  `foto2` text,
  `observacion` text,
  `estado` bigint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_bio_tipo`
--

CREATE TABLE `det_bio_tipo` (
  `id` int(11) NOT NULL,
  `idmonitoreo` int(11) NOT NULL,
  `idpersona1` int(11) NOT NULL,
  `idpersona2` int(11) NOT NULL,
  `idpersona3` int(11) NOT NULL,
  `idcargo1` int(11) NOT NULL,
  `idcargo2` int(11) NOT NULL,
  `idcargo3` int(11) NOT NULL,
  `punto_monitoreo` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `fecha` date NOT NULL,
  `codigo_muestra` varchar(45) NOT NULL,
  `descripcion` text NOT NULL,
  `foto1` text NOT NULL,
  `foto2` text NOT NULL,
  `observacion` text NOT NULL,
  `tipo` bigint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_cuest_opcion`
--

CREATE TABLE `det_cuest_opcion` (
  `id` int(11) NOT NULL,
  `idcuestionario` int(11) NOT NULL,
  `idopcion` int(11) NOT NULL,
  `puntaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_ergonomia`
--

CREATE TABLE `det_ergonomia` (
  `id` int(11) NOT NULL,
  `idtrabajador` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `punto_monitoreo` varchar(45) NOT NULL,
  `fecha_monitoreo` date NOT NULL,
  `hora_monitoreo` varchar(45) NOT NULL,
  `trabajadores_expuestos` varchar(45) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `edad` varchar(45) NOT NULL,
  `puesto` varchar(45) NOT NULL,
  `hoario_trabajo` varchar(45) NOT NULL,
  `turno_rotativo` varchar(45) NOT NULL,
  `peso_talla` varchar(45) NOT NULL,
  `porcentaje_hora_campo` varchar(45) DEFAULT NULL,
  `trabajo_campo_1` text,
  `trabajo_campo_2` text,
  `trabajo_campo_3` text,
  `trabajo_campo_4` text,
  `trabajo_campo_5` text,
  `porcentaje_hora_oficina` varchar(45) DEFAULT NULL,
  `trabajo_oficina_1` text,
  `trabajo_oficina_2` text,
  `trabajo_oficina_3` text,
  `trabajo_oficina_4` text,
  `trabajo_oficina_5` text,
  `dolencia_fisica` text,
  `discapacidad` text,
  `gestacion` text,
  `altura_asiento` text,
  `altura_espaldar` text,
  `alcolchonado_recubierto` text,
  `reposabrazos` text,
  `largo` text,
  `ancho` text,
  `superficie` text,
  `colocacion_posicion` text,
  `espacio_piernas` text,
  `espacio_silla` text,
  `altura_pantalla_laptop` text,
  `altura_pantalla_pc` text,
  `distancia_cabeza` text,
  `foto1` text,
  `foto2` text,
  `foto3` text,
  `foto4` text,
  `foto5` text,
  `estado` bigint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_iluminacion`
--

CREATE TABLE `det_iluminacion` (
  `id` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `idtrabajador` int(11) NOT NULL,
  `punto_monitoreo` varchar(45) NOT NULL,
  `fecha_monitoreo` date NOT NULL,
  `hora_monitoreo` varchar(45) NOT NULL,
  `max1` varchar(45) NOT NULL,
  `max2` varchar(45) NOT NULL,
  `max3` varchar(45) NOT NULL,
  `min1` varchar(45) NOT NULL,
  `min2` varchar(45) NOT NULL,
  `min3` varchar(45) NOT NULL,
  `avg1` varchar(45) NOT NULL,
  `avg2` varchar(45) NOT NULL,
  `avg3` varchar(45) NOT NULL,
  `tipo_iluminaria` varchar(45) NOT NULL,
  `altura_iluminaria` varchar(45) NOT NULL,
  `trabajo_realizado` text NOT NULL,
  `caracteristicas_entorno` text NOT NULL,
  `numero_trabajadores` int(11) NOT NULL,
  `foto1` text,
  `foto2` text,
  `foto3` text,
  `foto4` text,
  `foto5` text,
  `foto6` text,
  `foto7` text,
  `foto8` text,
  `foto9` text,
  `foto10` text,
  `estado` bigint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_psicosocial`
--

CREATE TABLE `det_psicosocial` (
  `id` int(11) NOT NULL,
  `idmonitoreo` int(11) NOT NULL,
  `idpersona1` int(11) NOT NULL,
  `idcargo1` int(11) NOT NULL,
  `idpersona2` int(11) NOT NULL,
  `idcargo2` int(11) NOT NULL,
  `idcuest_opcion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_ruido`
--

CREATE TABLE `det_ruido` (
  `id` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `idinstalacion` int(11) NOT NULL,
  `punto_monitoreo` varchar(45) NOT NULL,
  `fecha_monitoreo` date NOT NULL,
  `hora_monitoreo` varchar(45) NOT NULL,
  `fuente_ruido` varchar(45) NOT NULL,
  `foto` text NOT NULL,
  `numero_trabajadores` varchar(45) NOT NULL,
  `min1` decimal(15,2) NOT NULL,
  `min2` decimal(15,2) NOT NULL,
  `min3` decimal(15,2) NOT NULL,
  `max1` decimal(15,2) NOT NULL,
  `max2` decimal(15,2) NOT NULL,
  `max3` decimal(15,2) NOT NULL,
  `lequiv1` decimal(15,2) NOT NULL,
  `lequiv2` decimal(15,2) NOT NULL,
  `lequiv3` decimal(15,2) NOT NULL,
  `actividades` text NOT NULL,
  `estado` bigint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_temperatura`
--

CREATE TABLE `det_temperatura` (
  `id` int(11) NOT NULL,
  `idmonitoreo` int(11) NOT NULL,
  `idpersona1` int(11) NOT NULL,
  `idpersona2` int(11) NOT NULL,
  `idpersona3` int(11) NOT NULL,
  `idcargo1` int(11) NOT NULL,
  `idcargo2` int(11) NOT NULL,
  `idcargo3` int(11) NOT NULL,
  `punto_monitoreo` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `area` varchar(45) NOT NULL,
  `seccion` varchar(45) NOT NULL,
  `piso` varchar(45) NOT NULL,
  `tem_min` decimal(15,2) NOT NULL,
  `tem_max` decimal(15,2) NOT NULL,
  `tem_pro` decimal(15,2) NOT NULL,
  `hum_min` decimal(15,2) NOT NULL,
  `hum_max` decimal(15,2) NOT NULL,
  `hum_pro` decimal(15,2) NOT NULL,
  `vel_min` decimal(15,2) NOT NULL,
  `vel_max` decimal(15,2) NOT NULL,
  `vel_pro` decimal(15,2) NOT NULL,
  `equipos` varchar(45) NOT NULL,
  `trabajos` varchar(45) NOT NULL,
  `foto1` text NOT NULL COMMENT 'equipo de ventilacion',
  `foto2` text NOT NULL COMMENT 'area general',
  `foto3` text NOT NULL COMMENT 'area general'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distrito`
--

CREATE TABLE `distrito` (
  `id` int(11) NOT NULL,
  `distrito` varchar(50) DEFAULT NULL,
  `idProv` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `distrito`
--

INSERT INTO `distrito` (`id`, `distrito`, `idProv`) VALUES
(1, 'ARAMANGO', 1),
(2, 'COPALLIN', 1),
(3, 'EL PARCO', 1),
(4, 'IMAZA', 1),
(5, 'LA PECA', 1),
(6, 'CHISQUILLA', 2),
(7, 'CHURUJA', 2),
(8, 'COROSHA', 2),
(9, 'CUISPES', 2),
(10, 'FLORIDA', 2),
(11, 'JAZAN', 2),
(12, 'JUMBILLA', 2),
(13, 'RECTA', 2),
(14, 'SAN CARLOS', 2),
(15, 'SHIPASBAMBA', 2),
(16, 'VALERA', 2),
(17, 'YAMBRASBAMBA', 2),
(18, 'ASUNCION', 3),
(19, 'BALSAS', 3),
(20, 'CHACHAPOYAS', 3),
(21, 'CHETO', 3),
(22, 'CHILIQUIN', 3),
(23, 'CHUQUIBAMBA', 3),
(24, 'GRANADA', 3),
(25, 'HUANCAS', 3),
(26, 'LA JALCA', 3),
(27, 'LEIMEBAMBA', 3),
(28, 'LEVANTO', 3),
(29, 'MAGDALENA', 3),
(30, 'MARISCAL CASTILLA', 3),
(31, 'MOLINOPAMPA', 3),
(32, 'MONTEVIDEO', 3),
(33, 'OLLEROS', 3),
(34, 'QUINJALCA', 3),
(35, 'SAN FRANCISCO DE DAGUAS', 3),
(36, 'SAN ISIDRO DE MAINO', 3),
(37, 'SOLOCO', 3),
(38, 'SONCHE', 3),
(39, 'EL CENEPA', 4),
(40, 'NIEVA', 4),
(41, 'RIO SANTIAGO', 4),
(42, 'CAMPORREDONDO', 5),
(43, 'COCABAMBA', 5),
(44, 'COLCAMAR', 5),
(45, 'CONILA', 5),
(46, 'INGUILPATA', 5),
(47, 'LAMUD', 5),
(48, 'LONGUITA', 5),
(49, 'LONYA CHICO', 5),
(50, 'LUYA', 5),
(51, 'LUYA VIEJO', 5),
(52, 'MARIA', 5),
(53, 'OCALLI', 5),
(54, 'OCUMAL', 5),
(55, 'PISUQUIA', 5),
(56, 'PROVIDENCIA', 5),
(57, 'SAN CRISTOBAL', 5),
(58, 'SAN FRANCISCO DEL YESO', 5),
(59, 'SAN JERONIMO', 5),
(60, 'SAN JUAN DE LOPECANCHA', 5),
(61, 'SANTA CATALINA', 5),
(62, 'SANTO TOMAS', 5),
(63, 'TINGO', 5),
(64, 'TRITA', 5),
(65, 'CHIRIMOTO', 6),
(66, 'COCHAMAL', 6),
(67, 'HUAMBO', 6),
(68, 'LIMABAMBA', 6),
(69, 'LONGAR', 6),
(70, 'MARISCAL BENAVIDES', 6),
(71, 'MILPUC', 6),
(72, 'OMIA', 6),
(73, 'SAN NICOLAS', 6),
(74, 'SANTA ROSA', 6),
(75, 'TOTORA', 6),
(76, 'VISTA ALEGRE', 6),
(77, 'BAGUA GRANDE', 7),
(78, 'CAJARURO', 7),
(79, 'CUMBA', 7),
(80, 'EL MILAGRO', 7),
(81, 'JAMALCA', 7),
(82, 'LONYA GRANDE', 7),
(83, 'YAMON', 7),
(84, 'AIJA', 8),
(85, 'CORIS', 8),
(86, 'HUACLLAN', 8),
(87, 'LA MERCED', 8),
(88, 'SUCCHA', 8),
(89, 'ACZO', 9),
(90, 'CHACCHO', 9),
(91, 'CHINGAS', 9),
(92, 'LLAMELLIN', 9),
(93, 'MIRGAS', 9),
(94, 'SAN JUAN DE RONTOY', 9),
(95, 'ACOCHACA', 10),
(96, 'CHACAS', 10),
(97, 'ABELARDO PARDO LEZAMETA', 11),
(98, 'ANTONIO RAYMONDI', 11),
(99, 'AQUIA', 11),
(100, 'CAJACAY', 11),
(101, 'CANIS', 11),
(102, 'CHIQUIAN', 11),
(103, 'COLQUIOC', 11),
(104, 'HUALLANCA', 11),
(105, 'HUASTA', 11),
(106, 'HUAYLLACAYAN', 11),
(107, 'LA PRIMAVERA', 11),
(108, 'MANGAS', 11),
(109, 'PACLLON', 11),
(110, 'SAN MIGUEL DE CORPANQUI', 11),
(111, 'TICLLOS', 11),
(112, 'ACOPAMPA', 12),
(113, 'AMASHCA', 12),
(114, 'ANTA', 12),
(115, 'ATAQUERO', 12),
(116, 'CARHUAZ', 12),
(117, 'MARCARA', 12),
(118, 'PARIAHUANCA', 12),
(119, 'SAN MIGUEL DE ACO', 12),
(120, 'SHILLA', 12),
(121, 'TINCO', 12),
(122, 'YUNGAR', 12),
(123, 'SAN LUIS', 13),
(124, 'SAN NICOLAS', 13),
(125, 'YAUYA', 13),
(126, 'BUENA VISTA ALTA', 14),
(127, 'CASMA', 14),
(128, 'COMANDANTE NOEL', 14),
(129, 'YAUTAN', 14),
(130, 'ACO', 15),
(131, 'BAMBAS', 15),
(132, 'CORONGO', 15),
(133, 'CUSCA', 15),
(134, 'LA PAMPA', 15),
(135, 'YANAC', 15),
(136, 'YUPAN', 15),
(137, 'COCHABAMBA', 16),
(138, 'COLCABAMBA', 16),
(139, 'HUANCHAY', 16),
(140, 'HUARAZ', 16),
(141, 'INDEPENDENCIA', 16),
(142, 'JANGAS', 16),
(143, 'LA LIBERTAD', 16),
(144, 'OLLEROS', 16),
(145, 'PAMPAS', 16),
(146, 'PARIACOTO', 16),
(147, 'PIRA', 16),
(148, 'TARICA', 16),
(149, 'ANRA', 17),
(150, 'CAJAY', 17),
(151, 'CHAVIN DE HUANTAR', 17),
(152, 'HUACACHI', 17),
(153, 'HUACCHIS', 17),
(154, 'HUACHIS', 17),
(155, 'HUANTAR', 17),
(156, 'HUARI', 17),
(157, 'MASIN', 17),
(158, 'PAUCAS', 17),
(159, 'PONTO', 17),
(160, 'RAHUAPAMPA', 17),
(161, 'RAPAYAN', 17),
(162, 'SAN MARCOS', 17),
(163, 'SAN PEDRO DE CHANA', 17),
(164, 'UCO', 17),
(165, 'COCHAPETI', 18),
(166, 'CULEBRAS', 18),
(167, 'HUARMEY', 18),
(168, 'HUAYAN', 18),
(169, 'MALVAS', 18),
(170, 'CARAZ', 19),
(171, 'HUALLANCA', 19),
(172, 'HUATA', 19),
(173, 'HUAYLAS', 19),
(174, 'MATO', 19),
(175, 'PAMPAROMAS', 19),
(176, 'PUEBLO LIBRE', 19),
(177, 'SANTA CRUZ', 19),
(178, 'SANTO TORIBIO', 19),
(179, 'YURACMARCA', 19),
(180, 'CASCA', 20),
(181, 'ELEAZAR GUZMAN BARRON', 20),
(182, 'FIDEL OLIVAS ESCUDERO', 20),
(183, 'LLAMA', 20),
(184, 'LLUMPA', 20),
(185, 'LUCMA', 20),
(186, 'MUSGA', 20),
(187, 'PISCOBAMBA', 20),
(188, 'ACAS', 21),
(189, 'CAJAMARQUILLA', 21),
(190, 'CARHUAPAMPA', 21),
(191, 'COCHAS', 21),
(192, 'CONGAS', 21),
(193, 'LLIPA', 21),
(194, 'OCROS', 21),
(195, 'SAN CRISTOBAL DE RAJAN', 21),
(196, 'SAN PEDRO', 21),
(197, 'SANTIAGO DE CHILCAS', 21),
(198, 'BOLOGNESI', 22),
(199, 'CABANA', 22),
(200, 'CONCHUCOS', 22),
(201, 'HUACASCHUQUE', 22),
(202, 'HUANDOVAL', 22),
(203, 'LACABAMBA', 22),
(204, 'LLAPO', 22),
(205, 'PALLASCA', 22),
(206, 'PAMPAS', 22),
(207, 'SANTA ROSA', 22),
(208, 'TAUCA', 22),
(209, 'HUAYLLAN', 23),
(210, 'PAROBAMBA', 23),
(211, 'POMABAMBA', 23),
(212, 'QUINUABAMBA', 23),
(213, 'CATAC', 24),
(214, 'COTAPARACO', 24),
(215, 'HUAYLLAPAMPA', 24),
(216, 'LLACLLIN', 24),
(217, 'MARCA', 24),
(218, 'PAMPAS CHICO', 24),
(219, 'PARARIN', 24),
(220, 'RECUAY', 24),
(221, 'TAPACOCHA', 24),
(222, 'TICAPAMPA', 24),
(223, 'CACERES DEL PERU', 25),
(224, 'CHIMBOTE', 25),
(225, 'COISHCO', 25),
(226, 'MACATE', 25),
(227, 'MORO', 25),
(228, 'NEPEÑA', 25),
(229, 'NUEVO CHIMBOTE', 25),
(230, 'SAMANCO', 25),
(231, 'SANTA', 25),
(232, 'ACOBAMBA', 26),
(233, 'ALFONSO UGARTE', 26),
(234, 'CASHAPAMPA', 26),
(235, 'CHINGALPO', 26),
(236, 'HUAYLLABAMBA', 26),
(237, 'QUICHES', 26),
(238, 'RAGASH', 26),
(239, 'SAN JUAN', 26),
(240, 'SICSIBAMBA', 26),
(241, 'SIHUAS', 26),
(242, 'CASCAPARA', 27),
(243, 'MANCOS', 27),
(244, 'MATACOTO', 27),
(245, 'QUILLO', 27),
(246, 'RANRAHIRCA', 27),
(247, 'SHUPLUY', 27),
(248, 'YANAMA', 27),
(249, 'YUNGAY', 27),
(250, 'ABANCAY', 28),
(251, 'CHACOCHE', 28),
(252, 'CIRCA', 28),
(253, 'CURAHUASI', 28),
(254, 'HUANIPACA', 28),
(255, 'LAMBRAMA', 28),
(256, 'PICHIRHUA', 28),
(257, 'SAN PEDRO DE CACHORA', 28),
(258, 'TAMBURCO', 28),
(259, 'ANDAHUAYLAS', 29),
(260, 'ANDARAPA', 29),
(261, 'CHIARA', 29),
(262, 'HUANCARAMA', 29),
(263, 'HUANCARAY', 29),
(264, 'HUAYANA', 29),
(265, 'KAQUIABAMBA', 29),
(266, 'KISHUARA', 29),
(267, 'PACOBAMBA', 29),
(268, 'PACUCHA', 29),
(269, 'PAMPACHIRI', 29),
(270, 'POMACOCHA', 29),
(271, 'SAN ANTONIO DE CACHI', 29),
(272, 'SAN JERONIMO', 29),
(273, 'SAN MIGUEL DE CHACCRAMPA', 29),
(274, 'SANTA MARIA DE CHICMO', 29),
(275, 'TALAVERA', 29),
(276, 'TUMAY HUARACA', 29),
(277, 'TURPO', 29),
(278, 'ANTABAMBA', 30),
(279, 'EL ORO', 30),
(280, 'HUAQUIRCA', 30),
(281, 'JUAN ESPINOZA MEDRANO', 30),
(282, 'OROPESA', 30),
(283, 'PACHACONAS', 30),
(284, 'SABAINO', 30),
(285, 'CAPAYA', 31),
(286, 'CARAYBAMBA', 31),
(287, 'CHALHUANCA', 31),
(288, 'CHAPIMARCA', 31),
(289, 'COLCABAMBA', 31),
(290, 'COTARUSE', 31),
(291, 'HUAYLLO', 31),
(292, 'JUSTO APU SAHUARAURA', 31),
(293, 'LUCRE', 31),
(294, 'POCOHUANCA', 31),
(295, 'SAN JUAN DE CHACÑA', 31),
(296, 'SAÑAYCA', 31),
(297, 'SORAYA', 31),
(298, 'TAPAIRIHUA', 31),
(299, 'TINTAY', 31),
(300, 'TORAYA', 31),
(301, 'YANACA', 31),
(302, 'ANCO-HUALLO', 32),
(303, 'CHINCHEROS', 32),
(304, 'COCHARCAS', 32),
(305, 'HUACCANA', 32),
(306, 'OCOBAMBA', 32),
(307, 'ONGOY', 32),
(308, 'RANRACANCHA', 32),
(309, 'URANMARCA', 32),
(310, 'CHALLHUAHUACHO', 33),
(311, 'COTABAMBAS', 33),
(312, 'COYLLURQUI', 33),
(313, 'HAQUIRA', 33),
(314, 'MARA', 33),
(315, 'TAMBOBAMBA', 33),
(316, 'CHUQUIBAMBILLA', 34),
(317, 'CURASCO', 34),
(318, 'CURPAHUASI', 34),
(319, 'GAMARRA', 34),
(320, 'HUAYLLATI', 34),
(321, 'MAMARA', 34),
(322, 'MICAELA BASTIDAS', 34),
(323, 'PATAYPAMPA', 34),
(324, 'PROGRESO', 34),
(325, 'SAN ANTONIO', 34),
(326, 'SANTA ROSA', 34),
(327, 'TURPAY', 34),
(328, 'VILCABAMBA', 34),
(329, 'VIRUNDO', 34),
(330, 'ALTO SELVA ALEGRE', 35),
(331, 'AREQUIPA', 35),
(332, 'CAYMA', 35),
(333, 'CERRO COLORADO', 35),
(334, 'CHARACATO', 35),
(335, 'CHIGUATA', 35),
(336, 'JACOBO HUNTER', 35),
(337, 'JOSE LUIS BUSTAMANTE Y RIVERO', 35),
(338, 'LA JOYA', 35),
(339, 'MARIANO MELGAR', 35),
(340, 'MIRAFLORES', 35),
(341, 'MOLLEBAYA', 35),
(342, 'PAUCARPATA', 35),
(343, 'POCSI', 35),
(344, 'POLOBAYA', 35),
(345, 'QUEQUEÑA', 35),
(346, 'SABANDIA', 35),
(347, 'SACHACA', 35),
(348, 'SAN JUAN DE SIGUAS', 35),
(349, 'SAN JUAN DE TARUCANI', 35),
(350, 'SANTA ISABEL DE SIGUAS', 35),
(351, 'SANTA RITA DE SIGUAS', 35),
(352, 'SOCABAYA', 35),
(353, 'TIABAYA', 35),
(354, 'UCHUMAYO', 35),
(355, 'VITOR  1/', 35),
(356, 'YANAHUARA', 35),
(357, 'YARABAMBA', 35),
(358, 'YURA', 35),
(359, 'CAMANA', 36),
(360, 'JOSE MARIA QUIMPER', 36),
(361, 'MARIANO NICOLAS VALCARCEL', 36),
(362, 'MARISCAL CACERES', 36),
(363, 'NICOLAS DE PIEROLA', 36),
(364, 'OCOÑA', 36),
(365, 'QUILCA', 36),
(366, 'SAMUEL PASTOR', 36),
(367, 'ACARI', 37),
(368, 'ATICO', 37),
(369, 'ATIQUIPA', 37),
(370, 'BELLA UNION', 37),
(371, 'CAHUACHO', 37),
(372, 'CARAVELI', 37),
(373, 'CHALA', 37),
(374, 'CHAPARRA', 37),
(375, 'HUANUHUANU', 37),
(376, 'JAQUI', 37),
(377, 'LOMAS', 37),
(378, 'QUICACHA', 37),
(379, 'YAUCA', 37),
(380, 'ANDAGUA', 38),
(381, 'APLAO', 38),
(382, 'AYO', 38),
(383, 'CHACHAS', 38),
(384, 'CHILCAYMARCA', 38),
(385, 'CHOCO', 38),
(386, 'HUANCARQUI', 38),
(387, 'MACHAGUAY', 38),
(388, 'ORCOPAMPA', 38),
(389, 'PAMPACOLCA', 38),
(390, 'TIPAN', 38),
(391, 'UÑON', 38),
(392, 'URACA', 38),
(393, 'VIRACO', 38),
(394, 'ACHOMA', 39),
(395, 'CABANACONDE', 39),
(396, 'CALLALLI', 39),
(397, 'CAYLLOMA', 39),
(398, 'CHIVAY', 39),
(399, 'COPORAQUE', 39),
(400, 'HUAMBO', 39),
(401, 'HUANCA', 39),
(402, 'ICHUPAMPA', 39),
(403, 'LARI', 39),
(404, 'LLUTA', 39),
(405, 'MACA', 39),
(406, 'MADRIGAL', 39),
(407, 'MAJES', 39),
(408, 'SAN ANTONIO DE CHUCA', 39),
(409, 'SIBAYO', 39),
(410, 'TAPAY', 39),
(411, 'TISCO', 39),
(412, 'TUTI', 39),
(413, 'YANQUE', 39),
(414, 'ANDARAY', 40),
(415, 'CAYARANI', 40),
(416, 'CHICHAS', 40),
(417, 'CHUQUIBAMBA', 40),
(418, 'IRAY', 40),
(419, 'RIO GRANDE', 40),
(420, 'SALAMANCA', 40),
(421, 'YANAQUIHUA', 40),
(422, 'COCACHACRA', 41),
(423, 'DEAN VALDIVIA', 41),
(424, 'ISLAY', 41),
(425, 'MEJIA', 41),
(426, 'MOLLENDO', 41),
(427, 'PUNTA DE BOMBON', 41),
(428, 'ALCA', 42),
(429, 'CHARCANA', 42),
(430, 'COTAHUASI', 42),
(431, 'HUAYNACOTAS', 42),
(432, 'PAMPAMARCA', 42),
(433, 'PUYCA', 42),
(434, 'QUECHUALLA', 42),
(435, 'SAYLA', 42),
(436, 'TAURIA', 42),
(437, 'TOMEPAMPA', 42),
(438, 'TORO', 42),
(439, 'CANGALLO', 43),
(440, 'CHUSCHI', 43),
(441, 'LOS MOROCHUCOS', 43),
(442, 'MARIA PARADO DE BELLIDO', 43),
(443, 'PARAS', 43),
(444, 'TOTOS', 43),
(445, 'ACOCRO', 44),
(446, 'ACOS VINCHOS', 44),
(447, 'AYACUCHO', 44),
(448, 'CARMEN ALTO', 44),
(449, 'CHIARA', 44),
(450, 'JESUS NAZARENO', 44),
(451, 'OCROS', 44),
(452, 'PACAYCASA', 44),
(453, 'QUINUA', 44),
(454, 'SAN JOSE DE TICLLAS', 44),
(455, 'SAN JUAN BAUTISTA', 44),
(456, 'SANTIAGO DE PISCHA', 44),
(457, 'SOCOS', 44),
(458, 'TAMBILLO', 44),
(459, 'VINCHOS', 44),
(460, 'CARAPO', 45),
(461, 'SACSAMARCA', 45),
(462, 'SANCOS', 45),
(463, 'SANTIAGO DE LUCANAMARCA', 45),
(464, 'AYAHUANCO', 46),
(465, 'HUAMANGUILLA', 46),
(466, 'HUANTA', 46),
(467, 'IGUAIN', 46),
(468, 'LLOCHEGUA', 46),
(469, 'LURICOCHA', 46),
(470, 'SANTILLANA', 46),
(471, 'SIVIA', 46),
(472, 'ANCO', 47),
(473, 'AYNA', 47),
(474, 'CHILCAS', 47),
(475, 'CHUNGUI', 47),
(476, 'LUIS CARRANZA', 47),
(477, 'SAN MIGUEL', 47),
(478, 'SANTA ROSA', 47),
(479, 'TAMBO', 47),
(480, 'AUCARA', 48),
(481, 'CABANA', 48),
(482, 'CARMEN SALCEDO', 48),
(483, 'CHAVIÑA', 48),
(484, 'CHIPAO', 48),
(485, 'HUAC-HUAS', 48),
(486, 'LARAMATE', 48),
(487, 'LEONCIO PRADO', 48),
(488, 'LLAUTA', 48),
(489, 'LUCANAS', 48),
(490, 'OCAÑA', 48),
(491, 'OTOCA', 48),
(492, 'PUQUIO', 48),
(493, 'SAISA', 48),
(494, 'SAN CRISTOBAL', 48),
(495, 'SAN JUAN', 48),
(496, 'SAN PEDRO', 48),
(497, 'SAN PEDRO DE PALCO', 48),
(498, 'SANCOS', 48),
(499, 'SANTA ANA DE HUAYCAHUACHO', 48),
(500, 'SANTA LUCIA', 48),
(501, 'CHUMPI', 49),
(502, 'CORACORA', 49),
(503, 'CORONEL CASTAÑEDA', 49),
(504, 'PACAPAUSA', 49),
(505, 'PULLO', 49),
(506, 'PUYUSCA', 49),
(507, 'SAN FRANCISCO DE RAVACAYCO', 49),
(508, 'UPAHUACHO', 49),
(509, 'COLTA', 50),
(510, 'CORCULLA', 50),
(511, 'LAMPA', 50),
(512, 'MARCABAMBA', 50),
(513, 'OYOLO', 50),
(514, 'PARARCA', 50),
(515, 'PAUSA', 50),
(516, 'SAN JAVIER DE ALPABAMBA', 50),
(517, 'SAN JOSE DE USHUA', 50),
(518, 'SARA SARA', 50),
(519, 'BELEN', 51),
(520, 'CHALCOS', 51),
(521, 'CHILCAYOC', 51),
(522, 'HUACAÑA', 51),
(523, 'MORCOLLA', 51),
(524, 'PAICO', 51),
(525, 'QUEROBAMBA', 51),
(526, 'SAN PEDRO DE LARCAY', 51),
(527, 'SAN SALVADOR DE QUIJE', 51),
(528, 'SANTIAGO DE PAUCARAY', 51),
(529, 'SORAS', 51),
(530, 'ALCAMENCA', 52),
(531, 'APONGO', 52),
(532, 'ASQUIPATA', 52),
(533, 'CANARIA', 52),
(534, 'CAYARA', 52),
(535, 'COLCA', 52),
(536, 'HUAMANQUIQUIA', 52),
(537, 'HUANCAPI', 52),
(538, 'HUANCARAYLLA', 52),
(539, 'HUAYA', 52),
(540, 'SARHUA', 52),
(541, 'VILCANCHOS', 52),
(542, 'ACCOMARCA', 53),
(543, 'CARHUANCA', 53),
(544, 'CONCEPCION', 53),
(545, 'HUAMBALPA', 53),
(546, 'INDEPENDENCIA', 53),
(547, 'SAURAMA', 53),
(548, 'VILCAS HUAMAN', 53),
(549, 'VISCHONGO', 53),
(550, 'CACHACHI', 54),
(551, 'CAJABAMBA', 54),
(552, 'CONDEBAMBA', 54),
(553, 'SITACOCHA', 54),
(554, 'ASUNCION', 55),
(555, 'CAJAMARCA', 55),
(556, 'CHETILLA', 55),
(557, 'COSPAN', 55),
(558, 'ENCAÑADA', 55),
(559, 'JESUS', 55),
(560, 'LLACANORA', 55),
(561, 'LOS BAÑOS DEL INCA', 55),
(562, 'MAGDALENA', 55),
(563, 'MATARA', 55),
(564, 'NAMORA', 55),
(565, 'SAN JUAN', 55),
(566, 'CELENDIN', 56),
(567, 'CHUMUCH', 56),
(568, 'CORTEGANA', 56),
(569, 'HUASMIN', 56),
(570, 'JORGE CHAVEZ', 56),
(571, 'JOSE GALVEZ', 56),
(572, 'LA LIBERTAD DE PALLAN', 56),
(573, 'MIGUEL IGLESIAS', 56),
(574, 'OXAMARCA', 56),
(575, 'SOROCHUCO', 56),
(576, 'SUCRE', 56),
(577, 'UTCO', 56),
(578, 'ANGUIA', 57),
(579, 'CHADIN', 57),
(580, 'CHALAMARCA', 57),
(581, 'CHIGUIRIP', 57),
(582, 'CHIMBAN', 57),
(583, 'CHOROPAMPA', 57),
(584, 'CHOTA', 57),
(585, 'COCHABAMBA', 57),
(586, 'CONCHAN', 57),
(587, 'HUAMBOS', 57),
(588, 'LAJAS', 57),
(589, 'LLAMA', 57),
(590, 'MIRACOSTA', 57),
(591, 'PACCHA', 57),
(592, 'PION', 57),
(593, 'QUEROCOTO', 57),
(594, 'SAN JUAN DE LICUPIS', 57),
(595, 'TACABAMBA', 57),
(596, 'TOCMOCHE', 57),
(597, 'CHILETE', 58),
(598, 'CONTUMAZA', 58),
(599, 'CUPISNIQUE', 58),
(600, 'GUZMANGO', 58),
(601, 'SAN BENITO', 58),
(602, 'SANTA CRUZ DE TOLEDO', 58),
(603, 'TANTARICA', 58),
(604, 'YONAN', 58),
(605, 'CALLAYUC', 59),
(606, 'CHOROS', 59),
(607, 'CUJILLO', 59),
(608, 'CUTERVO', 59),
(609, 'LA RAMADA', 59),
(610, 'PIMPINGOS', 59),
(611, 'QUEROCOTILLO', 59),
(612, 'SAN ANDRES DE CUTERVO', 59),
(613, 'SAN JUAN DE CUTERVO', 59),
(614, 'SAN LUIS DE LUCMA', 59),
(615, 'SANTA CRUZ', 59),
(616, 'SANTO TOMAS', 59),
(617, 'SOCOTA', 59),
(618, 'STO. DOMINGO DE LA CAPILLA', 59),
(619, 'TORIBIO CASANOVA', 59),
(620, 'BAMBAMARCA', 60),
(621, 'CHUGUR', 60),
(622, 'HUALGAYOC', 60),
(623, 'BELLAVISTA', 61),
(624, 'CHONTALI', 61),
(625, 'COLASAY', 61),
(626, 'HUABAL', 61),
(627, 'JAEN', 61),
(628, 'LAS PIRIAS', 61),
(629, 'POMAHUACA', 61),
(630, 'PUCARA', 61),
(631, 'SALLIQUE', 61),
(632, 'SAN FELIPE', 61),
(633, 'SAN JOSE DEL ALTO', 61),
(634, 'SANTA ROSA', 61),
(635, 'CHIRINOS', 62),
(636, 'HUARANGO', 62),
(637, 'LA COIPA', 62),
(638, 'NAMBALLE', 62),
(639, 'SAN IGNACIO', 62),
(640, 'SAN JOSE DE LOURDES', 62),
(641, 'TABACONAS', 62),
(642, 'CHANCAY', 63),
(643, 'EDUARDO VILLANUEVA', 63),
(644, 'GREGORIO PITA', 63),
(645, 'ICHOCAN', 63),
(646, 'JOSE MANUEL QUIROZ', 63),
(647, 'JOSE SABOGAL', 63),
(648, 'PEDRO GALVEZ', 63),
(649, 'BOLIVAR', 64),
(650, 'CALQUIS', 64),
(651, 'CATILLUC', 64),
(652, 'EL PRADO', 64),
(653, 'LA FLORIDA', 64),
(654, 'LLAPA', 64),
(655, 'NANCHOC', 64),
(656, 'NIEPOS', 64),
(657, 'SAN GREGORIO', 64),
(658, 'SAN MIGUEL', 64),
(659, 'SAN SILVESTRE DE COCHAN', 64),
(660, 'TONGOD', 64),
(661, 'UNION AGUA BLANCA', 64),
(662, 'SAN BERNARDINO', 65),
(663, 'SAN LUIS', 65),
(664, 'SAN PABLO', 65),
(665, 'TUMBADEN', 65),
(666, 'ANDABAMBA', 66),
(667, 'CATACHE', 66),
(668, 'CHANCAYBAÑOS', 66),
(669, 'LA ESPERANZA', 66),
(670, 'NINABAMBA', 66),
(671, 'PULAN', 66),
(672, 'SANTA CRUZ', 66),
(673, 'SAUCEPAMPA', 66),
(674, 'SEXI', 66),
(675, 'UTICYACU', 66),
(676, 'YAUYUCAN', 66),
(677, 'ACOMAYO', 67),
(678, 'ACOPIA', 67),
(679, 'ACOS', 67),
(680, 'MOSOC LLACTA', 67),
(681, 'POMACANCHI', 67),
(682, 'RONDOCAN', 67),
(683, 'SANGARARA', 67),
(684, 'ANCAHUASI', 68),
(685, 'ANTA', 68),
(686, 'CACHIMAYO', 68),
(687, 'CHINCHAYPUJIO', 68),
(688, 'HUAROCONDO', 68),
(689, 'LIMATAMBO', 68),
(690, 'MOLLEPATA', 68),
(691, 'PUCYURA', 68),
(692, 'ZURITE', 68),
(693, 'CALCA', 69),
(694, 'COYA', 69),
(695, 'LAMAY', 69),
(696, 'LARES', 69),
(697, 'PISAC', 69),
(698, 'SAN SALVADOR', 69),
(699, 'TARAY', 69),
(700, 'YANATILE', 69),
(701, 'CHECCA', 70),
(702, 'KUNTURKANKI', 70),
(703, 'LANGUI', 70),
(704, 'LAYO', 70),
(705, 'PAMPAMARCA', 70),
(706, 'QUEHUE', 70),
(707, 'TUPAC AMARU', 70),
(708, 'YANAOCA', 70),
(709, 'CHECACUPE', 71),
(710, 'COMBAPATA', 71),
(711, 'MARANGANI', 71),
(712, 'PITUMARCA', 71),
(713, 'SAN PABLO', 71),
(714, 'SAN PEDRO', 71),
(715, 'SICUANI', 71),
(716, 'TINTA', 71),
(717, 'CAPACMARCA', 72),
(718, 'CHAMACA', 72),
(719, 'COLQUEMARCA', 72),
(720, 'LIVITACA', 72),
(721, 'LLUSCO', 72),
(722, 'QUIÑOTA', 72),
(723, 'SANTO TOMAS', 72),
(724, 'VELILLE', 72),
(725, 'CCORCA', 73),
(726, 'CUSCO', 73),
(727, 'POROY', 73),
(728, 'SAN JERONIMO', 73),
(729, 'SAN SEBASTIAN', 73),
(730, 'SANTIAGO', 73),
(731, 'SAYLLA', 73),
(732, 'WANCHAQ', 73),
(733, 'ALTO PICHIGUA', 74),
(734, 'CONDOROMA', 74),
(735, 'COPORAQUE', 74),
(736, 'ESPINAR', 74),
(737, 'OCORURO', 74),
(738, 'PALLPATA', 74),
(739, 'PICHIGUA', 74),
(740, 'SUYCKUTAMBO', 74),
(741, 'ECHARATE', 75),
(742, 'HUAYOPATA', 75),
(743, 'MARANURA', 75),
(744, 'OCOBAMBA', 75),
(745, 'PICHARI', 75),
(746, 'QUELLOUNO', 75),
(747, 'QUIMBIRI', 75),
(748, 'SANTA ANA', 75),
(749, 'SANTA TERESA', 75),
(750, 'VILCABAMBA', 75),
(751, 'ACCHA', 76),
(752, 'CCAPI', 76),
(753, 'COLCHA', 76),
(754, 'HUANOQUITE', 76),
(755, 'OMACHA', 76),
(756, 'PACCARITAMBO', 76),
(757, 'PARURO', 76),
(758, 'PILLPINTO', 76),
(759, 'YAURISQUE', 76),
(760, 'CAICAY', 77),
(761, 'CHALLABAMBA', 77),
(762, 'COLQUEPATA', 77),
(763, 'HUANCARANI', 77),
(764, 'KOSÑIPATA', 77),
(765, 'PAUCARTAMBO', 77),
(766, 'ANDAHUAYLILLAS', 78),
(767, 'CAMANTI', 78),
(768, 'CCARHUAYO', 78),
(769, 'CCATCA', 78),
(770, 'CUSIPATA', 78),
(771, 'HUARO', 78),
(772, 'LUCRE', 78),
(773, 'MARCAPATA', 78),
(774, 'OCONGATE', 78),
(775, 'OROPESA', 78),
(776, 'QUIQUIJANA', 78),
(777, 'URCOS', 78),
(778, 'CHINCHERO', 79),
(779, 'HUAYLLABAMBA', 79),
(780, 'MACHUPICCHU', 79),
(781, 'MARAS', 79),
(782, 'OLLANTAYTAMBO', 79),
(783, 'URUBAMBA', 79),
(784, 'YUCAY', 79),
(785, 'ACOBAMBA', 80),
(786, 'ANDABAMBA', 80),
(787, 'ANTA', 80),
(788, 'CAJA', 80),
(789, 'MARCAS', 80),
(790, 'PAUCARA', 80),
(791, 'POMACOCHA', 80),
(792, 'ROSARIO', 80),
(793, 'ANCHONGA', 81),
(794, 'CALLANMARCA', 81),
(795, 'CCOCHACCASA', 81),
(796, 'CHINCHO', 81),
(797, 'CONGALLA', 81),
(798, 'HUANCA-HUANCA', 81),
(799, 'HUAYLLAY GRANDE', 81),
(800, 'JULCAMARCA', 81),
(801, 'LIRCAY', 81),
(802, 'SAN ANTONIO DE ANTAPARCO', 81),
(803, 'SANTO TOMAS DE PATA', 81),
(804, 'SECCLLA', 81),
(805, 'ARMA', 82),
(806, 'AURAHUA', 82),
(807, 'CAPILLAS', 82),
(808, 'CASTROVIRREYNA', 82),
(809, 'CHUPAMARCA', 82),
(810, 'COCAS', 82),
(811, 'HUACHOS', 82),
(812, 'HUAMATAMBO', 82),
(813, 'MOLLEPAMPA', 82),
(814, 'SAN JUAN', 82),
(815, 'SANTA ANA', 82),
(816, 'TANTARA', 82),
(817, 'TICRAPO', 82),
(818, 'ANCO', 83),
(819, 'CHINCHIHUASI', 83),
(820, 'CHURCAMPA', 83),
(821, 'EL CARMEN', 83),
(822, 'LA MERCED', 83),
(823, 'LOCROJA', 83),
(824, 'PACHAMARCA', 83),
(825, 'PAUCARBAMBA', 83),
(826, 'SAN MIGUEL DE MAYOCC', 83),
(827, 'SAN PEDRO DE CORIS', 83),
(828, 'ACOBAMBILLA', 84),
(829, 'ACORIA', 84),
(830, 'ASCENCION', 84),
(831, 'CONAYCA', 84),
(832, 'CUENCA', 84),
(833, 'HUACHOCOLPA', 84),
(834, 'HUANCAVELICA', 84),
(835, 'HUANDO', 84),
(836, 'HUANDO', 84),
(837, 'HUAYLLAHUARA', 84),
(838, 'IZCUCHACA', 84),
(839, 'LARIA', 84),
(840, 'MANTA', 84),
(841, 'MARISCAL CACERES', 84),
(842, 'MOYA', 84),
(843, 'NUEVO OCCORO', 84),
(844, 'PALCA', 84),
(845, 'PILCHACA', 84),
(846, 'VILCA', 84),
(847, 'YAULI', 84),
(848, 'AYAVI', 85),
(849, 'CORDOVA', 85),
(850, 'HUAYACUNDO ARMA', 85),
(851, 'HUAYTARA', 85),
(852, 'LARAMARCA', 85),
(853, 'OCOYO', 85),
(854, 'PILPICHACA', 85),
(855, 'QUERCO', 85),
(856, 'QUITO-ARMA', 85),
(857, 'SAN ANTONIO DE CUSICANCHA', 85),
(858, 'SAN FSCO. DE SANGAYAICO', 85),
(859, 'SAN ISIDRO', 85),
(860, 'SANTIAGO DE CHOCORVOS', 85),
(861, 'SANTIAGO DE QUIRAHUARA', 85),
(862, 'SANTO DOMINGO DE CAPILLAS', 85),
(863, 'TAMBO', 85),
(864, 'ACOSTAMBO', 86),
(865, 'ACRAQUIA', 86),
(866, 'AHUAYCHA', 86),
(867, 'COLCABAMBA', 86),
(868, 'DANIEL HERNANDEZ', 86),
(869, 'HUACHOCOLPA', 86),
(870, 'HUARIBAMBA', 86),
(871, 'PAMPAS', 86),
(872, 'PAZOS', 86),
(873, 'QUISHUAR', 86),
(874, 'SALCABAMBA', 86),
(875, 'SALCAHUASI', 86),
(876, 'SAN MARCOS DE ROCCHAC', 86),
(877, 'SURCUBAMBA', 86),
(878, 'TINTAY PUNCU', 86),
(879, 'YAHUIMPUQUIO', 86),
(880, 'AMBO', 87),
(881, 'CAYNA', 87),
(882, 'COLPAS', 87),
(883, 'CONCHAMARCA', 87),
(884, 'HUACAR', 87),
(885, 'SAN FRANCISCO', 87),
(886, 'SAN RAFAEL', 87),
(887, 'TOMAYQUICHUA', 87),
(888, 'CHUQUIS', 88),
(889, 'LA UNION', 88),
(890, 'MARIAS', 88),
(891, 'PACHAS', 88),
(892, 'QUIVILLA', 88),
(893, 'RIPAN', 88),
(894, 'SHUNQUI', 88),
(895, 'SILLAPATA', 88),
(896, 'YANAS', 88),
(897, 'CANCHABAMBA', 89),
(898, 'COCHABAMBA', 89),
(899, 'HUACAYBAMBA', 89),
(900, 'PINRA', 89),
(901, 'ARANCAY', 90),
(902, 'CHAVIN DE PARIARCA', 90),
(903, 'JACAS GRANDE', 90),
(904, 'JIRCAN', 90),
(905, 'LLATA', 90),
(906, 'MIRAFLORES', 90),
(907, 'MONZON', 90),
(908, 'PUNCHAO', 90),
(909, 'PUÑOS', 90),
(910, 'SINGA', 90),
(911, 'TANTAMAYO', 90),
(912, 'AMARILIS', 91),
(913, 'CHINCHAO', 91),
(914, 'CHURUBAMBA', 91),
(915, 'HUANUCO', 91),
(916, 'MARGOS', 91),
(917, 'PILCOMARCA', 91),
(918, 'QUISQUI', 91),
(919, 'SAN FRANCISCO DE CAYRAN', 91),
(920, 'SAN PEDRO DE CHAULAN', 91),
(921, 'SANTA MARIA DEL VALLE', 91),
(922, 'YARUMAYO', 91),
(923, 'BAÑOS', 92),
(924, 'JESUS', 92),
(925, 'JIVIA', 92),
(926, 'QUEROPALCA', 92),
(927, 'RONDOS', 92),
(928, 'SAN FRANCISCO DE ASIS', 92),
(929, 'SAN MIGUEL DE CAURI', 92),
(930, 'DANIEL ALOMIA  ROBLES', 93),
(931, 'HERMILIO VALDIZAN', 93),
(932, 'JOSE CRESPO Y CASTILLO', 93),
(933, 'LUYANDO', 93),
(934, 'MARIANO DAMASO BERAUN', 93),
(935, 'RUPA-RUPA', 93),
(936, 'CHOLON', 94),
(937, 'HUACRACHUCO', 94),
(938, 'SAN BUENAVENTURA', 94),
(939, 'CHAGLLA', 95),
(940, 'MOLINO', 95),
(941, 'PANAO', 95),
(942, 'UMARI', 95),
(943, 'CODO DEL POZUZO', 96),
(944, 'HONORIA', 96),
(945, 'PUERTO INCA', 96),
(946, 'TOURNAVISTA', 96),
(947, 'YUYAPICHIS', 96),
(948, 'APARICIO POMARES', 97),
(949, 'CAHUAC', 97),
(950, 'CHACABAMBA', 97),
(951, 'CHAVINILLO', 97),
(952, 'CHORAS', 97),
(953, 'JACAS CHICO', 97),
(954, 'OBAS', 97),
(955, 'PAMPAMARCA', 97),
(956, 'ALTO LARAN', 98),
(957, 'CHAVIN', 98),
(958, 'CHINCHA ALTA', 98),
(959, 'CHINCHA BAJA', 98),
(960, 'EL CARMEN', 98),
(961, 'GROCIO PRADO', 98),
(962, 'PUEBLO NUEVO', 98),
(963, 'SAN JUAN DE YANAC', 98),
(964, 'SAN PEDRO DE HUACARPANA', 98),
(965, 'SUNAMPE', 98),
(966, 'TAMBO DE MORA', 98),
(967, 'ICA', 99),
(968, 'LA TINGUIÑA', 99),
(969, 'LOS AQUIJES', 99),
(970, 'OCUCAJE', 99),
(971, 'PACHACUTEC', 99),
(972, 'PARCONA', 99),
(973, 'PUEBLO NUEVO', 99),
(974, 'SALAS', 99),
(975, 'SAN JOSE DE LOS MOLINOS', 99),
(976, 'SAN JUAN BAUTISTA', 99),
(977, 'SANTIAGO', 99),
(978, 'SUBTANJALLA', 99),
(979, 'TATE', 99),
(980, 'YAUCA DEL ROSARIO', 99),
(981, 'CHANGUILLO', 100),
(982, 'EL INGENIO', 100),
(983, 'MARCONA', 100),
(984, 'NAZCA', 100),
(985, 'VISTA ALEGRE', 100),
(986, 'LLIPATA', 101),
(987, 'PALPA', 101),
(988, 'RIO GRANDE', 101),
(989, 'SANTA CRUZ', 101),
(990, 'TIBILLO', 101),
(991, 'HUANCANO', 102),
(992, 'HUMAY', 102),
(993, 'INDEPENDENCIA', 102),
(994, 'PARACAS', 102),
(995, 'PISCO', 102),
(996, 'SAN ANDRES', 102),
(997, 'SAN CLEMENTE', 102),
(998, 'TUPAC AMARU INCA', 102),
(999, 'CHANCHAMAYO', 103),
(1000, 'PERENE', 103),
(1001, 'PICHANAQUI', 103),
(1002, 'SAN LUIS DE SHUARO', 103),
(1003, 'SAN RAMON', 103),
(1004, 'VITOC', 103),
(1005, 'AHUAC', 104),
(1006, 'CHONGOS BAJO', 104),
(1007, 'CHUPACA', 104),
(1008, 'HUACHAC', 104),
(1009, 'HUAMANCACA CHICO', 104),
(1010, 'SAN JUAN DE ISCOS', 104),
(1011, 'SAN JUAN DE JARPA', 104),
(1012, 'TRES DE DICIEMBRE', 104),
(1013, 'YANACANCHA', 104),
(1014, 'ACO', 105),
(1015, 'ANDAMARCA', 105),
(1016, 'CHAMBARA', 105),
(1017, 'COCHAS', 105),
(1018, 'COMAS', 105),
(1019, 'CONCEPCION', 105),
(1020, 'HEROINAS TOLEDO', 105),
(1021, 'MANZANARES', 105),
(1022, 'MARISCAL CASTILLA', 105),
(1023, 'MATAHUASI', 105),
(1024, 'MITO', 105),
(1025, 'NUEVE DE JULIO', 105),
(1026, 'ORCOTUNA', 105),
(1027, 'SAN JOSE DE QUERO', 105),
(1028, 'SANTA ROSA DE OCOPA', 105),
(1029, 'CARHUACALLANGA', 106),
(1030, 'CHACAPAMPA', 106),
(1031, 'CHICCHE', 106),
(1032, 'CHILCA', 106),
(1033, 'CHONGOS ALTO', 106),
(1034, 'CHUPURO', 106),
(1035, 'COLCA', 106),
(1036, 'CULLHUAS', 106),
(1037, 'EL TAMBO', 106),
(1038, 'HUACRAPUQUIO', 106),
(1039, 'HUALHUAS', 106),
(1040, 'HUANCAN', 106),
(1041, 'HUANCAYO', 106),
(1042, 'HUASICANCHA', 106),
(1043, 'HUAYUCACHI', 106),
(1044, 'INGENIO', 106),
(1045, 'PARIAHUANCA', 106),
(1046, 'PILCOMAYO', 106),
(1047, 'PUCARA', 106),
(1048, 'QUICHUAY', 106),
(1049, 'QUILCAS', 106),
(1050, 'SAN AGUSTIN', 106),
(1051, 'SAN JERONIMO DE TUNAN', 106),
(1052, 'SANTO DOMINGO DE ACOBAMBA', 106),
(1053, 'SAÑO', 106),
(1054, 'SAPALLANGA', 106),
(1055, 'SICAYA', 106),
(1056, 'VIQUES', 106),
(1057, 'ACOLLA', 107),
(1058, 'APATA', 107),
(1059, 'ATAURA', 107),
(1060, 'CANCHAYLLO', 107),
(1061, 'CURICACA', 107),
(1062, 'EL MANTARO', 107),
(1063, 'HUAMALI', 107),
(1064, 'HUARIPAMPA', 107),
(1065, 'HUERTAS', 107),
(1066, 'JANJAILLO', 107),
(1067, 'JAUJA', 107),
(1068, 'JULCAN', 107),
(1069, 'LEONOR ORDOÑEZ', 107),
(1070, 'LLOCLLAPAMPA', 107),
(1071, 'MARCO', 107),
(1072, 'MASMA', 107),
(1073, 'MASMA CHICCHE', 107),
(1074, 'MOLINOS', 107),
(1075, 'MONOBAMBA', 107),
(1076, 'MUQUI', 107),
(1077, 'MUQUIYAUYO', 107),
(1078, 'PACA', 107),
(1079, 'PACCHA', 107),
(1080, 'PANCAN', 107),
(1081, 'PARCO', 107),
(1082, 'POMACANCHA', 107),
(1083, 'RICRAN', 107),
(1084, 'SAN LORENZO', 107),
(1085, 'SAN PEDRO DE CHUNAN', 107),
(1086, 'SAUSA', 107),
(1087, 'SINCOS', 107),
(1088, 'TUNAN MARCA', 107),
(1089, 'YAULI', 107),
(1090, 'YAUYOS', 107),
(1091, 'CARHUAMAYO', 108),
(1092, 'JUNIN', 108),
(1093, 'ONDORES', 108),
(1094, 'ULCUMAYO', 108),
(1095, 'COVIRIALI', 109),
(1096, 'LLAYLLA', 109),
(1097, 'MAZAMARI', 109),
(1098, 'PAMPA HERMOSA', 109),
(1099, 'PANGOA', 109),
(1100, 'RIO NEGRO', 109),
(1101, 'RIO TAMBO', 109),
(1102, 'SATIPO', 109),
(1103, 'ACOBAMBA', 110),
(1104, 'HUARICOLCA', 110),
(1105, 'HUASAHUASI', 110),
(1106, 'LA UNION', 110),
(1107, 'PALCA', 110),
(1108, 'PALCAMAYO', 110),
(1109, 'SAN PEDRO DE CAJAS', 110),
(1110, 'TAPO', 110),
(1111, 'TARMA', 110),
(1112, 'CHACAPALPA', 111),
(1113, 'HUAY-HUAY', 111),
(1114, 'LA OROYA', 111),
(1115, 'MARCAPOMACOCHA', 111),
(1116, 'MOROCOCHA', 111),
(1117, 'PACCHA', 111),
(1118, 'SANTA ROSA DE SACCO', 111),
(1119, 'STA. BARBARA DE CARHUACAYAN', 111),
(1120, 'SUITUCANCHA', 111),
(1121, 'YAULI', 111),
(1122, 'ASCOPE', 112),
(1123, 'CASA GRANDE', 112),
(1124, 'CHICAMA', 112),
(1125, 'CHOCOPE', 112),
(1126, 'MAGDALENA DE CAO', 112),
(1127, 'PAIJAN', 112),
(1128, 'RAZURI', 112),
(1129, 'SANTIAGO DE CAO', 112),
(1130, 'BAMBAMARCA', 113),
(1131, 'BOLIVAR', 113),
(1132, 'CONDORMARCA', 113),
(1133, 'LONGOTEA', 113),
(1134, 'UCHUMARCA', 113),
(1135, 'UCUNCHA', 113),
(1136, 'CHEPEN', 114),
(1137, 'PACANGA', 114),
(1138, 'PUEBLO NUEVO', 114),
(1139, 'CASCAS', 115),
(1140, 'LUCMA', 115),
(1141, 'MARMOT', 115),
(1142, 'SAYAPULLO', 115),
(1143, 'CALAMARCA', 116),
(1144, 'CARABAMBA', 116),
(1145, 'HUASO', 116),
(1146, 'JULCAN', 116),
(1147, 'AGALLPAMPA', 117),
(1148, 'CHARAT', 117),
(1149, 'HUARANCHAL', 117),
(1150, 'LA CUESTA', 117),
(1151, 'MACHE', 117),
(1152, 'OTUZCO', 117),
(1153, 'PARANDAY', 117),
(1154, 'SALPO', 117),
(1155, 'SINSICAP', 117),
(1156, 'USQUIL', 117),
(1157, 'GUADALUPE', 118),
(1158, 'JEQUETEPEQUE', 118),
(1159, 'PACASMAYO', 118),
(1160, 'SAN JOSE', 118),
(1161, 'SAN PEDRO DE LLOC', 118),
(1162, 'BULDIBUYO', 119),
(1163, 'CHILLIA', 119),
(1164, 'HUANCASPATA', 119),
(1165, 'HUAYLILLAS', 119),
(1166, 'HUAYO', 119),
(1167, 'ONGON', 119),
(1168, 'PARCOY', 119),
(1169, 'PATAZ', 119),
(1170, 'PIAS', 119),
(1171, 'SANTIAGO DE CHALLAS', 119),
(1172, 'TAURIJA', 119),
(1173, 'TAYABAMBA', 119),
(1174, 'URPAY', 119),
(1175, 'CHUGAY', 120),
(1176, 'COCHORCO', 120),
(1177, 'CURGOS', 120),
(1178, 'HUAMACHUCO', 120),
(1179, 'MARCABAL', 120),
(1180, 'SANAGORAN', 120),
(1181, 'SARIN', 120),
(1182, 'SARTIMBAMBA', 120),
(1183, 'ANGASMARCA', 121),
(1184, 'CACHICADAN', 121),
(1185, 'MOLLEBAMBA', 121),
(1186, 'MOLLEPATA', 121),
(1187, 'QUIRUVILCA', 121),
(1188, 'SANTA CRUZ DE CHUCA', 121),
(1189, 'SANTIAGO DE CHUCO', 121),
(1190, 'SITABAMBA', 121),
(1191, 'EL PORVENIR', 122),
(1192, 'FLORENCIA DE MORA', 122),
(1193, 'HUANCHACO', 122),
(1194, 'LA ESPERANZA', 122),
(1195, 'LAREDO', 122),
(1196, 'MOCHE', 122),
(1197, 'POROTO', 122),
(1198, 'SALAVERRY', 122),
(1199, 'SIMBAL', 122),
(1200, 'TRUJILLO', 122),
(1201, 'VICTOR LARCO HERRERA', 122),
(1202, 'CHAO', 123),
(1203, 'GUADALUPITO', 123),
(1204, 'VIRU', 123),
(1205, 'CAYALTI', 124),
(1206, 'CHICLAYO', 124),
(1207, 'CHONGOYAPE', 124),
(1208, 'ETEN', 124),
(1209, 'ETEN PUERTO', 124),
(1210, 'JOSE LEONARDO ORTIZ', 124),
(1211, 'LA VICTORIA', 124),
(1212, 'LAGUNAS', 124),
(1213, 'MONSEFU', 124),
(1214, 'NUEVA ARICA', 124),
(1215, 'OYOTUN', 124),
(1216, 'PATAPO', 124),
(1217, 'PICSI', 124),
(1218, 'PIMENTEL', 124),
(1219, 'POMALCA', 124),
(1220, 'PUCALA', 124),
(1221, 'REQUE', 124),
(1222, 'SANTA ROSA', 124),
(1223, 'SAÑA', 124),
(1224, 'TUMAN', 124),
(1225, 'CANARIS', 125),
(1226, 'FERRENAFE', 125),
(1227, 'INCAHUASI', 125),
(1228, 'MANUEL A. MESONES MURO', 125),
(1229, 'PITIPO', 125),
(1230, 'PUEBLO NUEVO', 125),
(1231, 'CHOCHOPE', 126),
(1232, 'ILLIMO', 126),
(1233, 'JAYANCA', 126),
(1234, 'LAMBAYEQUE', 126),
(1235, 'MOCHUMI', 126),
(1236, 'MORROPE', 126),
(1237, 'MOTUPE', 126),
(1238, 'OLMOS', 126),
(1239, 'PACORA', 126),
(1240, 'SALAS', 126),
(1241, 'SAN JOSE', 126),
(1242, 'TUCUME', 126),
(1243, 'BARRANCA', 127),
(1244, 'PARAMONGA', 127),
(1245, 'PATIVILCA', 127),
(1246, 'SUPE', 127),
(1247, 'SUPE PUERTO', 127),
(1248, 'CAJATAMBO', 128),
(1249, 'COPA', 128),
(1250, 'GORGOR', 128),
(1251, 'HUANCAPON', 128),
(1252, 'MANAS', 128),
(1253, 'BELLAVISTA', 129),
(1254, 'CALLAO', 129),
(1255, 'CARMEN DE LA LEGUA  REYNOSO', 129),
(1256, 'LA PERLA', 129),
(1257, 'LA PUNTA', 129),
(1258, 'VENTANILLA', 129),
(1259, 'ARAHUAY', 130),
(1260, 'CANTA', 130),
(1261, 'HUAMANTANGA', 130),
(1262, 'HUAROS', 130),
(1263, 'LACHAQUI', 130),
(1264, 'SAN BUENAVENTURA', 130),
(1265, 'SANTA ROSA DE QUIVES', 130),
(1266, 'ASIA', 131),
(1267, 'CALANGO', 131),
(1268, 'CERRO AZUL', 131),
(1269, 'CHILCA', 131),
(1270, 'COAYLLO', 131),
(1271, 'IMPERIAL', 131),
(1272, 'LUNAHUANA', 131),
(1273, 'MALA', 131),
(1274, 'NUEVO IMPERIAL', 131),
(1275, 'PACARAN', 131),
(1276, 'QUILMANA', 131),
(1277, 'SAN ANTONIO', 131),
(1278, 'SAN LUIS', 131),
(1279, 'SAN VICENTE DE CAÑETE', 131),
(1280, 'SANTA CRUZ DE FLORES', 131),
(1281, 'ZUÑIGA', 131),
(1282, 'ATAVILLOS ALTO', 132),
(1283, 'ATAVILLOS BAJO', 132),
(1284, 'AUCALLAMA', 132),
(1285, 'CHANCAY', 132),
(1286, 'HUARAL', 132),
(1287, 'IHUARI', 132),
(1288, 'LAMPIAN', 132),
(1289, 'PACARAOS', 132),
(1290, 'SAN MIGUEL DE ACOS', 132),
(1291, 'SANTA CRUZ DE ANDAMARCA', 132),
(1292, 'SUMBILCA', 132),
(1293, 'VEINTISIETE DE NOVIEMBRE', 132),
(1294, 'ANTIOQUIA', 133),
(1295, 'CALLAHUANCA', 133),
(1296, 'CARAMPOMA', 133),
(1297, 'CHICLA', 133),
(1298, 'CUENCA', 133),
(1299, 'HUACHUPAMPA', 133),
(1300, 'HUANZA', 133),
(1301, 'HUAROCHIRI', 133),
(1302, 'LAHUAYTAMBO', 133),
(1303, 'LANGA', 133),
(1304, 'LARAOS', 133),
(1305, 'MARIATANA', 133),
(1306, 'MATUCANA', 133),
(1307, 'RICARDO PALMA', 133),
(1308, 'SAN ANDRES DE TUPICOCHA', 133),
(1309, 'SAN ANTONIO', 133),
(1310, 'SAN BARTOLOME', 133),
(1311, 'SAN DAMIAN', 133),
(1312, 'SAN JUAN DE IRIS', 133),
(1313, 'SAN JUAN DE TANTARANCHE', 133),
(1314, 'SAN LORENZO DE QUINTI', 133),
(1315, 'SAN MATEO', 133),
(1316, 'SAN MATEO DE OTAO', 133),
(1317, 'SAN PEDRO DE CASTA', 133),
(1318, 'SAN PEDRO DE HUANCAYRE', 133),
(1319, 'SANGALLAYA', 133),
(1320, 'SANTA CRUZ DE COCACHACRA', 133),
(1321, 'SANTA EULALIA', 133),
(1322, 'SANTIAGO DE ANCHUCAYA', 133),
(1323, 'SANTIAGO DE TUNA', 133),
(1324, 'STO. DMGO. DE LOS OLLEROS', 133),
(1325, 'SURCO', 133),
(1326, 'AMBAR', 134),
(1327, 'CALETA DE CARQUIN', 134),
(1328, 'CHECRAS', 134),
(1329, 'HUACHO', 134),
(1330, 'HUALMAY', 134),
(1331, 'HUAURA', 134),
(1332, 'LEONCIO PRADO', 134),
(1333, 'PACCHO', 134),
(1334, 'SANTA LEONOR', 134),
(1335, 'SANTA MARIA', 134),
(1336, 'SAYAN', 134),
(1337, 'VEGUETA', 134),
(1338, 'ANCON', 135),
(1339, 'ATE', 135),
(1340, 'BARRANCO', 135),
(1341, 'BREÑA', 135),
(1342, 'CARABAYLLO', 135),
(1343, 'CHACLACAYO', 135),
(1344, 'CHORRILLOS', 135),
(1345, 'CIENEGUILLA', 135),
(1346, 'COMAS', 135),
(1347, 'EL AGUSTINO', 135),
(1348, 'INDEPENDENCIA', 135),
(1349, 'JESUS MARIA', 135),
(1350, 'LA MOLINA', 135),
(1351, 'LA VICTORIA', 135),
(1352, 'LIMA', 135),
(1353, 'LINCE', 135),
(1354, 'LOS OLIVOS', 135),
(1355, 'LURIGANCHO', 135),
(1356, 'LURIN', 135),
(1357, 'MAGDALENA DEL MAR', 135),
(1358, 'MAGDALENA VIEJA', 135),
(1359, 'MIRAFLORES', 135),
(1360, 'PACHACAMAC', 135),
(1361, 'PUCUSANA', 135),
(1362, 'PUENTE PIEDRA', 135),
(1363, 'PUNTA HERMOSA', 135),
(1364, 'PUNTA NEGRA', 135),
(1365, 'RIMAC', 135),
(1366, 'SAN BARTOLO', 135),
(1367, 'SAN BORJA', 135),
(1368, 'SAN ISIDRO', 135),
(1369, 'SAN JUAN DE LURIGANCHO', 135),
(1370, 'SAN JUAN DE MIRAFLORES', 135),
(1371, 'SAN LUIS', 135),
(1372, 'SAN MARTIN DE PORRES', 135),
(1373, 'SAN MIGUEL', 135),
(1374, 'SANTA ANITA', 135),
(1375, 'SANTA MARIA DEL MAR', 135),
(1376, 'SANTA ROSA', 135),
(1377, 'SANTIAGO DE SURCO', 135),
(1378, 'SURQUILLO', 135),
(1379, 'VILLA EL SALVADOR', 135),
(1380, 'VILLA MARIA DEL TRIUNFO', 135),
(1381, 'ANDAJES', 136),
(1382, 'CAUJUL', 136),
(1383, 'COCHAMARCA', 136),
(1384, 'NAVAN', 136),
(1385, 'OYON', 136),
(1386, 'PACHANGARA', 136),
(1387, 'ALIS', 137),
(1388, 'AYAUCA', 137),
(1389, 'AYAVIRI', 137),
(1390, 'AZANGARO', 137),
(1391, 'CACRA', 137),
(1392, 'CARANIA', 137),
(1393, 'CATAHUASI', 137),
(1394, 'CHOCOS', 137),
(1395, 'COCHAS', 137),
(1396, 'COLONIA', 137),
(1397, 'HONGOS', 137),
(1398, 'HUAMPARA', 137),
(1399, 'HUANCAYA', 137),
(1400, 'HUANGASCAR', 137),
(1401, 'HUANTAN', 137),
(1402, 'HUAYEC', 137),
(1403, 'LARAOS', 137),
(1404, 'LINCHA', 137),
(1405, 'MADEAN', 137),
(1406, 'MIRAFLORES', 137),
(1407, 'OMAS', 137),
(1408, 'PUTINZA', 137),
(1409, 'QUINCHES', 137),
(1410, 'QUINOCAY', 137),
(1411, 'SAN JOAQUIN', 137),
(1412, 'SAN PEDRO DE PILAS', 137),
(1413, 'TANTA', 137),
(1414, 'TAURIPAMPA', 137),
(1415, 'TOMAS', 137),
(1416, 'TUPE', 137),
(1417, 'VIÑAC', 137),
(1418, 'VITIS', 137),
(1419, 'YAUYOS', 137),
(1420, 'BALSAPUERTO', 138),
(1421, 'BARRANCA', 138),
(1422, 'CAHUAPANAS', 138),
(1423, 'JEBEROS', 138),
(1424, 'LAGUNAS', 138),
(1425, 'MANSERICHE', 138),
(1426, 'MORONA', 138),
(1427, 'PASTAZA', 138),
(1428, 'SANTA CRUZ', 138),
(1429, 'TENIENTE CESAR LOPEZ ROJAS', 138),
(1430, 'YURIMAGUAS', 138),
(1431, 'NAUTA', 139),
(1432, 'PARINARI', 139),
(1433, 'TIGRE', 139),
(1434, 'TROMPETEROS', 139),
(1435, 'URARINAS', 139),
(1436, 'PEBAS', 140),
(1437, 'RAMON CASTILLA', 140),
(1438, 'SAN PABLO', 140),
(1439, 'YAVARI', 140),
(1440, 'ALTO NANAY', 141),
(1441, 'BELEN', 141),
(1442, 'FERNANDO LORES', 141),
(1443, 'INDIANA', 141),
(1444, 'IQUITOS', 141),
(1445, 'LAS AMAZONAS', 141),
(1446, 'MAZAN', 141),
(1447, 'NAPO', 141),
(1448, 'PUNCHANA', 141),
(1449, 'PUTUMAYO', 141),
(1450, 'SAN JUAN BAUTISTA', 141),
(1451, 'TORRES CAUSANA', 141),
(1452, 'ALTO TAPICHE', 142),
(1453, 'CAPELO', 142),
(1454, 'EMILIO SAN MARTIN', 142),
(1455, 'JENARO HERRERA', 142),
(1456, 'MAQUIA', 142),
(1457, 'PUINAHUA', 142),
(1458, 'REQUENA', 142),
(1459, 'SAQUENA', 142),
(1460, 'SOPLIN', 142),
(1461, 'TAPICHE', 142),
(1462, 'YAQUERANA', 142),
(1463, 'YAQUERANA', 142),
(1464, 'CONTAMANA', 143),
(1465, 'INAHUAYA', 143),
(1466, 'PADRE MARQUEZ', 143),
(1467, 'PAMPA HERMOSA', 143),
(1468, 'SARAYACU', 143),
(1469, 'VARGAS GUERRA', 143),
(1470, 'FITZCARRALD', 144),
(1471, 'HUEPETUCHE', 144),
(1472, 'MADRE DE DIOS', 144),
(1473, 'MANU', 144),
(1474, 'IBERIA', 145),
(1475, 'IÑAPARI', 145),
(1476, 'TAHUAMANU', 145),
(1477, 'INAMBARI', 146),
(1478, 'LABERINTO', 146),
(1479, 'LAS PIEDRAS', 146),
(1480, 'TAMBOPATA', 146),
(1481, 'CHOJATA', 147),
(1482, 'COALAQUE', 147),
(1483, 'ICHUYA', 147),
(1484, 'LA CAPILLA', 147),
(1485, 'LLOQUE', 147),
(1486, 'MATALAQUE', 147),
(1487, 'OMATE', 147),
(1488, 'PUQUINA', 147),
(1489, 'QUINISTAQUILLAS', 147),
(1490, 'UBINAS', 147),
(1491, 'YUNGA', 147),
(1492, 'EL ALGARROBAL', 148),
(1493, 'ILO', 148),
(1494, 'PACOCHA', 148),
(1495, 'CARUMAS', 149),
(1496, 'CUCHUMBAYA', 149),
(1497, 'MOQUEGUA', 149),
(1498, 'SAMEGUA', 149),
(1499, 'SAN CRISTOBAL', 149),
(1500, 'TORATA', 149),
(1501, 'CHACAYAN', 150),
(1502, 'GOYLLARISQUIZGA', 150),
(1503, 'PAUCAR', 150),
(1504, 'SAN PEDRO DE PILLAO', 150),
(1505, 'SANTA ANA DE TUSI', 150),
(1506, 'TAPUC', 150),
(1507, 'VILCABAMBA', 150),
(1508, 'YANAHUANCA', 150),
(1509, 'CHONTABAMBA', 151),
(1510, 'HUANCABAMBA', 151),
(1511, 'OXAPAMPA', 151),
(1512, 'PALCAZU', 151),
(1513, 'POZUZO', 151),
(1514, 'PUERTO BERMUDEZ', 151),
(1515, 'VILLA RICA', 151),
(1516, 'CHAUPIMARCA', 152),
(1517, 'HUACHON', 152),
(1518, 'HUARIACA', 152),
(1519, 'HUAYLLAY', 152),
(1520, 'NINACACA', 152),
(1521, 'PALLANCHACRA', 152),
(1522, 'PAUCARTAMBO', 152),
(1523, 'SAN FCO.DE ASIS DE YARUSYACAN', 152),
(1524, 'SIMON BOLIVAR', 152),
(1525, 'TICLACAYAN', 152),
(1526, 'TINYAHUARCO', 152),
(1527, 'VICCO', 152),
(1528, 'YANACANCHA', 152),
(1529, 'AYABACA', 153),
(1530, 'FRIAS', 153),
(1531, 'JILILI', 153),
(1532, 'LAGUNAS', 153),
(1533, 'MONTERO', 153),
(1534, 'PACAIPAMPA', 153),
(1535, 'PAIMAS', 153),
(1536, 'SAPILLICA', 153),
(1537, 'SICCHEZ', 153),
(1538, 'SUYO', 153),
(1539, 'CANCHAQUE', 154),
(1540, 'EL CARMEN DE LA FRONTERA', 154),
(1541, 'HUANCABAMBA', 154),
(1542, 'HUARMACA', 154),
(1543, 'LALAQUIZ', 154),
(1544, 'SAN MIGUEL DE EL FAIQUE', 154),
(1545, 'SONDOR', 154),
(1546, 'SONDORILLO', 154),
(1547, 'BUENOS AIRES', 155),
(1548, 'CHALACO', 155),
(1549, 'CHULUCANAS', 155),
(1550, 'LA MATANZA', 155),
(1551, 'MORROPON', 155),
(1552, 'SALITRAL', 155),
(1553, 'SAN JUAN DE BIGOTE', 155),
(1554, 'SANTA CATALINA DE MOSSA', 155),
(1555, 'SANTO DOMINGO', 155),
(1556, 'YAMANGO', 155),
(1557, 'AMOTAPE', 156),
(1558, 'ARENAL', 156),
(1559, 'COLAN', 156),
(1560, 'LA HUACA', 156),
(1561, 'PAITA', 156),
(1562, 'TAMARINDO', 156),
(1563, 'VICHAYAL', 156),
(1564, 'CASTILLA', 157),
(1565, 'CATACAOS', 157),
(1566, 'CURA MORI', 157),
(1567, 'EL TALLAN', 157),
(1568, 'LA ARENA', 157),
(1569, 'LA UNION', 157),
(1570, 'LAS LOMAS', 157),
(1571, 'PIURA', 157),
(1572, 'TAMBO GRANDE', 157),
(1573, 'BELLAVISTA DE LA UNION', 158),
(1574, 'BERNAL', 158),
(1575, 'CRISTO NOS VALGA', 158),
(1576, 'RINCONADA LLICUAR', 158),
(1577, 'SECHURA', 158),
(1578, 'VICE', 158),
(1579, 'BELLAVISTA', 159),
(1580, 'IGNACIO ESCUDERO', 159),
(1581, 'LANCONES', 159),
(1582, 'MARCAVELICA', 159),
(1583, 'MIGUEL CHECA', 159),
(1584, 'QUERECOTILLO', 159),
(1585, 'SALITRAL', 159),
(1586, 'SULLANA', 159),
(1587, 'EL ALTO', 160),
(1588, 'LA BREA', 160),
(1589, 'LOBITOS', 160),
(1590, 'LOS ORGANOS', 160),
(1591, 'MANCORA', 160),
(1592, 'PARIÑAS', 160),
(1593, 'ACHAYA', 161),
(1594, 'ARAPA', 161),
(1595, 'ASILLO', 161),
(1596, 'AZANGARO', 161),
(1597, 'CAMINACA', 161),
(1598, 'CHUPA', 161),
(1599, 'JOSE D. CHOQUEHUANCA', 161),
(1600, 'MUYANI', 161),
(1601, 'POTONI', 161),
(1602, 'SAMAN', 161),
(1603, 'SAN ANTON', 161),
(1604, 'SAN JOSE', 161),
(1605, 'SAN JUAN DE SALINAS', 161),
(1606, 'SANTIAGO DE PUPUJA', 161),
(1607, 'TIRAPATA', 161),
(1608, 'AJOYANI', 162),
(1609, 'AYAPATA', 162),
(1610, 'COASA', 162),
(1611, 'CORANI', 162),
(1612, 'CRUCERO', 162),
(1613, 'ITUATA', 162),
(1614, 'MACUSANI', 162),
(1615, 'OLLACHEA', 162),
(1616, 'SAN GABAN', 162),
(1617, 'USICAYOS', 162),
(1618, 'DESAGUADERO', 163),
(1619, 'HUACULLANI', 163),
(1620, 'JULI', 163),
(1621, 'KELLUYO', 163),
(1622, 'PISACOMA', 163),
(1623, 'POMATA', 163),
(1624, 'ZEPITA', 163),
(1625, 'CAPAZO', 164),
(1626, 'CONDURIRI', 164),
(1627, 'ILAVE', 164),
(1628, 'PILCUYO', 164),
(1629, 'SANTA ROSA', 164),
(1630, 'COJATA', 165),
(1631, 'HUANCANE', 165),
(1632, 'HUATASANI', 165),
(1633, 'INCHUPALLA', 165),
(1634, 'PUSI', 165),
(1635, 'ROSASPATA', 165),
(1636, 'TARACO', 165),
(1637, 'VILQUE CHICO', 165),
(1638, 'CABANILLA', 166),
(1639, 'CALAPUJA', 166),
(1640, 'LAMPA', 166),
(1641, 'NICASIO', 166),
(1642, 'OCUVIRI', 166),
(1643, 'PALCA', 166),
(1644, 'PARATIA', 166),
(1645, 'PUCARA', 166),
(1646, 'SANTA LUCIA', 166),
(1647, 'VILAVILA', 166),
(1648, 'ANTAUTA', 167),
(1649, 'AYAVIRI', 167),
(1650, 'CUPI', 167),
(1651, 'LLALLI', 167),
(1652, 'MACARI', 167),
(1653, 'NUYOA', 167),
(1654, 'ORURILLO', 167),
(1655, 'SANTA ROSA', 167),
(1656, 'UMACHIRI', 167),
(1657, 'CONIMA', 168),
(1658, 'HUAYRAPATA', 168),
(1659, 'MOHO', 168),
(1660, 'TILALI', 168),
(1661, 'ACORA', 169),
(1662, 'AMANTANI', 169),
(1663, 'ATUNCOLLA', 169),
(1664, 'CAPACHICA', 169),
(1665, 'CHUCUITO', 169),
(1666, 'COATA', 169),
(1667, 'HUATA', 169),
(1668, 'MAYAZO', 169),
(1669, 'PAUCARCOLLA', 169),
(1670, 'PICHACANI', 169),
(1671, 'PLATERIA', 169),
(1672, 'PUNO', 169),
(1673, 'SAN ANTONIO', 169),
(1674, 'TIQUILLACA', 169),
(1675, 'VILQUE', 169),
(1676, 'ANANEA', 170),
(1677, 'PEDRO VILCA APAZA', 170),
(1678, 'PUTINA', 170),
(1679, 'QUILCAPUNCU', 170),
(1680, 'SINA', 170),
(1681, 'CABANA', 171),
(1682, 'CABANILLAS', 171),
(1683, 'CARACOTO', 171),
(1684, 'JULIACA', 171),
(1685, 'ALTO INAMBARI', 172),
(1686, 'CUYOCUYO', 172),
(1687, 'LIMBANI', 172),
(1688, 'PATAMBUCO', 172),
(1689, 'PHARA', 172),
(1690, 'QUIACA', 172),
(1691, 'SAN JUAN DEL ORO', 172),
(1692, 'SANDIA', 172),
(1693, 'YANAHUAYA', 172),
(1694, 'ANAPIA', 173),
(1695, 'COPANI', 173),
(1696, 'CUTURAPI', 173),
(1697, 'OLLARAYA', 173),
(1698, 'TINICACHI', 173),
(1699, 'UNICACHI', 173),
(1700, 'YUNGUYO', 173),
(1701, 'ALTO BIAVO', 174),
(1702, 'BAJO BIAVO', 174),
(1703, 'BELLAVISTA', 174),
(1704, 'HUALLAGA', 174),
(1705, 'SAN PABLO', 174),
(1706, 'SAN RAFAEL', 174),
(1707, 'AGUA BLANCA', 175),
(1708, 'SAN JOSE DE SISA', 175),
(1709, 'SAN MARTIN', 175),
(1710, 'SANTA ROSA', 175),
(1711, 'SHATOJA', 175),
(1712, 'ALTO SAPOSOA', 176),
(1713, 'EL ESLABON', 176),
(1714, 'PISCOYACU', 176),
(1715, 'SACANCHE', 176),
(1716, 'SAPOSOA', 176),
(1717, 'TINGO DE SAPOSOA', 176),
(1718, 'ALONSO DE ALVARADO', 177),
(1719, 'BARRANQUITA', 177),
(1720, 'CAYNARACHI', 177),
(1721, 'CUÑUMBUQUI', 177),
(1722, 'LAMAS', 177),
(1723, 'PINTO RECODO', 177),
(1724, 'RUMISAPA', 177),
(1725, 'SAN ROQUE DE CUMBAZA', 177),
(1726, 'SHANAO', 177),
(1727, 'TABALOSOS', 177),
(1728, 'ZAPATERO', 177),
(1729, 'CAMPANILLA', 178),
(1730, 'HUICUNGO', 178),
(1731, 'JUANJUI', 178),
(1732, 'PACHIZA', 178),
(1733, 'PAJARILLO', 178),
(1734, 'CALZADA', 179),
(1735, 'HABANA', 179),
(1736, 'JEPELACIO', 179),
(1737, 'MOYOBAMBA', 179),
(1738, 'SORITOR', 179),
(1739, 'YANTALO', 179),
(1740, 'BUENOS AIRES', 180),
(1741, 'CASPISAPA', 180),
(1742, 'PICOTA', 180),
(1743, 'PILLUANA', 180),
(1744, 'PUCACACA', 180),
(1745, 'SAN CRISTOBAL', 180),
(1746, 'SAN HILARION', 180),
(1747, 'SHAMBOYACU', 180),
(1748, 'TINGO DE PONASA', 180),
(1749, 'TRES UNIDOS', 180),
(1750, 'AWAJUN', 181),
(1751, 'ELIAS SOPLIN VARGAS', 181),
(1752, 'NUEVA CAJAMARCA', 181),
(1753, 'PARDO MIGUEL', 181),
(1754, 'POSIC', 181),
(1755, 'RIOJA', 181),
(1756, 'SAN FERNANDO', 181),
(1757, 'YORONGOS', 181),
(1758, 'YURACYACU', 181),
(1759, 'ALBERTO LEVEAU', 182),
(1760, 'CACATACHI', 182),
(1761, 'CHAZUTA', 182),
(1762, 'CHIPURANA', 182),
(1763, 'EL PORVENIR', 182),
(1764, 'HUIMBAYOC', 182),
(1765, 'JUAN GUERRA', 182),
(1766, 'LA BANDA DE SHILCAYO', 182),
(1767, 'MORALES', 182),
(1768, 'PAPAPLAYA', 182),
(1769, 'SAN ANTONIO', 182),
(1770, 'SAUCE', 182),
(1771, 'SHAPAJA', 182),
(1772, 'TARAPOTO', 182),
(1773, 'NUEVO PROGRESO', 183),
(1774, 'POLVORA', 183),
(1775, 'SHUNTE', 183),
(1776, 'TOCACHE', 183),
(1777, 'UCHIZA', 183),
(1778, 'CAIRANI', 184),
(1779, 'CAMILACA', 184),
(1780, 'CANDARAVE', 184),
(1781, 'CURIBAYA', 184),
(1782, 'HUANUARA', 184),
(1783, 'QUILAHUANI', 184),
(1784, 'ILABAYA', 185),
(1785, 'ITE', 185),
(1786, 'LOCUMBA', 185),
(1787, 'ALTO DE LA ALIANZA', 186),
(1788, 'CALANA', 186),
(1789, 'CIUDAD NUEVA', 186),
(1790, 'GREGORIO ALBARRACIN LANCHIPA', 186),
(1791, 'INCLAN', 186),
(1792, 'PACHIA', 186),
(1793, 'PALCA', 186),
(1794, 'POCOLLAY', 186),
(1795, 'SAMA', 186),
(1796, 'TACNA', 186),
(1797, 'ESTIQUE', 187),
(1798, 'ESTIQUE-PAMPA', 187),
(1799, 'HEROES ALBARRACIN', 187),
(1800, 'SITAJARA', 187),
(1801, 'SUSAPAYA', 187),
(1802, 'TARATA', 187),
(1803, 'TARUCACHI', 187),
(1804, 'TICACO', 187),
(1805, 'CASITAS', 188),
(1806, 'ZORRITOS', 188),
(1807, 'CORRALES', 189),
(1808, 'LA CRUZ', 189),
(1809, 'PAMPAS DE HOSPITAL', 189),
(1810, 'SAN JACINTO', 189),
(1811, 'SAN JUAN DE LA VIRGEN', 189),
(1812, 'TUMBES', 189),
(1813, 'AGUAS VERDES', 190),
(1814, 'MATAPALO', 190),
(1815, 'PAPAYAL', 190),
(1816, 'ZARUMILLA', 190),
(1817, 'RAYMONDI', 191),
(1818, 'SEPAHUA', 191),
(1819, 'TAHUANIA', 191),
(1820, 'YURUA', 191),
(1821, 'CALLERIA', 192),
(1822, 'CAMPOVERDE', 192),
(1823, 'IPARIA', 192),
(1824, 'MASISEA', 192),
(1825, 'NUEVA REQUENA', 192),
(1826, 'YARINACOCHA', 192),
(1827, 'CURIMANA', 193),
(1828, 'IRAZOLA', 193),
(1829, 'PADRE ABAD', 193),
(1830, 'PURUS', 194);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `idubigeo` int(11) DEFAULT NULL,
  `razon_social` text NOT NULL,
  `ruc` varchar(11) NOT NULL,
  `act_economica` text NOT NULL,
  `domicilio_fiscal` text NOT NULL,
  `comentario` text,
  `estado` varchar(5) NOT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  `usercreated` bigint(5) NOT NULL,
  `userupdated` bigint(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `idubigeo`, `razon_social`, `ruc`, `act_economica`, `domicilio_fiscal`, `comentario`, `estado`, `mes`, `year`, `usercreated`, `userupdated`, `created_at`, `updated_at`) VALUES
(1, 2, 'SERVICONSTRUYE S.A.C.', '20601369622', 'Activ.de Arquitectura e Ingenieria', 'MZA. O LOTE 15 ASC. SEÑOR DE LOS MILAGROS  LIMA', NULL, '0', '07', '2021', 1, NULL, '2021-07-02 20:42:30', '2021-07-02 20:42:30'),
(2, 3, 'MGC INGENIERIA Y SERVICIOS S.A.C - MGC S.A.C', '20491938740', 'ARQUITECTURA, INGENIERIA Y CONSULTORIA', 'JR. DOÃA ESTHER NRO 359 URB. LA VIRREYNA  LIMA', NULL, '0', '07', '2021', 2, NULL, '2021-07-09 23:23:14', '2021-07-09 23:23:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_contacto`
--

CREATE TABLE `empresa_contacto` (
  `id` int(11) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `celular` varchar(45) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `cargo` text NOT NULL,
  `usercreated` bigint(5) NOT NULL,
  `userupdated` bigint(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa_contacto`
--

INSERT INTO `empresa_contacto` (`id`, `idempresa`, `nombre`, `correo`, `celular`, `telefono`, `cargo`, `usercreated`, `userupdated`, `created_at`, `updated_at`) VALUES
(3, 1, 'cdcdw', 'cdw@gmail.com', '981766260', NULL, 'derfr', 1, NULL, '2021-07-05 20:37:59', '2021-07-05 20:37:59'),
(4, 2, 'CLAUDIA PORTON', 'CFORTON@MGC-INGENIERIA.COM', '947139102', NULL, 'GERENTE ADMINISTRATIVO', 2, NULL, '2021-07-09 23:23:14', '2021-07-09 23:23:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuestacovid`
--

CREATE TABLE `encuestacovid` (
  `idencuesta` int(11) NOT NULL,
  `pregunta` varchar(20) NOT NULL,
  `encuesta` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `encuestacovid`
--

INSERT INTO `encuestacovid` (`idencuesta`, `pregunta`, `encuesta`) VALUES
(1, '1', '1'),
(2, '2', '1'),
(3, '3', '1'),
(4, '4', '1'),
(5, '5', '1'),
(6, '6', '1'),
(7, '7', '1'),
(8, '8', '1'),
(9, '9', '1'),
(10, '10', '1'),
(11, '11', '1'),
(12, '12', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `epp`
--

CREATE TABLE `epp` (
  `id` int(11) NOT NULL,
  `idagente` int(11) NOT NULL,
  `entrega` varchar(50) NOT NULL,
  `tipo_equipo` varchar(45) NOT NULL,
  `usercreated` bigint(5) NOT NULL,
  `userupdated` bigint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `epp`
--

INSERT INTO `epp` (`id`, `idagente`, `entrega`, `tipo_equipo`, `usercreated`, `userupdated`) VALUES
(23, 48, 'Registro de entrega de equipos 001', '0', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `epp_equipos`
--

CREATE TABLE `epp_equipos` (
  `id` int(11) NOT NULL,
  `idagente` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `nombre` text NOT NULL,
  `frecuencia` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `epp_equipos`
--

INSERT INTO `epp_equipos` (`id`, `idagente`, `codigo`, `nombre`, `frecuencia`) VALUES
(10, 48, 'EQP001', 'MASCARILLAS', '1 Dia'),
(11, 48, 'EQP002', 'CARETAS', '6 Meses');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `epp_trabajador`
--

CREATE TABLE `epp_trabajador` (
  `id` int(11) NOT NULL,
  `idepp` int(11) NOT NULL,
  `idagente` int(11) NOT NULL,
  `nombres` text NOT NULL,
  `dni` text NOT NULL,
  `area` text NOT NULL,
  `puesto_trabajo` text NOT NULL,
  `fecha_entrega` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `epp_trabajador`
--

INSERT INTO `epp_trabajador` (`id`, `idepp`, `idagente`, `nombres`, `dni`, `area`, `puesto_trabajo`, `fecha_entrega`) VALUES
(93, 23, 48, 'juan', '12345678', 'TI', 'aa', NULL),
(94, 23, 48, 'pedro', '1524', 'cc', 'dd', NULL),
(95, 23, 48, 'jose', '1235', 'xx', 'ff', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `epp_trabajador_equipos`
--

CREATE TABLE `epp_trabajador_equipos` (
  `id` int(11) NOT NULL,
  `idepp` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `idagente` int(11) NOT NULL,
  `idtrabajador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `epp_trabajador_equipos`
--

INSERT INTO `epp_trabajador_equipos` (`id`, `idepp`, `idequipo`, `idagente`, `idtrabajador`) VALUES
(12, 23, 10, 48, 93),
(13, 23, 10, 48, 94),
(14, 23, 11, 48, 94);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `marca` varchar(45) NOT NULL,
  `modelo` varchar(45) NOT NULL,
  `serie` varchar(45) NOT NULL,
  `fecha_calibracion` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `mes` varchar(45) NOT NULL,
  `year` varchar(45) NOT NULL,
  `estado` bigint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `nombre`, `marca`, `modelo`, `serie`, `fecha_calibracion`, `fecha_termino`, `mes`, `year`, `estado`) VALUES
(1, 'MEDIDOR DE TEMPERATURA', 'TRACEABLE', '4184', '130713752', '2021-06-09', '2022-06-09', '06', '2021', 0),
(2, 'LUXÓMETRO', 'EXTECH', '407026', 'A.016013', '2021-06-09', '2022-06-09', '06', '2021', 2),
(11, 'LUXÓMETRO', 'EXTECH INSTRUMENTS', '407026', 'A.013603', '2021-11-26', '2022-11-26', '06', '2021', 2),
(12, 'LUXÓMETRO', 'EXTECH INSTRUMENTS', '407026', 'A.016956', '2021-11-26', '2022-11-26', '06', '2021', 0),
(13, 'DOSÍMETRO', 'SVANTEK', 'SV102+', '59303', '2021-11-26', '2022-11-26', '06', '2021', 2),
(14, 'SONÓMETRO', 'SVANTEK', 'SVAN971', '55490', '2021-11-26', '2022-11-26', '06', '2021', 0),
(15, 'SONÓMETRO', 'SVANTEK', 'SVAN971', '56185', '2021-11-26', '2022-11-26', '06', '2021', 2),
(16, 'DETECTOR MULTIGASES', 'MULTIRAE', 'PGM6208', '152-M1984', '2021-11-26', '2022-11-26', '06', '2021', 0),
(17, 'TERMOHIGROMETRO', 'CONTROL COMPANY', '4184', '130713752', '2021-11-26', '2022-11-26', '06', '2021', 0),
(18, 'LUXÓMETRO', 'EXTECH', '407026', 'A.016013', NULL, NULL, '06', '2021', 1),
(19, 'CALIBRADOR DE SONÓMETRO', 'SVANTEK', 'SV34', '58600', '2020-03-10', '2021-03-10', '06', '2021', 1),
(20, 'DISTANCIÓMETRO', 'LEICA', 'D810', '', '2018-11-05', '2019-11-05', '06', '2021', 1),
(21, 'LUXÓMETRO', 'EXTECH', '407026', 'A.041032', '2020-03-10', '2021-03-10', '06', '2021', 1),
(22, 'BOMBA GRAVIMÉTRICA', 'SKC', '220-5000TC', '12604', '2020-03-06', '2021-03-06', '06', '2021', 2),
(23, 'CALIBRADOR DE BOMBA', 'SKC', '375-07550', '16538488', '2020-03-06', '2021-03-06', '06', '2021', 1),
(24, 'ENVIRONMENTAL METER', 'EXTECH', '45170', 'A.093035', '2020-03-10', '2021-03-10', '06', '2021', 1),
(25, 'OTROS', 'OTROS', 'OTROS', 'OTROS', NULL, NULL, '01', '2021', 2);

--
-- Disparadores `equipos`
--
DELIMITER $$
CREATE TRIGGER `calcularfecha` BEFORE INSERT ON `equipos` FOR EACH ROW SET NEW.fecha_termino = date_add(new.fecha_calibracion, INTERVAL 1 year)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `calcularfechaupdate` BEFORE UPDATE ON `equipos` FOR EACH ROW SET NEW.fecha_termino = date_add(new.fecha_calibracion, INTERVAL 1 year)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos_detalle`
--

CREATE TABLE `equipos_detalle` (
  `id` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `idpersona` int(11) DEFAULT NULL,
  `motivo` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hproyectoempresa`
--

CREATE TABLE `hproyectoempresa` (
  `id` int(11) NOT NULL,
  `mes` varchar(45) NOT NULL,
  `year` varchar(45) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `cant_trab_emp_proy` varchar(20) DEFAULT NULL,
  `domicilio_fiscal` text,
  `usercreated` varchar(45) NOT NULL,
  `userupdated` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hproyectoempresa`
--

INSERT INTO `hproyectoempresa` (`id`, `mes`, `year`, `idempresa`, `cant_trab_emp_proy`, `domicilio_fiscal`, `usercreated`, `userupdated`, `created_at`, `updated_at`) VALUES
(1, '01', '2021', 1, '33', NULL, '1', NULL, '2021-07-05 15:39:48', '2021-07-05 15:39:48'),
(2, '07', '2021', 2, '25', NULL, '2', NULL, '2021-07-09 18:23:14', '2021-07-09 18:23:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inspecciones`
--

CREATE TABLE `inspecciones` (
  `id` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `fecha_hallazgo` date NOT NULL,
  `hora_hallazgo` varchar(45) NOT NULL,
  `descripcion_peligro` varchar(45) NOT NULL,
  `idcategoria_peligro` int(11) NOT NULL,
  `idsubcategoria_peligro` int(11) DEFAULT NULL,
  `area` varchar(45) NOT NULL,
  `subarea` varchar(45) NOT NULL,
  `tipo_ubicacion` text NOT NULL,
  `responsable` varchar(45) NOT NULL,
  `cargo` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `estado` bigint(5) NOT NULL,
  `foto1` text,
  `foto2` text,
  `foto3` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instalacion`
--

CREATE TABLE `instalacion` (
  `id` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `area_monitoreada` text,
  `instalacion` text,
  `descripcion_instalacion` text,
  `calle_numero` text,
  `distrito` text,
  `area_geografica` text,
  `region_geografica` text,
  `estado` bigint(5) DEFAULT NULL,
  `usercreated` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monitoreo`
--

CREATE TABLE `monitoreo` (
  `id` int(11) NOT NULL,
  `idagencia` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `idservicio` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observacion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ms_tagente`
--

CREATE TABLE `ms_tagente` (
  `id` int(11) NOT NULL,
  `idmonitoreo` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ms_tagente`
--

INSERT INTO `ms_tagente` (`id`, `idmonitoreo`, `nombre`, `estado`) VALUES
(1, 6, 'PLAN COVID-19', '1'),
(2, 1, 'RUIDO SONOMÉTRICO', '0'),
(3, 1, 'RUIDO DOSIMETRICO', '0'),
(4, 1, 'ILUMINACION', '0'),
(5, 1, 'HUMEDAD', '0'),
(6, 1, 'TEMPERATURA', '0'),
(7, 1, 'VELOCIDAD DEL AIRE', '0'),
(8, 2, 'SUPERFICIES INERTE (HISOPADO)S', '0'),
(9, 2, 'SUPERFICIES VIVAS (ENGUAJE)', '0'),
(10, 3, 'POLVO INHALABLE', '0'),
(11, 3, 'POLVO RESPIRABLE', '0'),
(12, 4, 'ISTAS', '0'),
(13, 4, 'F-SICO', '0'),
(14, 5, 'METODO RULA', '0'),
(15, 7, 'INSPECCIONES DE SST', '1'),
(16, 8, 'EPP', '1'),
(17, 9, 'IPERC', '1'),
(18, 10, 'MAPAS DE RIESGO', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ms_tmonitoreo`
--

CREATE TABLE `ms_tmonitoreo` (
  `id` int(11) NOT NULL,
  `monitoreo` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `idservicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ms_tmonitoreo`
--

INSERT INTO `ms_tmonitoreo` (`id`, `monitoreo`, `estado`, `idservicio`) VALUES
(1, 'FISICOS', '0', 1),
(2, 'BIOLOGICOS', '0', 1),
(3, 'QUIMICOS', '0', 1),
(4, 'RIESGO PSICOSOCIALES', '0', 1),
(5, 'DISERGONOMICOS', '0', 1),
(6, 'PLAN COVID-19', '1', 2),
(7, 'INSPECCIONES DE SST', '1', 3),
(8, 'EPP', '1', 4),
(9, 'IPERC', '1', 5),
(10, 'MAPAS DE RIESGO', '1', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ms_tservicio`
--

CREATE TABLE `ms_tservicio` (
  `id` int(11) NOT NULL,
  `servicios` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ms_tservicio`
--

INSERT INTO `ms_tservicio` (`id`, `servicios`, `descripcion`, `estado`) VALUES
(1, 'AGENTES OCUPACIONALES', '', '0'),
(2, 'PLAN COVID-19', '', '0'),
(3, 'INSPECCIONES DE SST', '', '0'),
(4, 'EPP', '', '0'),
(5, 'IPERC', '', '0'),
(6, 'MAPAS DE RIESGO', '', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcion`
--

CREATE TABLE `opcion` (
  `id` int(11) NOT NULL,
  `opcion` varchar(45) NOT NULL,
  `grupo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `nombre`, `descripcion`) VALUES
(1, 'dashboard.index', 'Pagina de Inicio'),
(2, 'empresa.index', 'Lista de Empresas'),
(3, 'empresa.agregar', 'Agregar Empresas'),
(4, 'empresa.editar', 'Editar Empresas'),
(5, 'empresa.dashboard', 'Indicadores de Empresa'),
(6, 'medicion.index', 'Lista de Equipos de Medicion'),
(7, 'medicion.agregar', 'Agregar Equipos de Medicion'),
(8, 'medicion.editar', 'Editar Equipos de Medicion'),
(9, 'medicion.detalle', 'Detalles de Equipos de Medicion'),
(10, 'medicion.dashboard', 'Indicadores de Equipos de Medicion'),
(11, 'personal.index', 'Lista de Personal de Hisso'),
(12, 'personal.agregar', 'Agregar Personal de Hisso'),
(13, 'personal.editar', 'Editar Personal de Hisso'),
(14, 'proyecto.index', 'Lista de Proyectos'),
(15, 'proyecto.agregar', 'Agregar Proyectos'),
(16, 'proyecto.editar', 'Editar Proyectos'),
(17, 'proyecto.ver', 'Ver Proyectos'),
(18, 'proyecto.dashboard', 'Indicadores de Proyectos'),
(19, 'trabajador.index', 'Lista de Trabajadores de Determinados servicios'),
(20, 'trabajador.agregarTrabajador', 'Agregar Trabajadores uno por uno de Determinados servicios'),
(21, 'trabajador.trabajador.editar', 'Editar Trabajadores de Determinados servicios'),
(22, 'trabajador.ver', 'Ver Trabajadores de Determinados servicios'),
(23, 'trabajador.trabajador.agregar', 'Agregar Trabajadores subiendo data en excel de Determinados servicios'),
(24, 'trabajador.agregarInstalacion', 'Agregar Instalaciones uno por uno de Determinados servicios'),
(25, 'trabajador.instalacion.agregar', 'Agregar Instalaciones subiendo data en excel de Determinados servicios'),
(26, 'trabajador.instalacion.editar', 'Editar Instalaciones de Determinados servicios'),
(27, 'usuario.index', 'Listar un usuario a un responsable (modulo en duda)'),
(28, 'usuario.agregar', 'Asignar un usuario a un responsable (modulo en duda)'),
(29, 'usuario.editar', 'Editar un usuario a un responsable (modulo en duda)'),
(30, 'asignacion.index', 'Listar responsables e equipos a un determinado Proyecto'),
(31, 'asignacion.agregar', 'Agregar responsables e equipos a un determinado Proyecto'),
(32, 'asignacion.editar', 'Editar responsables e equipos a un determinado Proyecto'),
(33, 'iluminacion.index', 'Listar Monitoreo Iluminacion'),
(34, 'iluminacion.agregar', 'Agregar Monitoreo Iluminacion'),
(35, 'iluminacion.editar', 'Editar Monitoreo Iluminacion'),
(36, 'ruido.index', 'Listar Monitoreo Ruido'),
(37, 'ruido.agregar', 'Agregar Monitoreo Ruido'),
(38, 'ruido.editar', 'Editar Monitoreo Ruido'),
(39, 'ergonomia.index', 'Listar Monitoreo Ergonomia'),
(40, 'ergonomia.agregar', 'Agregar Monitoreo Ergonomia'),
(41, 'ergonomia.editar', 'Editar Monitoreo Ergonomia'),
(42, 'biologico.index', 'Listar Monitoreo Biologico'),
(43, 'biologico.agregar', 'Agregar Monitoreo Biologico'),
(44, 'biologico.editar', 'Editar Monitoreo Biologico'),
(45, 'inspecciones.index', 'Listar Monitoreo Inspecciones'),
(46, 'inspecciones.agregar', 'Agregar Monitoreo Inspecciones'),
(47, 'inspecciones.editar', 'Editar Monitoreo Inspecciones'),
(48, 'inspecciones.ver', 'Ver Monitoreo Inspecciones'),
(49, 'covid.index', 'Listar Monitoreo Encuesta Covid'),
(50, 'covid.agregar', 'Agregar Monitoreo Encuesta Covid'),
(51, 'covid.meses', 'Ver Encuestas por Meses de Monitoreo Encuesta Covid'),
(52, 'epp.index', 'Listar Monitoreo EPP'),
(53, 'epp.agregar', 'Agregar Monitoreo EPP'),
(54, 'equiposepp.index', 'Listar Equipos Monitoreo EPP'),
(55, 'equiposepp.agregar', 'Agregar Equipos Monitoreo EPP'),
(56, 'epp.trabajador', 'Agregar Trabajador Monitoreo EPP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_usuario`
--

CREATE TABLE `permisos_usuario` (
  `permisos_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `estado` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso_establecido_agente`
--

CREATE TABLE `permiso_establecido_agente` (
  `permisos_id` int(11) NOT NULL,
  `ms_tagente_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idubigeo` int(11) DEFAULT NULL,
  `idempresa` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `celular` varchar(11) NOT NULL,
  `estado` bigint(5) NOT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `codigo_interno` varchar(45) DEFAULT NULL,
  `sexo` varchar(45) NOT NULL,
  `observacion` text,
  `direccion` text,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `usercreated` bigint(5) NOT NULL,
  `userupdated` bigint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `idusuario`, `idubigeo`, `idempresa`, `nombre`, `apellido`, `dni`, `fecha_nacimiento`, `celular`, `estado`, `correo`, `codigo_interno`, `sexo`, `observacion`, `direccion`, `fecha_inicio`, `fecha_fin`, `created_at`, `updated_at`, `usercreated`, `userupdated`) VALUES
(1, 1, NULL, 1, 'PEDRO ARTURO', 'RAMIREZ CAYCHO', '40942372', '1981-04-29', '963883101', 0, NULL, 'R0001', 'Masculino', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(2, 2, NULL, 1, 'DIANE MARIEL', 'SANTAMARÍA CALDERÓN', '45551874', '1988-03-15', '977727527', 0, '', 'R00002', 'Femenino', '', '', '2018-05-15', NULL, '2021-07-05 16:03:15', '2021-07-14 02:52:49', 1, NULL),
(3, 3, NULL, 1, 'EDUARDO ANTONIO', 'ALE ALARCON', '76586843', '1995-08-31', '994679869', 2, '', 'R00003', 'Masculino', '', '', NULL, NULL, '2021-07-05 16:07:47', '2021-07-09 23:34:38', 1, NULL),
(4, 4, NULL, 1, 'JAZMIN ESTHER', 'CÁCEDA PINEDO', '77044827', '1995-09-18', '960988531', 0, '', 'R00004', 'Femenino', '', '', '2019-08-01', NULL, '2021-07-05 16:11:16', '2021-07-14 03:02:38', 1, NULL),
(5, 5, NULL, 1, 'ROBINSON', 'ESPINAL ASENJO', '45630734', '1989-03-18', '960280396', 0, '', 'R00005', 'Masculino', '', '', '2020-03-02', NULL, '2021-07-05 16:14:17', '2021-07-14 03:02:42', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id` int(11) NOT NULL DEFAULT '0',
  `provincia` varchar(50) DEFAULT NULL,
  `idDepa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id`, `provincia`, `idDepa`) VALUES
(1, 'BAGUA', 1),
(2, 'BONGARA', 1),
(3, 'CHACHAPOYAS', 1),
(4, 'CONDORCANQUI', 1),
(5, 'LUYA', 1),
(6, 'RODRIGUEZ DE MENDOZA', 1),
(7, 'UTCUBAMBA', 1),
(8, 'AIJA', 2),
(9, 'ANTONIO RAYMONDI', 2),
(10, 'ASUNCION', 2),
(11, 'BOLOGNESI', 2),
(12, 'CARHUAZ', 2),
(13, 'CARLOS F.FITZCARRALD', 2),
(14, 'CASMA', 2),
(15, 'CORONGO', 2),
(16, 'HUARAZ', 2),
(17, 'HUARI', 2),
(18, 'HUARMEY', 2),
(19, 'HUAYLAS', 2),
(20, 'MARISCAL LUZURIAGA', 2),
(21, 'OCROS', 2),
(22, 'PALLASCA', 2),
(23, 'POMABAMBA', 2),
(24, 'RECUAY', 2),
(25, 'SANTA', 2),
(26, 'SIHUAS', 2),
(27, 'YUNGAY', 2),
(28, 'ABANCAY', 3),
(29, 'ANDAHUAYLAS', 3),
(30, 'ANTABAMBA', 3),
(31, 'AYMARAES', 3),
(32, 'CHINCHEROS', 3),
(33, 'COTABAMBAS', 3),
(34, 'GRAU', 3),
(35, 'AREQUIPA', 4),
(36, 'CAMANA', 4),
(37, 'CARAVELI', 4),
(38, 'CASTILLA', 4),
(39, 'CAYLLOMA', 4),
(40, 'CONDESUYOS', 4),
(41, 'ISLAY', 4),
(42, 'LA UNION', 4),
(43, 'CANGALLO', 5),
(44, 'HUAMANGA', 5),
(45, 'HUANCA SANCOS', 5),
(46, 'HUANTA', 5),
(47, 'LA MAR', 5),
(48, 'LUCANAS', 5),
(49, 'PARINACOCHAS', 5),
(50, 'PAUCAR DEL SARA SARA', 5),
(51, 'SUCRE', 5),
(52, 'VICTOR FAJARDO', 5),
(53, 'VILCASHUAMAN', 5),
(54, 'CAJABAMBA', 6),
(55, 'CAJAMARCA', 6),
(56, 'CELENDIN', 6),
(57, 'CHOTA', 6),
(58, 'CONTUMAZA', 6),
(59, 'CUTERVO', 6),
(60, 'HUALGAYOC', 6),
(61, 'JAEN', 6),
(62, 'SAN IGNACIO', 6),
(63, 'SAN MARCOS', 6),
(64, 'SAN MIGUEL', 6),
(65, 'SAN PABLO', 6),
(66, 'SANTA CRUZ', 6),
(67, 'ACOMAYO', 7),
(68, 'ANTA', 7),
(69, 'CALCA', 7),
(70, 'CANAS', 7),
(71, 'CANCHIS', 7),
(72, 'CHUMBIVILCAS', 7),
(73, 'CUSCO', 7),
(74, 'ESPINAR', 7),
(75, 'LA CONVENCION', 7),
(76, 'PARURO', 7),
(77, 'PAUCARTAMBO', 7),
(78, 'QUISPICANCHI', 7),
(79, 'URUBAMBA', 7),
(80, 'ACOBAMBA', 8),
(81, 'ANGARAES', 8),
(82, 'CASTROVIRREYNA', 8),
(83, 'CHURCAMPA', 8),
(84, 'HUANCAVELICA', 8),
(85, 'HUAYTARA', 8),
(86, 'TAYACAJA', 8),
(87, 'AMBO', 9),
(88, 'DOS DE MAYO', 9),
(89, 'HUACAYBAMBA', 9),
(90, 'HUAMALIES', 9),
(91, 'HUANUCO', 9),
(92, 'LAURICOCHA', 9),
(93, 'LEONCIO PRADO', 9),
(94, 'MARAÑON', 9),
(95, 'PACHITEA', 9),
(96, 'PUERTO INCA', 9),
(97, 'YAROWILCA', 9),
(98, 'CHINCHA', 10),
(99, 'ICA', 10),
(100, 'NAZCA', 10),
(101, 'PALPA', 10),
(102, 'PISCO', 10),
(103, 'CHANCHAMAYO', 11),
(104, 'CHUPACA', 11),
(105, 'CONCEPCION', 11),
(106, 'HUANCAYO', 11),
(107, 'JAUJA', 11),
(108, 'JUNIN', 11),
(109, 'SATIPO', 11),
(110, 'TARMA', 11),
(111, 'YAULI', 11),
(112, 'ASCOPE', 12),
(113, 'BOLIVAR', 12),
(114, 'CHEPEN', 12),
(115, 'GRAN CHIMU', 12),
(116, 'JULCAN', 12),
(117, 'OTUZCO', 12),
(118, 'PACASMAYO', 12),
(119, 'PATAZ', 12),
(120, 'SANCHEZ CARRION', 12),
(121, 'SANTIAGO DE CHUCO', 12),
(122, 'TRUJILLO', 12),
(123, 'VIRU', 12),
(124, 'CHICLAYO', 13),
(125, 'FERREÑAFE', 13),
(126, 'LAMBAYEQUE', 13),
(127, 'BARRANCA', 14),
(128, 'CAJATAMBO', 14),
(129, 'CALLAO', 14),
(130, 'CANTA', 14),
(131, 'CAÑETE', 14),
(132, 'HUARAL', 14),
(133, 'HUAROCHIRI', 14),
(134, 'HUAURA', 14),
(135, 'LIMA', 14),
(136, 'OYON', 14),
(137, 'YAUYOS', 14),
(138, 'ALTO AMAZONAS', 15),
(139, 'LORETO', 15),
(140, 'MARISCAL R.CASTILLA', 15),
(141, 'MAYNAS', 15),
(142, 'REQUENA', 15),
(143, 'UCAYALI', 15),
(144, 'MANU', 16),
(145, 'TAHUAMANU', 16),
(146, 'TAMBOPATA', 16),
(147, 'GENERAL SANCHEZ CERRO', 17),
(148, 'ILO', 17),
(149, 'MARISCAL NIETO', 17),
(150, 'DANIEL ALCIDES CARRION', 18),
(151, 'OXAPAMPA', 18),
(152, 'PASCO', 18),
(153, 'AYABACA', 19),
(154, 'HUANCABAMBA', 19),
(155, 'MORROPON', 19),
(156, 'PAITA', 19),
(157, 'PIURA', 19),
(158, 'SECHURA', 19),
(159, 'SULLANA', 19),
(160, 'TALARA', 19),
(161, 'AZANGARO', 20),
(162, 'CARABAYA', 20),
(163, 'CHUCUITO', 20),
(164, 'EL COLLAO', 20),
(165, 'HUANCANE', 20),
(166, 'LAMPA', 20),
(167, 'MELGAR', 20),
(168, 'MOHO', 20),
(169, 'PUNO', 20),
(170, 'SAN ANTONIO DE PUTINA', 20),
(171, 'SAN ROMAN', 20),
(172, 'SANDIA', 20),
(173, 'YUNGUYO', 20),
(174, 'BELLAVISTA', 21),
(175, 'EL DORADO', 21),
(176, 'HUALLAGA', 21),
(177, 'LAMAS', 21),
(178, 'MARISCAL CACERES', 21),
(179, 'MOYOBAMBA', 21),
(180, 'PICOTA', 21),
(181, 'RIOJA', 21),
(182, 'SAN MARTIN', 21),
(183, 'TOCACHE', 21),
(184, 'CANDARAVE', 22),
(185, 'JORGE BASADRE', 22),
(186, 'TACNA', 22),
(187, 'TARATA', 22),
(188, 'CONTRALMIRANTE VILLAR', 23),
(189, 'TUMBES', 23),
(190, 'ZARUMILLA', 23),
(191, 'ATALAYA', 24),
(192, 'CORONEL PORTILLO', 24),
(193, 'PADRE ABAD', 24),
(194, 'PURUS', 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `id` int(11) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `lima` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `year` varchar(45) NOT NULL,
  `estado` bigint(5) NOT NULL,
  `usercreated` bigint(5) NOT NULL,
  `userupdated` bigint(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`id`, `idempresa`, `codigo`, `fecha_inicio`, `fecha_fin`, `lima`, `provincia`, `mes`, `year`, `estado`, `usercreated`, `userupdated`, `created_at`, `updated_at`) VALUES
(15, 2, 'MGC I/001-2021', '2021-07-01', '2021-08-01', '5', '0', '07', '2021', 0, 1, NULL, '2021-07-16 20:55:31', '2021-07-16 20:55:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto_detalle`
--

CREATE TABLE `proyecto_detalle` (
  `id` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `motivo` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporte_proyecto_requisitos`
--

CREATE TABLE `reporte_proyecto_requisitos` (
  `id` int(11) NOT NULL,
  `razon` text NOT NULL,
  `codigo` varchar(11) NOT NULL,
  `idempresa` varchar(11) NOT NULL,
  `trabajadores` int(11) NOT NULL,
  `sedes` int(11) NOT NULL,
  `lima` varchar(45) NOT NULL,
  `provincia` varchar(2) NOT NULL,
  `iluminacion` varchar(2) NOT NULL,
  `ruido` varchar(2) NOT NULL,
  `ergonomia` varchar(2) NOT NULL,
  `asesoria` varchar(2) NOT NULL,
  `biologico` varchar(2) NOT NULL,
  `humedad` varchar(2) NOT NULL,
  `iperc` varchar(2) NOT NULL,
  `inspeccion` varchar(2) NOT NULL,
  `investigacion` varchar(2) NOT NULL,
  `psicosocial` varchar(2) NOT NULL,
  `supervision` varchar(2) NOT NULL,
  `estado` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requerimiento_proyecto`
--

CREATE TABLE `requerimiento_proyecto` (
  `id` int(11) NOT NULL,
  `idagente` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `requerimiento_proyecto`
--

INSERT INTO `requerimiento_proyecto` (`id`, `idagente`, `tipo`) VALUES
(2, 42, 'Trabajador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestacovid`
--

CREATE TABLE `respuestacovid` (
  `id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idusuario` int(11) NOT NULL,
  `pregunta` varchar(20) NOT NULL,
  `respuesta` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestacovid_filter`
--

CREATE TABLE `respuestacovid_filter` (
  `id` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` bigint(5) NOT NULL COMMENT '0= laboral;1=no laboral'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` text NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `descripcion`, `estado`) VALUES
(1, 'Gerente General', 'Rosa', 1),
(2, 'Coordinador de Operaciones y Comercial', 'Diane', 1),
(3, 'Supervisor Junior SST', 'Edu , Jazmin', 0),
(4, 'Asistente Comercial', '', 0),
(5, 'Analista de sistemas\r\n', '', 0),
(6, 'Supervisor de SST', 'Edu , Jazmin', 0),
(7, 'Asistente administrativo', '', 0),
(8, 'Monitorista', '', 0),
(9, 'Prevencionista', '', 0),
(10, 'Asistente de Operaciones y Comerciales', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_has_permisos`
--

CREATE TABLE `rol_has_permisos` (
  `rol_id` int(11) NOT NULL,
  `permisos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol_has_permisos`
--

INSERT INTO `rol_has_permisos` (`rol_id`, `permisos_id`) VALUES
(1, 1),
(5, 1),
(1, 2),
(2, 2),
(4, 2),
(5, 2),
(7, 2),
(10, 2),
(1, 3),
(2, 3),
(4, 3),
(5, 3),
(7, 3),
(10, 3),
(1, 4),
(2, 4),
(4, 4),
(5, 4),
(7, 4),
(10, 4),
(1, 5),
(2, 5),
(4, 5),
(5, 5),
(7, 5),
(10, 5),
(1, 6),
(2, 6),
(4, 6),
(5, 6),
(7, 6),
(10, 6),
(1, 7),
(2, 7),
(4, 7),
(5, 7),
(7, 7),
(10, 7),
(1, 8),
(2, 8),
(4, 8),
(5, 8),
(7, 8),
(10, 8),
(1, 9),
(2, 9),
(4, 9),
(5, 9),
(7, 9),
(10, 9),
(1, 10),
(2, 10),
(4, 10),
(5, 10),
(7, 10),
(10, 10),
(1, 11),
(2, 11),
(4, 11),
(5, 11),
(7, 11),
(10, 11),
(1, 12),
(2, 12),
(4, 12),
(5, 12),
(7, 12),
(10, 12),
(1, 13),
(2, 13),
(4, 13),
(5, 13),
(7, 13),
(10, 13),
(1, 14),
(2, 14),
(4, 14),
(5, 14),
(7, 14),
(10, 14),
(1, 15),
(2, 15),
(4, 15),
(5, 15),
(7, 15),
(10, 15),
(1, 16),
(2, 16),
(4, 16),
(5, 16),
(7, 16),
(10, 16),
(1, 17),
(2, 17),
(4, 17),
(5, 17),
(7, 17),
(10, 17),
(1, 18),
(2, 18),
(4, 18),
(5, 18),
(7, 18),
(10, 18),
(1, 19),
(2, 19),
(4, 19),
(5, 19),
(7, 19),
(10, 19),
(1, 20),
(2, 20),
(4, 20),
(5, 20),
(7, 20),
(10, 20),
(1, 21),
(2, 21),
(4, 21),
(5, 21),
(7, 21),
(10, 21),
(1, 22),
(2, 22),
(4, 22),
(5, 22),
(7, 22),
(10, 22),
(1, 23),
(2, 23),
(4, 23),
(5, 23),
(7, 23),
(10, 23),
(1, 24),
(2, 24),
(4, 24),
(5, 24),
(7, 24),
(10, 24),
(1, 25),
(2, 25),
(4, 25),
(5, 25),
(7, 25),
(10, 25),
(1, 26),
(2, 26),
(4, 26),
(5, 26),
(7, 26),
(10, 26),
(1, 27),
(5, 27),
(1, 28),
(5, 28),
(1, 29),
(5, 29),
(1, 30),
(2, 30),
(4, 30),
(5, 30),
(7, 30),
(10, 30),
(1, 31),
(2, 31),
(4, 31),
(5, 31),
(7, 31),
(10, 31),
(1, 32),
(2, 32),
(4, 32),
(5, 32),
(7, 32),
(10, 32),
(1, 33),
(2, 33),
(3, 33),
(4, 33),
(5, 33),
(6, 33),
(7, 33),
(8, 33),
(9, 33),
(10, 33),
(1, 34),
(2, 34),
(3, 34),
(4, 34),
(5, 34),
(6, 34),
(7, 34),
(8, 34),
(9, 34),
(10, 34),
(1, 35),
(2, 35),
(3, 35),
(4, 35),
(5, 35),
(6, 35),
(7, 35),
(8, 35),
(9, 35),
(10, 35),
(1, 36),
(2, 36),
(3, 36),
(4, 36),
(5, 36),
(6, 36),
(7, 36),
(8, 36),
(9, 36),
(10, 36),
(1, 37),
(2, 37),
(3, 37),
(4, 37),
(5, 37),
(6, 37),
(7, 37),
(8, 37),
(9, 37),
(10, 37),
(1, 38),
(2, 38),
(3, 38),
(4, 38),
(5, 38),
(6, 38),
(7, 38),
(8, 38),
(9, 38),
(10, 38),
(1, 39),
(2, 39),
(3, 39),
(4, 39),
(5, 39),
(6, 39),
(7, 39),
(8, 39),
(9, 39),
(10, 39),
(1, 40),
(2, 40),
(3, 40),
(4, 40),
(5, 40),
(6, 40),
(7, 40),
(8, 40),
(9, 40),
(10, 40),
(1, 41),
(2, 41),
(3, 41),
(4, 41),
(5, 41),
(6, 41),
(7, 41),
(8, 41),
(9, 41),
(10, 41),
(1, 42),
(2, 42),
(3, 42),
(4, 42),
(5, 42),
(6, 42),
(7, 42),
(8, 42),
(9, 42),
(10, 42),
(1, 43),
(2, 43),
(3, 43),
(4, 43),
(5, 43),
(6, 43),
(7, 43),
(8, 43),
(9, 43),
(10, 43),
(1, 44),
(2, 44),
(3, 44),
(4, 44),
(5, 44),
(6, 44),
(7, 44),
(8, 44),
(9, 44),
(10, 44),
(1, 45),
(2, 45),
(3, 45),
(4, 45),
(5, 45),
(6, 45),
(7, 45),
(9, 45),
(10, 45),
(1, 46),
(2, 46),
(3, 46),
(4, 46),
(5, 46),
(6, 46),
(7, 46),
(9, 46),
(10, 46),
(1, 47),
(2, 47),
(3, 47),
(4, 47),
(5, 47),
(6, 47),
(7, 47),
(9, 47),
(10, 47),
(1, 48),
(2, 48),
(3, 48),
(4, 48),
(5, 48),
(6, 48),
(7, 48),
(9, 48),
(10, 48),
(1, 49),
(2, 49),
(3, 49),
(4, 49),
(5, 49),
(7, 49),
(10, 49),
(1, 50),
(2, 50),
(3, 50),
(4, 50),
(5, 50),
(7, 50),
(10, 50),
(1, 51),
(2, 51),
(3, 51),
(4, 51),
(5, 51),
(7, 51),
(10, 51),
(1, 52),
(2, 52),
(3, 52),
(4, 52),
(5, 52),
(6, 52),
(7, 52),
(8, 52),
(9, 52),
(10, 52),
(1, 53),
(2, 53),
(3, 53),
(4, 53),
(5, 53),
(6, 53),
(7, 53),
(8, 53),
(9, 53),
(10, 53),
(1, 54),
(2, 54),
(3, 54),
(4, 54),
(5, 54),
(6, 54),
(7, 54),
(8, 54),
(9, 54),
(10, 54),
(1, 55),
(2, 55),
(3, 55),
(4, 55),
(5, 55),
(6, 55),
(7, 55),
(8, 55),
(9, 55),
(10, 55),
(1, 56),
(2, 56),
(3, 56),
(4, 56),
(5, 56),
(6, 56),
(7, 56),
(8, 56),
(9, 56),
(10, 56);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id` int(11) NOT NULL,
  `idservicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria_peligro`
--

CREATE TABLE `subcategoria_peligro` (
  `id` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategoria_peligro`
--

INSERT INTO `subcategoria_peligro` (`id`, `idcategoria`, `nombre`, `descripcion`) VALUES
(1, 1, 'Agentes infecciosos.', ''),
(2, 1, 'Agentes no infecciosos', ''),
(3, 2, 'Ruido', ''),
(4, 2, 'Iluminación', ''),
(5, 2, 'Temperatura', ''),
(6, 2, 'Vibraciones', ''),
(7, 2, 'Radiaciones (Ionizante y no ionizante).', ''),
(8, 3, 'Gases', ''),
(9, 3, 'Vapores', ''),
(10, 3, 'Líquidos', ''),
(11, 3, 'Polvos', ''),
(12, 3, 'Humos', ''),
(13, 3, 'Nieblas', ''),
(14, 4, 'Contacto directos', ''),
(15, 4, 'Contacto indirectos', ''),
(16, 5, 'Carga mental', ''),
(17, 5, 'Falta de apoyo y de calidad de liderazgo', ''),
(18, 5, 'escasas compensaciones', ''),
(19, 5, 'comunicación ineficaz', ''),
(20, 5, 'Otros', ''),
(21, 6, 'Levantamiento manual de cargas', ''),
(22, 6, 'Transporte manual de cargas', ''),
(23, 6, 'Empuje o tracción manual de cargas', ''),
(24, 6, 'Uso intensivo de las extremidades superiores', ''),
(25, 6, 'Uso de ordenador', ''),
(26, 6, 'Otros', ''),
(27, 7, 'Aplastamiento', ''),
(28, 7, 'Corte', ''),
(29, 7, 'Enganche', ''),
(30, 7, 'Atrapamiento', ''),
(31, 7, 'Otros', ''),
(32, 8, 'Distribución de espacios', ''),
(33, 8, 'Pisos/suelos', ''),
(34, 8, 'Techos o cubiertas', ''),
(35, 8, 'Distribución de máquinas y equipos', ''),
(36, 8, 'Escaleras y rampas', ''),
(37, 8, 'Puertas', ''),
(38, 8, 'La señalización', ''),
(39, 8, 'Otros', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `id` int(11) NOT NULL,
  `idagente` int(11) NOT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `nombres_apellidos` text,
  `puesto_trabajo` text,
  `area_monitoreada` text,
  `instalacion` text,
  `descripcion_instalacion` text,
  `calle_numero` text,
  `distrito` text,
  `area_geografica` text,
  `region_geografica` text,
  `estado` bigint(5) DEFAULT NULL,
  `usercreated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`id`, `idagente`, `dni`, `nombres_apellidos`, `puesto_trabajo`, `area_monitoreada`, `instalacion`, `descripcion_instalacion`, `calle_numero`, `distrito`, `area_geografica`, `region_geografica`, `estado`, `usercreated`) VALUES
(2, 42, '12345678', 'dd', 'fff', 'dd', 'sxsxza', 'qqq', 'errr', 'ttt', 'ttt', 'hhh', 0, 1),
(3, 42, '87654321', 'ee', 'vv', 'cc', 'sxss', 'www', 'eee', 'ggg', 'hhhy', 'hnnn', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubigeo`
--

CREATE TABLE `ubigeo` (
  `id` int(11) NOT NULL,
  `iddepartamento` int(11) NOT NULL,
  `idprovincia` int(11) NOT NULL,
  `iddistrito` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ubigeo`
--

INSERT INTO `ubigeo` (`id`, `iddepartamento`, `idprovincia`, `iddistrito`) VALUES
(2, 14, 135, 1372),
(3, 14, 135, 1377);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` text NOT NULL,
  `estado` bigint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `password`, `estado`) VALUES
(1, 'jose', '$2y$10$VQXhZGOKRE5VdJSFzjtWTO464GM7RnbUiaJ5gsonsCbVSdoPfmYcW', 0),
(2, '45551874', '$2y$10$IoKUh.0gErXogXwi8E01bet826yzU87zfYtQNfYkMIDaZSZdAar/q', 0),
(3, '76586843', '$2y$10$fl1F4zgM9Uk5r/JF36lSaedUXfXf1sUaT4yG9moVXYZMZRAsI9H/W', 0),
(4, '77044827', '$2y$10$huaCSjmqLv.eCEIUAWMvPu.Xh4Z5IwZWGTf4w4u8Pjbc6PsZoSnzW', 0),
(5, '45630734', '$2y$10$80cj2ztVXsZq04iX1mdkxutx5hKo9zNJhcxOiTeBVTgoq7wwthYxS', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_asigna_agente`
--

CREATE TABLE `usuario_asigna_agente` (
  `usuario_id` int(11) NOT NULL,
  `ms_tagente_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_has_rol`
--

CREATE TABLE `usuario_has_rol` (
  `usuario_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_has_rol`
--

INSERT INTO `usuario_has_rol` (`usuario_id`, `rol_id`) VALUES
(1, 1),
(2, 2),
(4, 3),
(5, 5),
(3, 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agente`
--
ALTER TABLE `agente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agente_proyecto` (`idproyecto`);

--
-- Indices de la tabla `asignacion_proyecto`
--
ALTER TABLE `asignacion_proyecto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asignacion_proyecto` (`idproyecto`);

--
-- Indices de la tabla `asignacion_responsable_equipo`
--
ALTER TABLE `asignacion_responsable_equipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asignacion_proyecto` (`idasignacion_proyecto`);

--
-- Indices de la tabla `calendario`
--
ALTER TABLE `calendario`
  ADD PRIMARY KEY (`idcalendario`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_peligro`
--
ALTER TABLE `categoria_peligro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `covid`
--
ALTER TABLE `covid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `covid_usuario` (`idusuario`),
  ADD KEY `covid_proyecto` (`idproyecto`);

--
-- Indices de la tabla `cuestionario`
--
ALTER TABLE `cuestionario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_ergonomia`
--
ALTER TABLE `detalle_ergonomia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iddet_ergonomia` (`iddet_ergonomia`);

--
-- Indices de la tabla `det_bio_hisopo`
--
ALTER TABLE `det_bio_hisopo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bio_proyecto` (`idproyecto`),
  ADD KEY `bio_instalacion` (`idinstalacion`);

--
-- Indices de la tabla `det_bio_tipo`
--
ALTER TABLE `det_bio_tipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bio_monitoreo` (`idmonitoreo`),
  ADD KEY `bio_persona1` (`idpersona1`),
  ADD KEY `bio_cargo1` (`idcargo1`),
  ADD KEY `bio_persona2` (`idpersona2`),
  ADD KEY `bio_cargo2` (`idcargo2`),
  ADD KEY `bio_persona3` (`idpersona3`),
  ADD KEY `bio_cargo3` (`idcargo3`);

--
-- Indices de la tabla `det_cuest_opcion`
--
ALTER TABLE `det_cuest_opcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `det_cuest_opcion_cuestionario` (`idcuestionario`);

--
-- Indices de la tabla `det_ergonomia`
--
ALTER TABLE `det_ergonomia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ergominia_trabajador` (`idtrabajador`),
  ADD KEY `ergonomia_proyecto` (`idproyecto`);

--
-- Indices de la tabla `det_iluminacion`
--
ALTER TABLE `det_iluminacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `det_iluminacion_proyecto` (`idproyecto`),
  ADD KEY `det_iluminacion_trabajador` (`idtrabajador`);

--
-- Indices de la tabla `det_psicosocial`
--
ALTER TABLE `det_psicosocial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `psicosocial_monitoreo` (`idmonitoreo`),
  ADD KEY `psicosocial_persona` (`idpersona1`),
  ADD KEY `psicosocial_cargo` (`idcargo1`),
  ADD KEY `psicosocial_persona1` (`idpersona2`),
  ADD KEY `psicosocial_cargo1` (`idcargo2`),
  ADD KEY `psicosocial_cuestion_opcion` (`idcuest_opcion`);

--
-- Indices de la tabla `det_ruido`
--
ALTER TABLE `det_ruido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `det_ruido_proyecto` (`idproyecto`),
  ADD KEY `det_ruido_instalacion` (`idinstalacion`) USING BTREE;

--
-- Indices de la tabla `det_temperatura`
--
ALTER TABLE `det_temperatura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `det_temperatura` (`idmonitoreo`),
  ADD KEY `temperatura_persona1` (`idpersona1`),
  ADD KEY `temperatura_persona2` (`idpersona2`),
  ADD KEY `temperatura_persona3` (`idpersona3`),
  ADD KEY `temperatura_cargo1` (`idcargo1`),
  ADD KEY `temperatura_cargo2` (`idcargo2`),
  ADD KEY `temperatura_cargo3` (`idcargo3`);

--
-- Indices de la tabla `distrito`
--
ALTER TABLE `distrito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `distrito_provincia` (`idProv`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_ubigeo` (`idubigeo`);

--
-- Indices de la tabla `empresa_contacto`
--
ALTER TABLE `empresa_contacto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idempresa` (`idempresa`);

--
-- Indices de la tabla `encuestacovid`
--
ALTER TABLE `encuestacovid`
  ADD PRIMARY KEY (`idencuesta`);

--
-- Indices de la tabla `epp`
--
ALTER TABLE `epp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `epp_equipos`
--
ALTER TABLE `epp_equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eppequipo_proyecto` (`idagente`);

--
-- Indices de la tabla `epp_trabajador`
--
ALTER TABLE `epp_trabajador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `epp_trabajador_equipos`
--
ALTER TABLE `epp_trabajador_equipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equipos_detalle`
--
ALTER TABLE `equipos_detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `equipo_detalle` (`idequipo`),
  ADD KEY `persona_equipo_detalle` (`idpersona`);

--
-- Indices de la tabla `hproyectoempresa`
--
ALTER TABLE `hproyectoempresa`
  ADD PRIMARY KEY (`id`,`year`,`mes`),
  ADD KEY `fk_hproyectoempresa_empresa1_idx` (`idempresa`);

--
-- Indices de la tabla `inspecciones`
--
ALTER TABLE `inspecciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inspecciones_persona` (`idpersona`),
  ADD KEY `inspecciones_categoria` (`idcategoria_peligro`),
  ADD KEY `inspecciones_subcategoria` (`idsubcategoria_peligro`),
  ADD KEY `inspecciones_proyecto` (`idproyecto`);

--
-- Indices de la tabla `instalacion`
--
ALTER TABLE `instalacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `instalacion_proyecto` (`idproyecto`);

--
-- Indices de la tabla `monitoreo`
--
ALTER TABLE `monitoreo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proyecto_equipo` (`idequipo`) USING BTREE,
  ADD KEY `proyecto_servicio` (`idservicio`);

--
-- Indices de la tabla `ms_tagente`
--
ALTER TABLE `ms_tagente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ms_tagente_ms_tmonitoreo1_idx` (`idmonitoreo`);

--
-- Indices de la tabla `ms_tmonitoreo`
--
ALTER TABLE `ms_tmonitoreo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ms_tmonitoreo_ms_tservicio1_idx` (`idservicio`);

--
-- Indices de la tabla `ms_tservicio`
--
ALTER TABLE `ms_tservicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `opcion`
--
ALTER TABLE `opcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisos_usuario`
--
ALTER TABLE `permisos_usuario`
  ADD PRIMARY KEY (`permisos_id`,`usuario_id`),
  ADD KEY `fk_permisos_has_usuario_usuario1_idx` (`usuario_id`),
  ADD KEY `fk_permisos_has_usuario_permisos1_idx` (`permisos_id`);

--
-- Indices de la tabla `permiso_establecido_agente`
--
ALTER TABLE `permiso_establecido_agente`
  ADD PRIMARY KEY (`permisos_id`,`ms_tagente_id`),
  ADD KEY `fk_permisos_has_ms_tagente_ms_tagente1_idx` (`ms_tagente_id`),
  ADD KEY `fk_permisos_has_ms_tagente_permisos1_idx` (`permisos_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `persona_usuario` (`idusuario`),
  ADD KEY `persona_ubigeo` (`idubigeo`),
  ADD KEY `persona_empresa` (`idempresa`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provincia_depa` (`idDepa`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proyecto_empresa` (`idempresa`);

--
-- Indices de la tabla `proyecto_detalle`
--
ALTER TABLE `proyecto_detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalle_proyecto` (`idproyecto`),
  ADD KEY `detalle_persona` (`idpersona`);

--
-- Indices de la tabla `reporte_proyecto_requisitos`
--
ALTER TABLE `reporte_proyecto_requisitos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `requerimiento_proyecto`
--
ALTER TABLE `requerimiento_proyecto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `respuestacovid`
--
ALTER TABLE `respuestacovid`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `respuestacovid_filter`
--
ALTER TABLE `respuestacovid_filter`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol_has_permisos`
--
ALTER TABLE `rol_has_permisos`
  ADD PRIMARY KEY (`rol_id`,`permisos_id`),
  ADD KEY `fk_rol_has_permisos_permisos1_idx` (`permisos_id`),
  ADD KEY `fk_rol_has_permisos_rol1_idx` (`rol_id`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subcategoria_peligro`
--
ALTER TABLE `subcategoria_peligro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategoria_categoria` (`idcategoria`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ubigeo`
--
ALTER TABLE `ubigeo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ubigeo_depar` (`iddepartamento`),
  ADD KEY `ubigeo_provincia` (`idprovincia`),
  ADD KEY `ubigeo_distrito` (`iddistrito`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario_asigna_agente`
--
ALTER TABLE `usuario_asigna_agente`
  ADD PRIMARY KEY (`usuario_id`,`ms_tagente_id`),
  ADD KEY `fk_usuario_has_ms_tagente_ms_tagente1_idx` (`ms_tagente_id`),
  ADD KEY `fk_usuario_has_ms_tagente_usuario1_idx` (`usuario_id`);

--
-- Indices de la tabla `usuario_has_rol`
--
ALTER TABLE `usuario_has_rol`
  ADD PRIMARY KEY (`usuario_id`,`rol_id`),
  ADD KEY `fk_usuario_has_rol_rol1_idx` (`rol_id`),
  ADD KEY `fk_usuario_has_rol_usuario1_idx` (`usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agente`
--
ALTER TABLE `agente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `asignacion_proyecto`
--
ALTER TABLE `asignacion_proyecto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `asignacion_responsable_equipo`
--
ALTER TABLE `asignacion_responsable_equipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `calendario`
--
ALTER TABLE `calendario`
  MODIFY `idcalendario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `categoria_peligro`
--
ALTER TABLE `categoria_peligro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `covid`
--
ALTER TABLE `covid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuestionario`
--
ALTER TABLE `cuestionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_ergonomia`
--
ALTER TABLE `detalle_ergonomia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `det_bio_hisopo`
--
ALTER TABLE `det_bio_hisopo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `det_bio_tipo`
--
ALTER TABLE `det_bio_tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `det_cuest_opcion`
--
ALTER TABLE `det_cuest_opcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `det_ergonomia`
--
ALTER TABLE `det_ergonomia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `det_iluminacion`
--
ALTER TABLE `det_iluminacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `det_psicosocial`
--
ALTER TABLE `det_psicosocial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `det_ruido`
--
ALTER TABLE `det_ruido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `empresa_contacto`
--
ALTER TABLE `empresa_contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `encuestacovid`
--
ALTER TABLE `encuestacovid`
  MODIFY `idencuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `epp`
--
ALTER TABLE `epp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `epp_equipos`
--
ALTER TABLE `epp_equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `epp_trabajador`
--
ALTER TABLE `epp_trabajador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT de la tabla `epp_trabajador_equipos`
--
ALTER TABLE `epp_trabajador_equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `equipos_detalle`
--
ALTER TABLE `equipos_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `hproyectoempresa`
--
ALTER TABLE `hproyectoempresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `inspecciones`
--
ALTER TABLE `inspecciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `instalacion`
--
ALTER TABLE `instalacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `monitoreo`
--
ALTER TABLE `monitoreo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ms_tagente`
--
ALTER TABLE `ms_tagente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `ms_tmonitoreo`
--
ALTER TABLE `ms_tmonitoreo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `ms_tservicio`
--
ALTER TABLE `ms_tservicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `opcion`
--
ALTER TABLE `opcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `proyecto_detalle`
--
ALTER TABLE `proyecto_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reporte_proyecto_requisitos`
--
ALTER TABLE `reporte_proyecto_requisitos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `requerimiento_proyecto`
--
ALTER TABLE `requerimiento_proyecto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `respuestacovid`
--
ALTER TABLE `respuestacovid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `respuestacovid_filter`
--
ALTER TABLE `respuestacovid_filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `subcategoria_peligro`
--
ALTER TABLE `subcategoria_peligro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ubigeo`
--
ALTER TABLE `ubigeo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `agente`
--
ALTER TABLE `agente`
  ADD CONSTRAINT `agente_proyecto` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`);

--
-- Filtros para la tabla `covid`
--
ALTER TABLE `covid`
  ADD CONSTRAINT `covid_proyecto` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`),
  ADD CONSTRAINT `covid_usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `detalle_ergonomia`
--
ALTER TABLE `detalle_ergonomia`
  ADD CONSTRAINT `detalle_ergonomia_ibfk_1` FOREIGN KEY (`iddet_ergonomia`) REFERENCES `det_ergonomia` (`id`);

--
-- Filtros para la tabla `det_bio_hisopo`
--
ALTER TABLE `det_bio_hisopo`
  ADD CONSTRAINT `bio_instalacion` FOREIGN KEY (`idinstalacion`) REFERENCES `instalacion` (`id`),
  ADD CONSTRAINT `bio_proyecto` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`);

--
-- Filtros para la tabla `det_bio_tipo`
--
ALTER TABLE `det_bio_tipo`
  ADD CONSTRAINT `bio_cargo1` FOREIGN KEY (`idcargo1`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `bio_cargo2` FOREIGN KEY (`idcargo2`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `bio_cargo3` FOREIGN KEY (`idcargo3`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `bio_monitoreo` FOREIGN KEY (`idmonitoreo`) REFERENCES `monitoreo` (`id`),
  ADD CONSTRAINT `bio_persona1` FOREIGN KEY (`idpersona1`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `bio_persona2` FOREIGN KEY (`idpersona2`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `bio_persona3` FOREIGN KEY (`idpersona3`) REFERENCES `persona` (`id`);

--
-- Filtros para la tabla `det_cuest_opcion`
--
ALTER TABLE `det_cuest_opcion`
  ADD CONSTRAINT `det_cuest_opcion_cuestionario` FOREIGN KEY (`idcuestionario`) REFERENCES `cuestionario` (`id`);

--
-- Filtros para la tabla `det_ergonomia`
--
ALTER TABLE `det_ergonomia`
  ADD CONSTRAINT `ergominia_trabajador` FOREIGN KEY (`idtrabajador`) REFERENCES `trabajador` (`id`),
  ADD CONSTRAINT `ergonomia_proyecto` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`);

--
-- Filtros para la tabla `det_iluminacion`
--
ALTER TABLE `det_iluminacion`
  ADD CONSTRAINT `det_iluminacion_proyecto` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`),
  ADD CONSTRAINT `det_iluminacion_trabajador` FOREIGN KEY (`idtrabajador`) REFERENCES `trabajador` (`id`);

--
-- Filtros para la tabla `det_psicosocial`
--
ALTER TABLE `det_psicosocial`
  ADD CONSTRAINT `psicosocial_cargo` FOREIGN KEY (`idcargo1`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `psicosocial_cargo1` FOREIGN KEY (`idcargo2`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `psicosocial_cuestion_opcion` FOREIGN KEY (`idcuest_opcion`) REFERENCES `cuestionario` (`id`),
  ADD CONSTRAINT `psicosocial_monitoreo` FOREIGN KEY (`idmonitoreo`) REFERENCES `monitoreo` (`id`),
  ADD CONSTRAINT `psicosocial_persona` FOREIGN KEY (`idpersona1`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `psicosocial_persona1` FOREIGN KEY (`idpersona2`) REFERENCES `persona` (`id`);

--
-- Filtros para la tabla `det_ruido`
--
ALTER TABLE `det_ruido`
  ADD CONSTRAINT `det_ruido_instalacion` FOREIGN KEY (`idinstalacion`) REFERENCES `instalacion` (`id`),
  ADD CONSTRAINT `det_ruido_proyecto` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`);

--
-- Filtros para la tabla `det_temperatura`
--
ALTER TABLE `det_temperatura`
  ADD CONSTRAINT `det_temperatura_monitoreo` FOREIGN KEY (`idmonitoreo`) REFERENCES `monitoreo` (`id`),
  ADD CONSTRAINT `temperatura_cargo1` FOREIGN KEY (`idcargo1`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `temperatura_cargo2` FOREIGN KEY (`idcargo2`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `temperatura_cargo3` FOREIGN KEY (`idcargo3`) REFERENCES `cargo` (`id`),
  ADD CONSTRAINT `temperatura_persona1` FOREIGN KEY (`idpersona1`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `temperatura_persona2` FOREIGN KEY (`idpersona2`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `temperatura_persona3` FOREIGN KEY (`idpersona3`) REFERENCES `persona` (`id`);

--
-- Filtros para la tabla `distrito`
--
ALTER TABLE `distrito`
  ADD CONSTRAINT `distrito_provincia` FOREIGN KEY (`idProv`) REFERENCES `provincia` (`id`);

--
-- Filtros para la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `empresa_ubigeo` FOREIGN KEY (`idubigeo`) REFERENCES `ubigeo` (`id`);

--
-- Filtros para la tabla `empresa_contacto`
--
ALTER TABLE `empresa_contacto`
  ADD CONSTRAINT `idempresa` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`id`);

--
-- Filtros para la tabla `equipos_detalle`
--
ALTER TABLE `equipos_detalle`
  ADD CONSTRAINT `equipo_detalle` FOREIGN KEY (`idequipo`) REFERENCES `equipos` (`id`),
  ADD CONSTRAINT `persona_equipo_detalle` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`id`);

--
-- Filtros para la tabla `hproyectoempresa`
--
ALTER TABLE `hproyectoempresa`
  ADD CONSTRAINT `fk_hproyectoempresa_empresa1` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inspecciones`
--
ALTER TABLE `inspecciones`
  ADD CONSTRAINT `inspecciones_categoria` FOREIGN KEY (`idcategoria_peligro`) REFERENCES `categoria_peligro` (`id`),
  ADD CONSTRAINT `inspecciones_persona` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `inspecciones_proyecto` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`),
  ADD CONSTRAINT `inspecciones_subcategoria	` FOREIGN KEY (`idsubcategoria_peligro`) REFERENCES `subcategoria_peligro` (`id`);

--
-- Filtros para la tabla `instalacion`
--
ALTER TABLE `instalacion`
  ADD CONSTRAINT `instalacion_proyecto` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`);

--
-- Filtros para la tabla `monitoreo`
--
ALTER TABLE `monitoreo`
  ADD CONSTRAINT `proyecto_equipo` FOREIGN KEY (`idequipo`) REFERENCES `equipos` (`id`),
  ADD CONSTRAINT `proyecto_servicio` FOREIGN KEY (`idservicio`) REFERENCES `servicio` (`id`);

--
-- Filtros para la tabla `ms_tagente`
--
ALTER TABLE `ms_tagente`
  ADD CONSTRAINT `fk_ms_tagente_ms_tmonitoreo1` FOREIGN KEY (`idmonitoreo`) REFERENCES `ms_tmonitoreo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ms_tmonitoreo`
--
ALTER TABLE `ms_tmonitoreo`
  ADD CONSTRAINT `fk_ms_tmonitoreo_ms_tservicio1` FOREIGN KEY (`idservicio`) REFERENCES `ms_tservicio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `permisos_usuario`
--
ALTER TABLE `permisos_usuario`
  ADD CONSTRAINT `fk_permisos_has_usuario_permisos1` FOREIGN KEY (`permisos_id`) REFERENCES `permisos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_permisos_has_usuario_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `permiso_establecido_agente`
--
ALTER TABLE `permiso_establecido_agente`
  ADD CONSTRAINT `fk_permisos_has_ms_tagente_ms_tagente1` FOREIGN KEY (`ms_tagente_id`) REFERENCES `ms_tagente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_permisos_has_ms_tagente_permisos1` FOREIGN KEY (`permisos_id`) REFERENCES `permisos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_empresa` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`id`),
  ADD CONSTRAINT `persona_ubigeo` FOREIGN KEY (`idubigeo`) REFERENCES `ubigeo` (`id`),
  ADD CONSTRAINT `persona_usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD CONSTRAINT `provincia_depa` FOREIGN KEY (`idDepa`) REFERENCES `departamento` (`id`);

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `proyecto_empresa` FOREIGN KEY (`idempresa`) REFERENCES `empresa` (`id`);

--
-- Filtros para la tabla `proyecto_detalle`
--
ALTER TABLE `proyecto_detalle`
  ADD CONSTRAINT `detalle_persona` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `detalle_proyecto	` FOREIGN KEY (`idproyecto`) REFERENCES `proyecto` (`id`);

--
-- Filtros para la tabla `rol_has_permisos`
--
ALTER TABLE `rol_has_permisos`
  ADD CONSTRAINT `fk_rol_has_permisos_permisos1` FOREIGN KEY (`permisos_id`) REFERENCES `permisos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rol_has_permisos_rol1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `subcategoria_peligro`
--
ALTER TABLE `subcategoria_peligro`
  ADD CONSTRAINT `subcategoria_categoria` FOREIGN KEY (`idcategoria`) REFERENCES `categoria_peligro` (`id`);

--
-- Filtros para la tabla `ubigeo`
--
ALTER TABLE `ubigeo`
  ADD CONSTRAINT `ubigeo_depar` FOREIGN KEY (`iddepartamento`) REFERENCES `departamento` (`id`),
  ADD CONSTRAINT `ubigeo_distrito` FOREIGN KEY (`iddistrito`) REFERENCES `distrito` (`id`),
  ADD CONSTRAINT `ubigeo_provincia` FOREIGN KEY (`idprovincia`) REFERENCES `provincia` (`id`);

--
-- Filtros para la tabla `usuario_asigna_agente`
--
ALTER TABLE `usuario_asigna_agente`
  ADD CONSTRAINT `fk_usuario_has_ms_tagente_ms_tagente1` FOREIGN KEY (`ms_tagente_id`) REFERENCES `ms_tagente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_ms_tagente_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario_has_rol`
--
ALTER TABLE `usuario_has_rol`
  ADD CONSTRAINT `fk_usuario_has_rol_rol1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_has_rol_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`root`@`localhost` EVENT `calibracion_equipo` ON SCHEDULE EVERY 1 DAY STARTS '2020-12-15 16:15:52' ON COMPLETION NOT PRESERVE ENABLE DO CALL verificar_calibracion_equipo()$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
