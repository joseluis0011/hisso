import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

function verificarAcceso(to,from,next) {
    let authUser =JSON.parse(localStorage.getItem('authUser'));
    if (authUser){
        let listRolPermisosByUsuario =JSON.parse(localStorage.getItem('listRolPermisosByUsuario'));
        if (listRolPermisosByUsuario.includes(to.name)){
            next();
        }else {
            let listaRolPermisosByUsuarioFilter =[];
            listRolPermisosByUsuario.map( x =>{
                if (x.includes('dashboard')){
                    listaRolPermisosByUsuarioFilter.push(x);
                }
            });
            if (to.name == 'dashboard.index'){
                next({name:listaRolPermisosByUsuarioFilter[0]});
            }else{
                next(from.path);
            }
        }
    }else {
        next('/login');
    }
}
export const rutas =[
    {
        path:'/login',
        name:'login',
        component:require('./components/modulos/authenticate/login').default,
        beforeEnter: (to, from, next) => {
            let authUser = JSON.parse(localStorage.getItem('authUser'));
            if (authUser) {
                next({ name: 'dashboard.index' });
            } else {
                next();
            }
        }
    },
    {
        path:'/',
        name:'dashboard.index',
        component:require('./components/modulos/monitoreo/dashboard').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/empresa',
        name:'empresa.index',
        component:require('./components/modulos/monitoreo/empresa/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/empresa/agregar',
        name:'empresa.agregar',
        component:require('./components/modulos/monitoreo/empresa/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/empresa/editar',
        name:'empresa.editar',
        component:require('./components/modulos/monitoreo/empresa/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/empresa/dashboard',
        name:'empresa.dashboard',
        component:require('./components/modulos/monitoreo/empresa/dashboard/index').default,
        props:true,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/medicion',
        name:'medicion.index',
        component:require('./components/modulos/monitoreo/medicion/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/medicion/agregar',
        name:'medicion.agregar',
        component:require('./components/modulos/monitoreo/medicion/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/medicion/editar',
        name:'medicion.editar',
        component:require('./components/modulos/monitoreo/medicion/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/medicion/detalle',
        name:'medicion.detalle',
        component:require('./components/modulos/monitoreo/medicion/detalle').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/medicion/dashboard',
        name:'medicion.dashboard',
        component:require('./components/modulos/monitoreo/medicion/dashboard/index').default,
        props:true,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/personal',
        name:'personal.index',
        component:require('./components/modulos/monitoreo/personal/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/personal/agregar',
        name:'personal.agregar',
        component:require('./components/modulos/monitoreo/personal/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/personal/editar',
        name:'personal.editar',
        component:require('./components/modulos/monitoreo/personal/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador',
        name:'trabajador.index',
        component:require('./components/modulos/monitoreo/trabajador/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador/agregarTrabajador',
        name:'trabajador.agregarTrabajador',
        component:require('./components/modulos/monitoreo/trabajador/addtrabajador').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador/agregarInstalacion',
        name:'trabajador.agregarInstalacion',
        component:require('./components/modulos/monitoreo/trabajador/addinstalacion').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador/ver/:id/:estado',
        name:'trabajador.ver',
        props:true,
        component:require('./components/modulos/monitoreo/trabajador/ver').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador/trabajador/agregar/:id/:estado',
        name:'trabajador.trabajador.agregar',
        props:true,
        component:require('./components/modulos/monitoreo/trabajador/trabajador/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador/trabajador/editar/:idproyecto/:estado/:id',
        name:'trabajador.trabajador.editar',
        props:true,
        component:require('./components/modulos/monitoreo/trabajador/trabajador/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador/instalacion/agregar/:id/:estado',
        name:'trabajador.instalacion.agregar',
        props:true,
        component:require('./components/modulos/monitoreo/trabajador/instalacion/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador/instalacion/editar/:idproyecto/:estado/:id',
        name:'trabajador.instalacion.editar',
        props:true,
        component:require('./components/modulos/monitoreo/trabajador/instalacion/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/proyecto',
        name:'proyecto.index',
        component:require('./components/modulos/monitoreo/proyecto/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/proyecto/agregar',
        name:'proyecto.agregar',
        component:require('./components/modulos/monitoreo/proyecto/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/proyecto/editar',
        name:'proyecto.editar',
        component:require('./components/modulos/monitoreo/proyecto/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/proyecto/ver/:id',
        name:'proyecto.ver',
        props:true,
        component:require('./components/modulos/monitoreo/proyecto/ver').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/proyecto/dashboard',
        name:'proyecto.dashboard',
        component:require('./components/modulos/monitoreo/proyecto/dashboard/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/asignacion',
        name:'asignacion.index',
        component:require('./components/modulos/monitoreo/asignacion/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/asignacion/crear',
        name:'asignacion.agregar',
        component:require('./components/modulos/monitoreo/asignacion/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/asignacion/edit',
        name:'asignacion.editar',
        component:require('./components/modulos/monitoreo/asignacion/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/usuario',
        name:'usuario.index',
        component:require('./components/modulos/monitoreo/usuario/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/usuario/agregar',
        name:'usuario.agregar',
        component:require('./components/modulos/monitoreo/usuario/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/usuario/editar/:id/:idus',
        name:'usuario.editar',
        props:true,
        component:require('./components/modulos/monitoreo/usuario/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/iluminacion',
        name:'iluminacion.index',
        component:require('./components/modulos/monitoreo/iluminacion/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/iluminacion/agregar/:idtrabajador/:idproyecto',
        name:'iluminacion.agregar',
        props:true,
        component:require('./components/modulos/monitoreo/iluminacion/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/iluminacion/editar/:idtrabajador/:idproyecto',
        name:'iluminacion.editar',
        props:true,
        component:require('./components/modulos/monitoreo/iluminacion/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/ruido',
        name:'ruido.index',
        component:require('./components/modulos/monitoreo/ruido/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/ruido/agregar/:idinstalacion/:idproyecto',
        name:'ruido.agregar',
        props:true,
        component:require('./components/modulos/monitoreo/ruido/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/ruido/editar/:idinstalacion/:idproyecto',
        name:'ruido.editar',
        props:true,
        component:require('./components/modulos/monitoreo/ruido/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/ergonomia',
        name:'ergonomia.index',
        component:require('./components/modulos/monitoreo/ergonomia/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/ergonomia/agregar/:idtrabajador/:idproyecto',
        name:'ergonomia.agregar',
        props:true,
        component:require('./components/modulos/monitoreo/ergonomia/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/ergonomia/editar/:idtrabajador/:idproyecto',
        name:'ergonomia.editar',
        props:true,
        component:require('./components/modulos/monitoreo/ergonomia/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/biologico',
        name:'biologico.index',
        component:require('./components/modulos/monitoreo/biologico/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/biologico/agregar/:idinstalacion/:idproyecto',
        name:'biologico.agregar',
        props:true,
        component:require('./components/modulos/monitoreo/biologico/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/biologico/editar/:idinstalacion/:idproyecto',
        name:'biologico.editar',
        props:true,
        component:require('./components/modulos/monitoreo/biologico/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/inspecciones',
        name:'inspecciones.index',
        component:require('./components/modulos/monitoreo/inspecciones/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/inspecciones/agregar',
        name:'inspecciones.agregar',
        props:true,
        component:require('./components/modulos/monitoreo/inspecciones/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/inspecciones/editar/:id',
        name:'inspecciones.editar',
        props:true,
        component:require('./components/modulos/monitoreo/inspecciones/editar').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/inspecciones/ver/:id',
        name:'inspecciones.ver',
        props:true,
        component:require('./components/modulos/monitoreo/inspecciones/ver').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/covid',
        name:'covid.index',
        component:require('./components/modulos/monitoreo/encuesta/covid/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/covid/agregar',
        name:'covid.agregar',
        component:require('./components/modulos/monitoreo/encuesta/covid/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/covid/meses',
        name:'covid.meses',
        component:require('./components/modulos/monitoreo/encuesta/covid/meses').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/trabajador/encuesta/covid',
        name:'trabajador.dashboard.index',
        component:require('./components/modulos/trabajador/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/empresa/trabajadores',
        name:'empresa.dashboard.index',
        component:require('./components/modulos/empresa/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/epp',
        name:'epp.index',
        component:require('./components/modulos/monitoreo/epp/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/epp/agregar',
        name:'epp.agregar',
        component:require('./components/modulos/monitoreo/epp/asignar/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/epp/trabajador',
        name:'epp.trabajador',
        component:require('./components/modulos/monitoreo/epp/asignar/trabajador').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    // equipos para epp
    {
        path:'/equipos/epp',
        name:'equiposepp.index',
        component:require('./components/modulos/monitoreo/epp/equipos/index').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
    {
        path:'/equipos/epp/agregar',
        name:'equiposepp.agregar',
        component:require('./components/modulos/monitoreo/epp/equipos/add').default,
        beforeEnter: (to,from,next) =>{
            verificarAcceso(to,from,next)
        }
    },
];

export default new Router({
    routes:rutas,
    mode:'history',
    linkActiveClass:'active'
})
