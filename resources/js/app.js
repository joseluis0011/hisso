require('./bootstrap');

window.Vue = require('vue');

import router from "./routes";
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/es'
import '../sass/element-variables.scss';
Vue.use(ElementUI, { locale },{size:'small'});
export const EventBus= new Vue();
window.EventBus = EventBus;
import VTooltip from 'v-tooltip';
Vue.use(VTooltip);
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css' //Vuesax styles
Vue.use(Vuesax, {
    // options here
});
Vue.prototype.$eventHub = new Vue();
import moment from 'moment';
moment.locale('es');
window.moment = moment;
Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY')
    }
});
Vue.component('app',require('./components/App.vue').default);
Vue.component('Auth',require('./components/Auth.vue').default);

const app =new Vue({
    el:'#app',
    router
});
