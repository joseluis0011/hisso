
<html>
<head>
    <title>Pase Laboral</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        html {
            font-family: sans-serif;
            font-size: 12px;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .font-xsm {
            font-size: 10px;
        }
        .font-sm {
            font-size: 12px;
        }
        .font-lg {
            font-size: 13px;
        }
        .font-xlg {
            font-size: 16px;
        }
        .font-xxlg {
            font-size: 22px;
        }
        .font-bold {
            font-weight: bold;
        }
        table {
            width: 100%;
            border-spacing: 0;
        }
        .voucher-company-right {
            border: 1px solid #333;
            padding-top: 15px;
            padding-bottom: 15px;
            margin-bottom: 10px;
        }
        .voucher-company-right tbody tr:first-child td {
            padding-top: 10px;
        }
        .voucher-company-right tbody tr:last-child td {
            padding-bottom: 10px;
        }
        .voucher-information {
            border: 1px solid #333;
        }
        .voucher-information.top-note, .voucher-information.top-note tbody tr td {
            border-top: 0;
        }
        .voucher-information tbody tr td {
            padding-top: 5px;
            padding-bottom: 5px;
            vertical-align: top;
        }
        .voucher-information-left tbody tr td {
            padding: 3px 10px;
            vertical-align: top;
        }
        .voucher-information-right tbody tr td {
            padding: 3px 10px;
            vertical-align: top;
        }
        .voucher-details {
        }
        .voucher-details thead {
            background-color: #f5f5f5;
        }
        .voucher-details thead tr th {
            /*border-top: 1px solid #333;*/
            /*border-bottom: 1px solid #333;*/
            padding: 5px 10px;
        }
        .voucher-details thead tr th:first-child {
            border-left: 1px solid #333;
        }
        .voucher-details thead tr th:last-child {
            border-right: 1px solid #333;
        }
        .voucher-details tbody tr td {
            /*border-bottom: 1px solid #333;*/
        }
        .voucher-details tbody tr td:first-child {
            border-left: 1px solid #333;
        }
        .voucher-details tbody tr td:last-child {
            border-right: 1px solid #333;
        }
        .voucher-details tbody tr td {
            padding: 5px 10px;
            vertical-align: middle;
        }
        .voucher-details tfoot tr td {
            padding: 3px 10px;
        }
        .voucher-totals {
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .voucher-totals tbody tr td {
            padding: 3px 10px;
            vertical-align: top;
        }
        .voucher-footer {
            margin-bottom: 30px;
        }
        .voucher-footer tbody tr td{
            border-top: 1px solid #333;
            padding: 3px 10px;
        }
        .company_logo {
            min-width: 150px;
            max-width: 100%;
            height: auto;
        }
        .pt-1 {
            padding-top: 1rem;
        }
    </style>
</head>
<body>
<table class="voucher-company">
    <tr>
        <td width="100%">
            <table class="voucher-company-left">
                <tbody>
                <tr><td class="text-left font-xxlg font-bold">Pase Laboral</td></tr>
                <tr><td class="text-left font-xxlg font-bold">{{$empresa[0]->razon_social}}</td></tr>
                <tr><td class="text-left font-xl font-bold">Ruc : {{$empresa[0]->ruc}}</td></tr>
                <tr><td class="text-left font-lg">Direccion : {{$empresa[0]->domicilio_fiscal}}</td></tr>
                <tr><td class="text-left font-lg">Departamento : {{$empresa[0]->departamento}}</td></tr>
                <tr><td class="text-left font-lg">Provincia : {{$empresa[0]->provincia}}</td></tr>
                <tr><td class="text-left font-lg">Distrito : {{$empresa[0]->distrito}}</td></tr>
                </tbody>
            </table>
        </td>
        {{--<td width="30%">
            <table class="voucher-company-right">
                <tbody>
                <tr><td class="text-center font-lg">Numero</td></tr>
                <tr><td class="text-center font-xlg font-bold">{{$data[0]->id}}</td></tr>
                </tbody>
            </table>
        </td>--}}
    </tr>
</table>
<table class="voucher-information">
    <tr>
        <td width="55%">
            <table class="voucher-information-left">
                <tbody>
                <tr>
                    <td width="20%">Nombre Trabajador:</td>
                    <td width="80%">{{$data[0]->nombre}} </td>
                </tr>
                <tr>
                        <td width="20%">DNI :</td>
                    <td width="80%">{{$data[0]->dni}}</td>
                </tr>
                <tr>
                    <td width="50%">Email: </td>
                    <td width="50%">{{$data[0]->correo}}</td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="45%">
            <table class="voucher-information-right">
                <tbody>
                <tr>
                    <td width="50%">Fecha Generado:</td>
                    <td width="50%">{{ \Carbon\Carbon::parse($now)->format('d/m/Y g:i:s A') }}</td>
                </tr>
                <tr>
                    <td width="50%">Fecha Valido hasta:</td>
                    <td width="50%">{{ \Carbon\Carbon::parse($addhours)->format('d/m/Y') }} 6:00:00 PM</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<table class="voucher-information">
    <thead>
    <tr>
        <th class="text-center" width="100px">Mensaje</th>
    </tr>
    </thead>
</table>
<table class="voucher-details">
    <thead>
    <tr>
        <th class="text-center" width="80px">.............</th>
    </tr>
    </thead>
</table>
</body>
</html>
