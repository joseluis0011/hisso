<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Hisso Consultores">
    <meta name="keywords" content="Hisso Consultores">

    <title>Hisso</title>
    <link rel="icon" href="{{asset('img/logo_hisso.jpeg')}}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/icofont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/themify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/feather-icon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="color" href="{{asset('assets/css/light-1.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <!-- login -->
    @if(Auth::check())

    @else
        <link href="{{asset('css/login.css')}}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    @endif
</head>
    <body>
    <div main-theme-layout="main-theme-layout-1" id="app">
        @if(Auth::check())
            <app ruta="{{route('basepath')}}"></app>
        @else
            <Auth ruta="{{route('basepath')}}"></Auth>
        @endif
    </div>
    <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <!-- Bootstrap js-->
    <script src="{{asset('assets/js/bootstrap/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap/bootstrap.js')}}"></script>
    <!-- feather icon js-->
    <script src="{{asset('assets/js/icons/feather-icon/feather.min.js')}}"></script>
    <script src="{{asset('assets/js/icons/feather-icon/feather-icon.js')}}"></script>
    <!-- Sidebar jquery-->
    <script src="{{asset('assets/js/sidebar-menu.js')}}"></script>
    <!-- Theme Config -->
    <script src="{{asset('assets/js/script.js')}}"></script>
    <script src="{{asset('assets/js/config.js')}}"></script>
    {{-- theme color--}}
    @if(Auth::check())
        <script src="{{asset('assets/js/theme-customizer/customizer.js')}}"></script>
    @else
        <script src="{{asset('js/login.js')}}"></script>
        <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    @endif
    {{-----------}}
    <script src="{{asset('assets/js/form-wizard/form-wizard-two.js')}}"></script>
    <script src="{{asset('assets/js/form-wizard/jquery.backstretch.min.js')}}"></script>
    <!-- login -->
    </body>
</html>
