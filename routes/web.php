<?php
    
    use App\Http\Controllers\Monitoreo\AsignacionController;
    use App\Http\Controllers\Monitoreo\EppController;
    use App\Http\Controllers\Monitoreo\EppEquiposController;
    use App\Models\Rol;
    use App\User;
    use Illuminate\Support\Facades\Auth;
    use function App\authUser;
    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */
    Route::post('/authenticate/login','Auth\LoginController@login');

    Route::group(['middleware' => ['web']], function () {
        Route::get('/permisos/getListaPermisoUsuario','PermisosController@permisosUsuario');
        Route::post('/authenticate/logout','Auth\LoginController@logout');

        Route::get('/authenticate/getRefrescarUsuarioAutenticado', function () {
            if (isset(Auth::user()->id)){
                return [authUser()];
            }else{
                return ['auth'=>false];
            }
        });
        // Monitoreo
        // empresa
        Route::post('/monitoreo/apiRuc','Monitoreo\EmpresaController@apiRuc');
        Route::post('/monitoreo/empresa/agregar','Monitoreo\EmpresaController@agregarEmpresa');
        Route::post('/monitoreo/empresa/getdata','Monitoreo\EmpresaController@getdata');
        Route::get('/monitoreo/empresa/export', 'Monitoreo\EmpresaController@exportExcel');
        Route::post('/monitoreo/empresa/valContacto','Monitoreo\EmpresaController@addContacto');
        Route::post('/monitoreo/empresa/valTrabajador','Monitoreo\EmpresaController@addTrabajador');
        Route::get('/monitoreo/empresa/dashboard', 'Monitoreo\EmpresaController@dashboard');
        Route::post('/monitoreo/empresa/delete','Monitoreo\EmpresaController@delete');
        Route::get('/monitoreo/empresa/getEmpresa', 'Monitoreo\EmpresaController@getEmpresa');
        // Proyecto
        Route::get('/monitoreo/empresa/getEmpresa','Monitoreo\ProyectoController@getEmpresa');
        Route::post('/monitoreo/proyecto/agregar','Monitoreo\ProyectoController@agregar');
        Route::get('/monitoreo/proyecto/getListarProyecto','Monitoreo\ProyectoController@index');
        Route::get('/monitoreo/proyecto/getdata','Monitoreo\ProyectoController@getdata');
        Route::post('/monitoreo/proyecto/estadoProyecto','Monitoreo\ProyectoController@estadoProyecto');
        Route::get('/monitoreo/proyecto/getListarDetalle','Monitoreo\ProyectoController@getListarDetalle');
        Route::get('/monitoreo/proyecto/export', 'Monitoreo\ProyectoController@exportExcel');
        Route::post('/monitoreo/proyecto/getDashidProyecto','Monitoreo\ProyectoController@getDashidProyecto');
        Route::get('/monitoreo/proyecto/getDashboard','Monitoreo\ProyectoController@getDashboard');
        Route::post('/monitoreo/proyecto/eliminarProyecto','Monitoreo\ProyectoController@eliminarProyecto');
        Route::post('/monitoreo/proyecto/obtenerCodigo','Monitoreo\ProyectoController@obtenerCodigo');
        Route::post('/monitoreo/proyecto/valAgentes','Monitoreo\ProyectoController@valAgentes');
        Route::post('/monitoreo/proyecto/deleteAgente','Monitoreo\ProyectoController@deleteAgente');
        // Trabajador
        Route::get('/monitoreo/trabajador/getProyecto','Monitoreo\TrabajadorController@getProyecto');
        Route::post('/monitoreo/trabajador/getEmpresa','Monitoreo\TrabajadorController@getEmpresa');
        Route::post('/monitoreo/trabajador/importTrabajador','Monitoreo\TrabajadorController@importTrabajador');
        Route::get('/monitoreo/trabajador/getListarTrabajador','Monitoreo\TrabajadorController@index');
        Route::get('/monitoreo/trabajador/getverTrabajador','Monitoreo\TrabajadorController@verTrabajador');
        Route::post('/monitoreo/trabajador/export', 'Monitoreo\TrabajadorController@exportExcel');
        Route::post('/monitoreo/trabajador/getdata','Monitoreo\TrabajadorController@getdata');
        Route::post('/monitoreo/trabajador/eliminar','Monitoreo\TrabajadorController@eliminarData');
        Route::get('/monitoreo/trabajador/formatoTrabajador','Monitoreo\TrabajadorController@formatoTrabajador');
        Route::post('/monitoreo/trabajador/guardarTrabajador', 'Monitoreo\TrabajadorController@guardarTrabajador');
        // instalacion
        Route::post('/monitoreo/trabajador/importInstalacion','Monitoreo\TrabajadorController@importInstalacion');
        Route::get('/monitoreo/trabajador/formatoInstalacion','Monitoreo\TrabajadorController@formatoInstalacion');
        Route::post('/monitoreo/trabajador/instalacion/agregarInstalacion','Monitoreo\TrabajadorController@agregarInstalacion');
        Route::post('/monitoreo/trabajador/instalacion/getInstalacion','Monitoreo\TrabajadorController@getInstalacion');
        Route::post('/monitoreo/trabajador/instalacion/eliminar','Monitoreo\TrabajadorController@eliminar');
        Route::post('/monitoreo/trabajador/guardarInstalacion', 'Monitoreo\TrabajadorController@guardarInstalacion');
        // Equipos
        Route::get('/monitpreo/equipo/getListarEquipo','Monitoreo\EquipoController@index');
        Route::post('/monitoreo/equipo/agregar','Monitoreo\EquipoController@agregarEquipo');
        Route::post('/monitoreo/equipo/estadoEquipo','Monitoreo\EquipoController@estadoEquipo');
        Route::post('/monitoreo/equipo/getdata','Monitoreo\EquipoController@getdata');
        Route::get('/monitoreo/equipo/export', 'Monitoreo\EquipoController@exportExcel');
        Route::get('/monitoreo/equipo/getDashboard', 'Monitoreo\EquipoController@getDashboard');
        Route::post('/monitoreo/equipo/getequipo','Monitoreo\EquipoController@getequipo');
        // Detalle Equipo
        Route::get('/monitoreo/equipo/getListarDetalleEquipo','Monitoreo\EquipoController@detalle');
        Route::post('/monitoreo/equipo/addDetalle','Monitoreo\EquipoController@addDetalle');
        Route::get('/monitoreo/equipo/persona', 'Monitoreo\EquipoController@getpersona');
        // Iluminacion
        Route::get('/monitoreo/iluminacion/getListarTrabajador', 'Monitoreo\IluminacionController@index');
        Route::post('/monitoreo/iluminacion/addTrabajador', 'Monitoreo\IluminacionController@addTrabajador');
        Route::post('/monitoreo/iluminacion/addDetalleIluminacion', 'Monitoreo\IluminacionController@addDetalleIluminacion');
        Route::post('/monitoreo/iluminacion/getIluminacion', 'Monitoreo\IluminacionController@getIluminacion');
        Route::post('/monitoreo/iluminacion/exportExcelTrabajadores', 'Monitoreo\IluminacionController@exportExcelTrabajadores');
        Route::post('/monitoreo/iluminacion/exportFormatoLey', 'Monitoreo\IluminacionController@exportFormatoLey');
        // Ruido
        Route::get('/monitoreo/ruido/getListarInstalacion', 'Monitoreo\RuidoController@index');
        Route::post('/monitoreo/ruido/addInstalacion', 'Monitoreo\RuidoController@addInstalacion');
        Route::post('/monitoreo/ruido/addDetalleRuido', 'Monitoreo\RuidoController@addDetalleRuido');
        Route::post('/monitoreo/ruido/getRuido', 'Monitoreo\RuidoController@getRuido');
        Route::post('/monitoreo/ruido/exportExcelInstalacion', 'Monitoreo\RuidoController@exportExcelInstalacion');
        Route::post('/monitoreo/ruido/exportFormatoLey', 'Monitoreo\RuidoController@exportFormatoLey');
        // Ergonomia
        Route::get('/monitoreo/ergonomia/getListarTrabajador', 'Monitoreo\ErgonomiaController@index');
        Route::post('/monitoreo/ergonomia/addDetalleErgonomia', 'Monitoreo\ErgonomiaController@addDetalleErgonomia');
        Route::post('/monitoreo/ergonomia/getErgonomia', 'Monitoreo\ErgonomiaController@getErgonomia');
        Route::post('/monitoreo/ergonomia/exportExcelTrabajadores', 'Monitoreo\ErgonomiaController@exportExcelTrabajadores');
        // Biologico
        Route::get('/monitoreo/biologico/getListarInstalacion', 'Monitoreo\BiologicoController@index');
        Route::post('/monitoreo/biologico/addDetalleBiologico', 'Monitoreo\BiologicoController@addDetalleBiologico');
        Route::post('/monitoreo/biologico/getBiologico', 'Monitoreo\BiologicoController@getBiologico');
        Route::post('/monitoreo/biologico/exportExcelInstalacion', 'Monitoreo\BiologicoController@exportExcelInstalacion');
        // Personas -> Personal de Hisso
        Route::get('/monitoreo/personal/getListarPersonal','Monitoreo\PersonaController@index');
        Route::post('/monitoreo/personal/agregar','Monitoreo\PersonaController@agregarPersona');
        Route::post('/monitoreo/personal/getdata','Monitoreo\PersonaController@getdata');
        Route::post('/monitoreo/personal/estadoMonitor','Monitoreo\PersonaController@estadoMonitor');
        Route::get('/monitoreo/personal/export', 'Monitoreo\PersonaController@exportExcel');
        Route::get('/monitoreo/personal/getempresa', 'Monitoreo\PersonaController@getempresa');
        Route::post('/monitoreo/personal/addcargo','Monitoreo\PersonaController@addcargo');
        Route::post('/monitoreo/personal/apiDni','Monitoreo\PersonaController@apiDni');
        Route::get('/monitoreo/personal/generaCodigoInterno', 'Monitoreo\PersonaController@generaCodigoInterno');
        // usuario
        Route::get('/monitoreo/usuario/getListarUsuario','Monitoreo\UsuarioController@index');
        Route::post('/monitoreo/usuario/addUsuario','Monitoreo\UsuarioController@addUsuario');
        Route::post('/monitoreo/usuario/obpassword','Monitoreo\UsuarioController@obpassword');
        Route::post('/monitoreo/usuario/estadoUsuario','Monitoreo\UsuarioController@estadoUsuario');
        Route::post('/monitoreo/usuario/getData','Monitoreo\UsuarioController@getData');
        // inspecciones
        Route::post('/monitoreo/inspecciones/addinspeccion','Monitoreo\InspeccionesController@addinspeccion');
        Route::get('/monitoreo/inspecciones/getListarinspeccion','Monitoreo\InspeccionesController@index');
        Route::get('/monitoreo/inspecciones/getlistcategoriaPeligro','Monitoreo\InspeccionesController@getlistcategoriaPeligro');
        Route::post('/monitoreo/inspecciones/getlistsubcategoriaPeligro','Monitoreo\InspeccionesController@getlistsubcategoriaPeligro');
        Route::post('/monitoreo/inspecciones/getdata','Monitoreo\InspeccionesController@getdata');
        Route::post('/monitoreo/inspecciones/estadoinspeccion','Monitoreo\InspeccionesController@estadoinspeccion');
        Route::get('/monitoreo/inspecciones/exportExcel','Monitoreo\InspeccionesController@exportExcel');
        // Comun
        Route::post('/monitoreo/comun/obtenerCodigo', 'ComunController@obtenerCodigo');
        Route::post('/monitoreo/comun/obtenerDataDetalle', 'ComunController@obtenerDataDetalle');
        Route::post('/monitoreo/comun/verificarId', 'ComunController@verificarId');
        Route::get('/monitoreo/comun/getRol', 'ComunController@getRol');
        Route::get('/monitoreo/comun/getResponsable', 'ComunController@getResponsable');
        Route::get('/monitoreo/comun/getCargo', 'ComunController@getCargo');
        Route::post('/monitoreo/comun/searchEmpresa', 'ComunController@searchEmpresa');
        Route::post('/monitoreo/comun/obteneridProyecto', 'ComunController@obteneridProyecto');
        Route::post('/monitoreo/comun/obteneridProyectoandEpp', 'ComunController@obteneridProyectoandEpp');
        Route::get('/monitoreo/comun/getServicios', 'ComunController@getServicios');
        Route::post('/monitoreo/comun/getMonitoreo', 'ComunController@getMonitoreo');
        Route::post('/monitoreo/comun/getCategoria', 'ComunController@getCategoria');
        Route::get('/monitoreo/comun/getdepartamento', 'ComunController@getdepartamento');
        Route::post('/monitoreo/comun/getprovincia','ComunController@getprovincia');
        Route::post('/monitoreo/comun/getdistrito','ComunController@getdistrito');
        // encuesta covid
        //hisso
        Route::get('/monitoreo/covid/listtrabajdores', 'Monitoreo\EncuestaConvidController@listtrabajdores');
        Route::post('/monitoreo/covid/importTrabajador', 'Monitoreo\EncuestaConvidController@importTrabajador');
        Route::get('/monitoreo/covid/listEncuesta', 'Monitoreo\EncuestaConvidController@listEncuesta');
        Route::post('/monitoreo/covid/validarEmpresa', 'Monitoreo\EncuestaConvidController@validarEmpresa');
        Route::post('/monitoreo/covid/eliminartrabajador', 'Monitoreo\EncuestaConvidController@eliminartrabajador');
        Route::post('/monitoreo/covid/searchPregunta', 'Monitoreo\EncuestaConvidController@searchPregunta');
        Route::post('/monitoreo/covid/searchTrabajador', 'Monitoreo\EncuestaConvidController@searchTrabajador');
        Route::post('/monitoreo/covid/exportTrabajador', 'Monitoreo\EncuestaConvidController@exportTrabajador');
        Route::post('/monitoreo/covid/agregarTrabajador', 'Monitoreo\EncuestaConvidController@agregarTrabajador');
        // trabajador
        Route::get('/encuesta/covid/preguntas', 'Trabajador\EncuestaCovidController@preguntas');
        Route::post('/encuesta/covid/agregar', 'Trabajador\EncuestaCovidController@agregar');
        Route::get('/encuesta/covid/verificar', 'Trabajador\EncuestaCovidController@verificar');
        Route::get('/encuesta/covid/download', 'Trabajador\EncuestaCovidController@download');
        // empresa
        Route::get('/empresa/listar', 'Empresa\EmpresaController@listar');
        // EPP
        Route::get('/monitoreo/epp/listtrabajdores', [EppController::class,'listar']);
        Route::get('/monitoreo/epp/listTrabajador', [EppController::class,'listTrabajador']);
        Route::post('/monitoreo/epp/equipoSeguridad', [EppController::class,'equipoSeguridad']);
        Route::post('/monitoreo/epp/puestoTrabajo', [EppController::class,'puestoTrabajo']);
        Route::post('/monitoreo/epp/generarCodigo', [EppController::class,'generarCodigo']);
        Route::post('/monitoreo/epp/agregar', [EppController::class,'agregar']);
        Route::post('/monitoreo/epp/updateTrabajadores', [EppController::class,'updateTrabajadores']);
        Route::post('/monitoreo/epp/addTrabajador', [EppController::class,'addTrabajador']);
        Route::get('/monitoreo/epp/getTrabajador', [EppController::class,'getTrabajador']);
        Route::post('/monitoreo/epp/deleteTrabajador', [EppController::class,'deleteTrabajador']);
        Route::post('/monitoreo/epp/formatoLey', [EppController::class,'formatoLey']);
        // EPP -> EQUIPOS
        Route::get('/monitoreo/epp/equipo/listar', [EppEquiposController::class,'listar']);
        Route::post('/monitoreo/epp/equipo/agregar', [EppEquiposController::class,'agregar']);
        Route::post('/monitoreo/epp/equipo/generarCodigo', [EppEquiposController::class,'generarCodigo']);
        Route::post('/monitoreo/epp/equipo/agregarEquipoTrabajador', [EppEquiposController::class,'agregarEquipoTrabajador']);
        Route::get('/monitoreo/epp/equipo/listEquipoTrabajador', [EppEquiposController::class,'listEquipoTrabajador']);
        Route::post('/monitoreo/epp/equipo/eliminarEquipo', [EppEquiposController::class,'eliminarEquipo']);
        Route::post('/monitoreo/epp/equipo/eliminarEquipoTrabajador', [EppEquiposController::class,'eliminarEquipoTrabajador']);
        // Asignacion de Proyecto
        Route::get('/monitoreo/asignacion/getCodigo', [AsignacionController::class,'getCodigo']);
        Route::post('/monitoreo/asignacion/getProyectos', [AsignacionController::class,'getProyectos']);
        Route::post('/monitoreo/asignacion/addOrUpdate', [AsignacionController::class,'addOrUpdate']);
        Route::get('/monitoreo/asignacion/listar', [AsignacionController::class,'listar']);
        Route::get('/monitoreo/asignacion/getData', [AsignacionController::class,'getData']);
        Route::post('/monitoreo/asignacion/cerrarProyecto', [AsignacionController::class,'cerrarProyecto']);
        Route::post('/monitoreo/asignacion/valServicio', [AsignacionController::class,'valServicio']);
        Route::post('/monitoreo/asignacion/eliminarResponsable', [AsignacionController::class,'eliminarResponsable']);
        Route::post('/monitoreo/asignacion/searchid', [AsignacionController::class,'searchid']);
    });


    Route::get('/{optional?}', function () {
        return view('welcome');
    })->name('basepath')
        ->where('optional','.*');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
